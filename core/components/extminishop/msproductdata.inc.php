<?php
return array(
    'fields' => array(
        'sex' => null,
        'age' => null,
        'unit' => null,
        'vendor_skuid' => null,
        'vendor_url' => null,
        'fabric' => null,
        'soon_available' => 0,
        'parsing_data' => 0,
        'count_in_row' => 0,
        'available_rows' => 0,
        'available_sizes' => 0,
        'in_stock' => 0,
        'season' => null,
        'old_id' => null,
    ),
    'fieldMeta' => array(
        'sex' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'season' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'age' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'unit' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'vendor_skuid' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'vendor_url' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'fabric' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
        'soon_available' => array(
            'dbtype' => 'tinyint',
            'precision' => '1',
            'attributes' => 'unsigned',
            'phptype' => 'boolean',
            'null' => true,
            'default' => 0,
        ),
        'parsing_data' => array(
            'dbtype' => 'bigint',
            'phptype' => 'timestamp',
            'null' => false,
            'default' => '0'
        ),
        'count_in_row' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'phptype' => 'integer',
            'null' => false,
            'default' => '0'
        ),
        'available_rows' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'phptype' => 'integer',
            'null' => false,
            'default' => '0'
        ),
        'available_sizes' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'phptype' => 'integer',
            'null' => false,
            'default' => 0
        ),
        'in_stock' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'phptype' => 'integer',
            'null' => false,
            'default' => '0'
        ),
        'old_id' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
            'default' => null,
        ),
    ),
    'indexes' => array(
        'sex' => array(
            'alias' => 'sex',
            'primary' => false,
            'unique' => false,
            'type' => 'BTREE',
            'columns' => array(
                'sex' => array(
                    'length' => '',
                    'collation' => 'A',
                    'null' => false,
                ),
            ),
        ),
        'season' => array(
            'alias' => 'season',
            'primary' => false,
            'unique' => false,
            'type' => 'BTREE',
            'columns' => array(
                'season' => array(
                    'length' => '',
                    'collation' => 'A',
                    'null' => false,
                ),
            ),
        ),
    ),
);