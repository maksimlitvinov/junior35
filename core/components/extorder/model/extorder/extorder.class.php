<?php
/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/

class ExtOrder
{
    /** @var modX $modx */
    public $modx;
    /** @var array $config */
    public $config;

    /**
     * ExtOrder constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;
        $this->config = $config;

        if (!$this->modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {return false;}
        $this->modx->getService('remains');
    }

    /**
     * Добавление кастомных полей
     */
    public function addFields()
    {
        $this->modx->loadClass('msOrder');
        $this->modx->loadClass('msOrderStatus');

        $fieldsOrder = array(
            'cart_type' => null,
            'promo_code' => null,
            'payment_date' => null,
            'shipment_date' => null,
            'shipment_warehouse' => null,
            'departure_number' => null,
            'card_number_refund' => null,
            'status_refund' => null,
            'date_refund' => null,
            'first_warning_message' => null,
            'second_warning_letter' => null,
            'date_end_reservation' => null,
            'rekvez' => null,
            'num' => null
        );
        $fieldMetaOrder = array(
            'cart_type' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'promo_code' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'payment_date' => array(
                'dbtype' => 'datetime',
                'phptype' => 'date',
                'null' => true,
            ),
            'shipment_date' => array(
                'dbtype' => 'datetime',
                'phptype' => 'date',
                'null' => true,
            ),
            'shipment_warehouse' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'departure_number' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'card_number_refund' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'status_refund' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'date_refund' => array(
                'dbtype' => 'datetime',
                'phptype' => 'date',
                'null' => true,
            ),
            'first_warning_message' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'second_warning_letter' => array(
                'dbtype' => 'varchar',
                'precision' => '255',
                'phptype' => 'string',
                'null' => true
            ),
            'date_end_reservation' => array(
                'dbtype' => 'datetime',
                'phptype' => 'date',
                'null' => true,
            ),
            'rekvez' => array(
                'dbtype' => 'text',
                'phptype' => 'string',
                'null' => true,
            ),
            'num' => array(
                'dbtype' => 'int',
                'precision' => '10',
                'phptype' => 'integer',
                'null' => true,
                'default' => null,
            )
        );

        $fieldsOrderStatus = array('return_remains' => 0);
        $fieldMetaOrderStatus = array(
            'return_remains' => array(
                'dbtype' => 'tinyint',
                'precision' => '1',
                'phptype' => 'integer',
                'null' => true,
                'default' => 0,
            )
        );

        $this->modx->map['msOrder']['fields'] = array_merge($this->modx->map['msOrder']['fields'], $fieldsOrder);
        $this->modx->map['msOrder']['fieldMeta'] = array_merge($this->modx->map['msOrder']['fieldMeta'], $fieldMetaOrder);
        $this->modx->map['msOrderStatus']['fields'] = array_merge($this->modx->map['msOrderStatus']['fields'], $fieldsOrderStatus);
        $this->modx->map['msOrderStatus']['fieldMeta'] = array_merge($this->modx->map['msOrderStatus']['fieldMeta'], $fieldMetaOrderStatus);
    }

    /**
     * @param msOrder $msOrder
     * @return bool
     */
    public function removeOrder($msOrder)
    {
        $status_id = $msOrder->get('status');
        $status = $this->modx->getObject('msOrderStatus', $status_id);

        if (!empty($status)) {
            if (!$status->get('return_remains')) {return true;}
        }

        $products = $msOrder->getMany('Products');
        if (empty($products)) {return true;}

        /** @var msOrderProduct $item */
        foreach ($products as $item) {
            $response = $this->removeOrderProduct($item);
            if (!$response['success']) {return $response;}
        }

        return $this->success('Заказ успешно удален.');
    }

    /**
     * @param msOrder $msOrder
     * @return false|string
     */
    public function getEndReservationDate($msOrder)
    {
        $date_format = $this->modx->getOption('manager_date_format', array(), 'Y-m-d');
        $type = $msOrder->get('cart_type');
        $reserve_days = $this->modx->getOption('reserv_day_' . $type, array(), 0);
        $now = date($date_format);
        return date( $date_format, strtotime( $now . ' +' . $reserve_days . ' days' ) );
    }

    /**
     * @param msOrder $msOrder
     * @return bool
     */
    public function createOrder($msOrder)
    {
        $products = $msOrder->getMany('Products');
        /** @var msOrderProduct $item */
        foreach ($products as $item) {
            $product = $item->toArray();
            if (!isset($product['options']['size'])) {continue;}
            $order_count = $product['count'];
            $productData = $this->modx->getObject('msProductData', $product['product_id']);
            $in_stock = $productData->get('in_stock');
            $available_sizes = $productData->get('available_sizes');
            if ($product['options']['size'] != 'all') {
                $remains = $this->modx->getObject('remainObject', array(
                    'product_id' => $product['product_id'],
                    'size' => $product['options']['size']
                ));
                $remains_count = $remains->get('count');
                if ($order_count > $remains_count) {return false;}
                $productData->set('available_sizes', $available_sizes - $order_count);
                $remains->set('count', $remains_count - $order_count);
                $remains->save();
                $productData->save();
            }
            else if ($product['options']['size'] == 'all') {
                $count_in_row = $productData->get('count_in_row');
                $rows_count = $productData->get('available_rows');
                if ($order_count > $rows_count) {return false;}
                $productData->set('in_stock', $in_stock - ($order_count * $count_in_row));
                $productData->set('available_rows', $rows_count - $order_count);
                $productData->save();
            }
        }
        return true;
    }

    /**
     * @param msOrder $msOrder
     * @return array
     */
    public function validateCount($msOrder)
    {
        $products = $msOrder->getMany('Products');
        /** @var msOrderProduct $item */
        foreach ($products as $item) {
            $product = $item->toArray();
            if (!isset($product['options']['size'])) {continue;}
            $order_count = $product['count'];
            $productData = $this->modx->getObject('msProductData', $product['product_id']);
            if ($product['options']['size'] != 'all') {
                $remains = $this->modx->getObject('remainObject', array(
                    'product_id' => $product['product_id'],
                    'size' => $product['options']['size']
                ));
                $remains_count = $remains->get('count');
                if ($order_count > $remains_count) {return $this->error('Указано недопустимое количество. Максимальное количество: ' . $remains_count);}
            }
            else if ($product['options']['size'] == 'all') {
                $rows_count = $productData->get('available_rows');
                if ($order_count > $rows_count) {$this->error('Указано недопустимое количество. Максимальное количество: ' . $rows_count);}
            }
        }
        return $this->success();
    }

    /**
     * @param msOrderProduct $orderProduct
     * @return bool
     */
    public function removeOrderProduct($orderProduct)
    {
        $order_id = $orderProduct->get('order_id');
        $msOrder = $this->modx->getObject('msOrder', $order_id);
        $status_id = $msOrder->get('status');
        $status = $this->modx->getObject('msOrderStatus', $status_id);
        if (!empty($status)) {
            if (!$status->get('return_remains')) {return true;}
        }

        $orderProductArray = $orderProduct->toArray();
        if (!isset($orderProductArray['options']['size'])) {return true;}

        $productData = $this->modx->getObject('msProductData', $orderProductArray['product_id']);
        $in_stock = $productData->get('in_stock');
        $available_sizes = $productData->get('available_sizes');
        $order_count = $orderProductArray['count'];

        if ($orderProductArray['options']['size'] != 'all') {
            $remains = $this->modx->getObject('remainObject', array(
                'product_id' => $orderProductArray['product_id'],
                'size' => $orderProductArray['options']['size']
            ));

            if (empty($remains)) {
                return $this->error('Размер "' . $orderProductArray['options']['size'] . '" не найден. Удаление не возможно.');
            }

            $remains_count = $remains->get('count');
            $remains->set('count', $remains_count + $order_count);
            $productData->set('available_sizes', $available_sizes + $order_count);
            $remains->save();
            $productData->save();
        }
        else if ($orderProductArray['options']['size'] == 'all') {
            $count_in_row = $productData->get('count_in_row');
            $rows_count = $productData->get('available_rows');
            $productData->set('in_stock', $in_stock + ($order_count * $count_in_row));
            $productData->set('available_rows', $order_count + $rows_count);
            $productData->save();
        }
        return $this->success('Товар успешно удален.');
    }

    /**
     * @param msOrderProduct $orderProduct
     * @return array
     */
    public function updateOrderProduct(msOrderProduct $orderProduct)
    {
        $order_product_id = $orderProduct->get('id');
        $msOrder = $this->modx->getObject('msOrderProduct', $order_product_id);

        $orderProductArray = $orderProduct->toArray();
        if (!isset($orderProductArray['options']['size'])) {return $this->success();}

        $productData = $this->modx->getObject('msProductData', $orderProductArray['product_id']);
        $in_stock = $productData->get('in_stock');
        $available_sizes = $productData->get('available_sizes');
        $order_count = $orderProductArray['count'];
        $old_order_count = $msOrder->get('count');
        $isRow = $orderProductArray['options']['size'] == 'all';

        if ($order_count <= 0) {
            return $this->error('Недопустимое количество. Минимальное количество: 1');
        }

        if ($isRow) {
            $count_in_row = $productData->get('count_in_row');
            $rows_count = $productData->get('available_rows');

            if ($order_count > $old_order_count) {
                $count = $order_count - $old_order_count;
                if ($count > $rows_count) {
                    return $this->error('Превышено допустимое количество. Допустимое количество: ' . ($old_order_count + $rows_count));
                }
                $productData->set('available_rows', $rows_count - $count);
                $productData->set('in_stock', $in_stock - ($count * $count_in_row));
                $productData->save();
                return $this->success('', array(
                    'max_count' => $rows_count - $count
                ));
            }
            else if ($order_count < $old_order_count) {
                $count = $old_order_count - $order_count;
                $productData->set('available_rows', $rows_count + $count);
                $productData->set('in_stock', $in_stock + ($count * $count_in_row));
                $productData->save();
                return $this->success('', array(
                    'max_count' => $rows_count + $count
                ));
            }
        }
        else {
            $remains = $this->modx->getObject('remainObject', array(
                'product_id' => $orderProductArray['product_id'],
                'size' => $orderProductArray['options']['size']
            ));
            $remains_count = $remains->get('count');
            if ($order_count > $old_order_count) {
                $count = $order_count - $old_order_count;
                if ($count > $remains_count) {
                    return $this->error('Превышено допустимое количество. Допустимое количество: ' . ($old_order_count + $remains_count));
                }
                $productData->set('available_sizes', $available_sizes - $count);
                $remains->set('count', $remains_count - $count);
                $productData->save();
                $remains->save();
                return $this->success('', array(
                    'max_count' => $remains_count - $count
                ));
            }
            else if ($order_count < $old_order_count) {
                $count = $old_order_count - $order_count;
                $productData->set('available_sizes', $available_sizes + $count);
                $remains->set('count', $remains_count + $count);
                $productData->save();
                $remains->save();
                return $this->success('', array(
                    'max_count' => $remains_count + $count
                ));
            }
        }
        return $this->success();
    }

    public function createOrderProduct(msOrderProduct $orderProduct)
    {
        $orderProductArray = $orderProduct->toArray();
        if (!isset($orderProductArray['options']['size'])) {return $this->success();}

        $productData = $this->modx->getObject('msProductData', $orderProductArray['product_id']);
        // Вналичии для рядов
        $in_stock = $productData->get('in_stock');
        // Вналичии для размеров
        $available_sizes = $productData->get('available_sizes');
        // Количество в заказе
        $order_count = $orderProductArray['count'];

        $order = $this->modx->getObject('msOrder', $orderProductArray['order_id']);
        $cart_type = $order->get('cart_type');

        $orderProductType = $orderProductArray['options']['size'] == 'all' ? 'row' : 'size';
        if ($cart_type != $orderProductType) {
            $type_name = $orderProductType == 'row' ? 'Ряды' : 'Размеры';
            return $this->error('Вы пытаетесь добавить товар другого типа. Тип заказа: ' . $type_name);
        }
        $isRow = $orderProductType == 'row';

        if ($isRow) {
            $count_in_row = $productData->get('count_in_row');
            $rows_count = $productData->get('available_rows');

            if ($order_count > $rows_count) {
                return $this->error('Превышено допустимое количество. Допустимое количество: ' . ($rows_count));
            }

            $productData->set('available_rows', $rows_count - $order_count);
            $productData->set('in_stock', $in_stock - ($order_count * $count_in_row));
            $productData->save();
            return $this->success('', array(
                'max_count' => $rows_count - $order_count
            ));
        }
        else {
            $remains = $this->modx->getObject('remainObject', array(
                'product_id' => $orderProductArray['product_id'],
                'size' => $orderProductArray['options']['size']
            ));
            if (empty($remains)) {
                return $this->error('Размер '  . $orderProductArray['options']['size'] . ', не найден');
            }
            $remains_count = $remains->get('count');
            if ($order_count > $remains_count) {
                return $this->error('Превышено допустимое количество. Допустимое количество: ' . $remains_count);
            }
            $remains->set('count', $remains_count - $order_count);
            $productData->set('available_sizes', $available_sizes - $order_count);
            $remains->save();
            $productData->save();
            return $this->success('', array(
                'max_count' => $remains_count - $order_count
            ));
        }
    }

    /**
     * @param msOrder $order
     * @return int
     */
    public function getProductsCount($order)
    {
        $products = $order->getMany('Products');
        $count = 0;
        foreach ($products as $product) {
            $count += $product->get('count');
        }
        return $count;
    }

    /**
     * @param msOrder $order
     * @param null|string $counter
     * @param string $body
     * @param null|string $prepay - Содержит новую дату для отправки повторного письма
     */
    public function sendWarning($order, $body, $counter = null, $prepay = null)
    {
        /** @var miniShop2 $ms2 */
        $ms2 = $this->modx->getService('minishop2');
        $user = $order->getOne('User');
        $user_profile = $user->getOne('Profile');

        $ms2->sendEmail($user_profile->get('email'), 'Неоплаченный заказ', $body);

        if (!empty($prepay)) {$order->set('date_end_reservation', $prepay);}
        if (!empty($counter)) {$order->set($counter, 1);}
        $order->save();
    }

    /**
     * @return array
     */
    private function success($message = '', $data = array())
    {
        return array(
            'success' => true,
            'message' => $message,
            'data' => $data
        );
    }

    /**
     * @param string $message
     * @return array
     */
    private function error($message = '')
    {
        return array(
            'success' => false,
            'message' => $message
        );
    }
}