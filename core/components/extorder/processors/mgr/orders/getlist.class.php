<?php
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/minishop2/processors/mgr/orders/getlist.class.php';

class orderGetListProcessor extends msOrderGetListProcessor
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function prepareArray(array $data)
    {
        if (empty($data['customer'])) {
            $data['customer'] = $data['customer_username'];
        }

        $data['status'] = '<span style="color:#' . $data['color'] . ';">' . $data['status'] . '</span>';
        unset($data['color']);

        if (isset($data['cost'])) {
            $data['cost'] = $this->ms2->formatPrice($data['cost']);
        }
        if (isset($data['cart_cost'])) {
            $data['cart_cost'] = $this->ms2->formatPrice($data['cart_cost']);
        }
        if (isset($data['delivery_cost'])) {
            $data['delivery_cost'] = $this->ms2->formatPrice($data['delivery_cost']);
        }
        if (isset($data['weight'])) {
            $data['weight'] = $this->ms2->formatWeight($data['weight']);
        }

        $data['actions'] = array(
            array(
                'cls' => '',
                'icon' => 'icon icon-edit',
                'title' => $this->modx->lexicon('ms2_menu_update'),
                'action' => 'updateOrder',
                'button' => true,
                'menu' => true,
            ),
            array(
                'cls' => array(
                    'menu' => 'red',
                    'button' => 'red',
                ),
                'icon' => 'icon icon-trash-o',
                'title' => $this->modx->lexicon('ms2_menu_remove'),
                'multiple' => $this->modx->lexicon('ms2_menu_remove_multiple'),
                'action' => 'removeOrder',
                'button' => true,
                'menu' => true,
            ),
            array(
                'cls' => '',
                'icon' => 'icon icon-file-text-o',
                'title' => 'Сгенерировать счет',
                'action' => 'invoice',
                'button' => true,
                'menu' => true,
            ),
            array(
                'cls' => '',
                'icon' => 'icon icon-file-o',
                'title' => 'ТОРГ-12',
                'action' => 'torg12',
                'button' => true,
                'menu' => true,
            ),
            array(
                'cls' => '',
                'icon' => 'icon icon-print',
                'title' => 'Распечатать',
                'action' => 'print',
                'button' => true,
                'menu' => true
            )
            /*
            array(
                'cls' => '',
                'icon' => 'icon icon-cog actions-menu',
                'menu' => false,
                'button' => true,
                'action' => 'showMenu',
                'type' => 'menu',
            ),
            */
        );

        return $data;
    }
}
return 'orderGetListProcessor';