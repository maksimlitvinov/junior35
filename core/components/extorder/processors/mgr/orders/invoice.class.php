<?php

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/extorder/vendor/autoload.php';

class orderInvoiceProcessor extends modProcessor
{
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var ExtOrder object|null  */
    public $extOrder;

    /**
     * orderInvoiceProcessor constructor.
     * @param modX $modx
     * @param array $properties
     */
    public function __construct(modX &$modx, array $properties = array())
    {
        parent::__construct($modx, $properties);
        $this->modx->getService('fileHandler','modFileHandler');
        $this->ms2 = $this->modx->getService('minishop2');
        if ($this->modx->loadClass('ExtOrder', MODX_CORE_PATH . 'components/extorder/model/extorder/', true, true)) {
            $this->extOrder = $this->modx->getService('ExtOrder');
        }
    }

    /**
     * @return array|mixed|string
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function process()
    {
        $filePath = MODX_ASSETS_PATH . 'documents/invoice/' . $this->properties['id'] . '/';
        $fileUrl = MODX_ASSETS_URL . 'documents/invoice/' . $this->properties['id'] . '/';
        $fileName = 'invoice_' . microtime() . '.xlsx';
        $row = 22;

        if (!file_exists($filePath)) {
            /** @var modDirectory $directory */
            $directory = $this->modx->fileHandler->make($filePath, array(), 'modDirectory');
            $directory->create();
        }

        /** @var msOrder $order */
        $order = $this->modx->getObject('msOrder', $this->properties['id']);
        /** @var array $orderProduct */
        $orderProducts = $order->getMany('Products');

        if (count($orderProducts) == 0) {return $this->failure('В этом заказе нет товаров.');}

        $rekvez = $order->get('rekvez');
        if (empty($rekvez)) {return $this->failure('Странно, но пользователь не указал реквизиты. Попросите его еще раз добавить реквизиты.');}

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(MODX_CORE_PATH . 'components/extorder/templates/invoice.xlsx');
        $spreadsheet->getActiveSheet()->setCellValue('H17', $rekvez);
        $spreadsheet->getActiveSheet()->setCellValue('H19', $rekvez);

        /** @var msOrderProduct $orderProduct */
        $i = 0;
        foreach ($orderProducts as $orderProduct) {
            $options = $orderProduct->get('options');
            $size = $options['size'] == 'all' ? '(Весь размерный ряд)' : '(Размер: ' . $options['size'] . ')';

            if ($i > 0) {
                $newRow = $spreadsheet->getActiveSheet();
                $newRow->insertNewRowBefore($row, 1);
                $this->copyRowFull($newRow, $newRow, 22, $row);
                $newRow->mergeCells("B{$row}:C{$row}");
                $newRow->mergeCells("D{$row}:X{$row}");
                $newRow->mergeCells("Y{$row}:AA{$row}");
                $newRow->mergeCells("AB{$row}:AC{$row}");
                $newRow->mergeCells("AD{$row}:AG{$row}");
                $newRow->mergeCells("AH{$row}:AL{$row}");
            }

            $spreadsheet->getActiveSheet()
                ->setCellValue('B' . $row, ++$i)
                ->setCellValue('D'.$row, $orderProduct->get('name') . ' ' . $size)
                ->setCellValue('AD'.$row, $this->ms2->formatPrice($orderProduct->get('price')))
                ->setCellValue('Y'.$row, $orderProduct->get('count'))
                ->setCellValue('AH'.$row, $this->ms2->formatPrice($orderProduct->get('cost')))
            ;
            $row++;
        }

        $date  = new DateTime($order->get('createdon'));
        $activeSheet = $spreadsheet->getActiveSheet();
        $row++;
        $cost = $this->ms2->formatPrice($order->get('cost'));
        $activeSheet
            ->setCellValue("B11", "Счёт на оплату №{$order->get('num')} от " . date_format($date, 'd.m.Y'))
            ->setCellValue("AH" . $row++, $cost)
            ->setCellValue("AH" . $row++, $cost)
            ->setCellValue("AH" . $row++, $cost)
            ->setCellValue('B' . $row,  'Всего наименований '. count($orderProducts) .', на сумму ' . $cost . ' руб.    '  )
        ;

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $objWriter->save($filePath . $fileName);
        return $this->success('Счет успешно сформирован', (object) array('invoice' => $fileUrl . $fileName));
    }

    /**
     * @param $ws_from
     * @param $ws_to
     * @param $row_from
     * @param $row_to
     */
    public function copyRowFull(&$ws_from, &$ws_to, $row_from, $row_to)
    {
        $ws_to->getRowDimension($row_to)->setRowHeight($ws_from->getRowDimension($row_from)->getRowHeight());
        $lastColumn = $ws_from->getHighestColumn();
        ++$lastColumn;
        for ($c = 'A'; $c != $lastColumn; ++$c) {
            $cell_from = $ws_from->getCell($c.$row_from);
            $cell_to = $ws_to->getCell($c.$row_to);
            $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
            $cell_to->setValue($cell_from->getValue());
        }
    }
}

return 'orderInvoiceProcessor';