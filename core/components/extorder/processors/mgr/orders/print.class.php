<?php

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/extorder/vendor/autoload.php';

class orderPrintProcessor extends modProcessor
{

    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var ExtOrder object|null  */
    public $extOrder;

    /**
     * orderInvoiceProcessor constructor.
     * @param modX $modx
     * @param array $properties
     */
    public function __construct(modX &$modx, array $properties = array())
    {
        parent::__construct($modx, $properties);
        $this->modx->getService('fileHandler','modFileHandler');
        $this->ms2 = $this->modx->getService('minishop2');
        if ($this->modx->loadClass('ExtOrder', MODX_CORE_PATH . 'components/extorder/model/extorder/', true, true)) {
            $this->extOrder = $this->modx->getService('ExtOrder');
        }
    }

    /**
     * Run the processor and return the result. Override this in your derivative class to provide custom functionality.
     * Used here for pre-2.2-style processors.
     *
     * @return mixed
     */
    public function process()
    {
        $msorder = $this->modx->getObject('msOrder', $this->properties['id']);
        $products = $msorder->getMany('Products');
        $orderproducts = array();
        foreach ($products as $product) {
            $pid = $product->get('product_id');
            $file = $this->modx->getObject('msProductFile', array('path' => $pid . '/small/'));
            $orderproducts[] = array_merge($product->toArray(), array('thumb' => $file->get('url')));
        }

        $doc = $this->ms2->pdoTools->getChunk('@FILE templates/print/order.tpl', array(
            'num' => $msorder->get('num'),
            'order' => array_merge($msorder->toArray(), array(
                'payment' => $msorder->Payment->get('name'),
                'delivery' => $msorder->Delivery->get('name'),
                'receiver' => $msorder->Address->get('receiver'),
                'phone' => $msorder->Address->get('phone'),
                'city' => $msorder->Address->get('city'),
                'street' => $msorder->Address->get('street'),
                'comment' => $msorder->Address->get('comment'),
                'status' => $msorder->Status->get('name')
            )),
            'products' => $orderproducts,
            'user' => $msorder->UserProfile->toArray()
        ));
        return $this->success('Счет успешно сформирован', (object) array('doc' => $doc));
    }
}
return 'orderPrintProcessor';