<?php

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/extorder/vendor/autoload.php';

class orderTorg12Processor extends modProcessor
{
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var ExtOrder object|null  */
    public $extOrder;

    public function __construct(modX &$modx, array $properties = array())
    {
        parent::__construct($modx, $properties);
        $this->modx->getService('fileHandler','modFileHandler');
        $this->ms2 = $this->modx->getService('minishop2');
        if ($this->modx->loadClass('ExtOrder', MODX_CORE_PATH . 'components/extorder/model/extorder/', true, true)) {
            $this->extOrder = $this->modx->getService('ExtOrder');
        }
    }

    public function process()
    {
        $filePath = MODX_ASSETS_PATH . 'documents/torg12/' . $this->properties['id'] . '/';
        $fileUrl = MODX_ASSETS_URL . 'documents/torg12/' . $this->properties['id'] . '/';
        $fileName = 'torg_12_' . microtime() . '.xlsx';
        $row = 33;

        if (!file_exists($filePath)) {
            /** @var modDirectory $directory */
            $directory = $this->modx->fileHandler->make($filePath, array(), 'modDirectory');
            $directory->create();
        }

        /** @var msOrder $order */
        $order = $this->modx->getObject('msOrder', $this->properties['id']);
        /** @var array $orderProduct */
        $orderProducts = $order->getMany('Products');

        if (count($orderProducts) == 0) {return $this->failure('В этом заказе нет товаров.');}

        $user = $order->getOne('User');
        $userProfile = $user->getOne('Profile');

        $date  = new DateTime($order->get('createdon'));

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(MODX_CORE_PATH . 'components/extorder/templates/torg12.xlsx');
        $spreadsheet->getActiveSheet()
            ->setCellValue('AX28', $order->get('num'))
            ->setCellValue('I18', $userProfile->get('fullname'))
            ->setCellValue('BI28',  date_format($date, 'd.m.Y') );

        $i = 0;
        /** @var msOrderProduct $orderProduct */
        foreach ($orderProducts as $orderProduct) {
            if ($i > 0) {
                $as = $spreadsheet->getActiveSheet();

                $as->insertNewRowBefore($row, 1);
                $as->mergeCells("A{$row}:C{$row}");
                $as->mergeCells("D{$row}:S{$row}");
                $as->mergeCells("T{$row}:W{$row}");
                $as->mergeCells("X{$row}:AB{$row}");
                $as->mergeCells("AC{$row}:AG{$row}");
                $as->mergeCells("AH{$row}:AL{$row}");
                $as->mergeCells("AM{$row}:AQ{$row}");
                $as->mergeCells("AR{$row}:AV{$row}");
                $as->mergeCells("AW{$row}:BA{$row}");
                $as->mergeCells("BB{$row}:BG{$row}");
                $as->mergeCells("BH{$row}:BP{$row}");
                $as->mergeCells("BQ{$row}:BW{$row}");
                $as->mergeCells("BX{$row}:CA{$row}");
                $as->mergeCells("CB{$row}:CH{$row}");
                $as->mergeCells("CI{$row}:CQ{$row}");

                $this->copyRowFull($as, $as, 33, $row);
            }

            $product = $orderProduct->getOne('Product');
            $options = $orderProduct->get('options');
            $size = $options['size'] == 'all' ? '(Весь размерный ряд)' : '(Размер: ' . $options['size'] . ')';

            $spreadsheet->getActiveSheet()
                ->setCellValue('A'.$row, ++$i)
                ->setCellValue('D'.$row, $orderProduct->get('name') . ' ' . $size)
                ->setCellValue('BH'.$row, $this->ms2->formatPrice($orderProduct->get('price')))
                ->setCellValue('BB'.$row, $orderProduct->get('count'))
                ->setCellValue('T'.$row, $product->get('article'))
                ->setCellValue('BQ'.$row, $this->ms2->formatPrice($orderProduct->get('cost')))
                ->setCellValue('CI'.$row, $this->ms2->formatPrice($orderProduct->get('cost')))
            ;

            $row++;
        }

        $activeSheet = $spreadsheet->getActiveSheet();
        $activeSheet
            ->setCellValue("CI" . $row++, $this->ms2->formatPrice($order->get('cost')));
        ;

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $objWriter->save($filePath . $fileName);
        return $this->success('Накладная успешно сформирована', (object) array('torg12' => $fileUrl . $fileName));
    }

    /**
     * @param $ws_from
     * @param $ws_to
     * @param $row_from
     * @param $row_to
     */
    public function copyRowFull(&$ws_from, &$ws_to, $row_from, $row_to)
    {
        $ws_to->getRowDimension($row_to)->setRowHeight($ws_from->getRowDimension($row_from)->getRowHeight());
        $lastColumn = $ws_from->getHighestColumn();
        ++$lastColumn;
        for ($c = 'A'; $c != $lastColumn; ++$c) {
            $cell_from = $ws_from->getCell($c.$row_from);
            $cell_to = $ws_to->getCell($c.$row_to);
            $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
            $cell_to->setValue($cell_from->getValue());
        }
    }
}

return 'orderTorg12Processor';