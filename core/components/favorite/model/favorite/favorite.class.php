<?php
class Favorite {
    /**
     * @var modX $modx
     */
    public $modx;
    /**
     * @var array
     */
    public $config;
    /**
     * @var bool
     */
    protected $hasSessionContext;

    /**
     * Favorite constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $corePath = $this->modx->getOption('favorite_core_path', $config, $this->modx->getOption('core_path') . 'components/favorite/');
        $assetsUrl = $this->modx->getOption('favorite_assets_url', $config, $this->modx->getOption('assets_url') . 'components/favorite/');
        $connectorUrl = $assetsUrl . 'connector.php';

        $this->config = array_merge(array(
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $connectorUrl,
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/'
        ), $config);

        $this->modx->addPackage('favorite', $this->config['modelPath']);
        $this->modx->lexicon->load('favorite:default');

        $this->hasSessionContext = $this->modx->user->hasSessionContext('web');
    }

    /**
     * @param mixed $rid
     * @return array
     */
    public function add($rid)
    {
        $result = array();
        $uid = $this->modx->user->get('id');
        if ($this->hasSessionContext) {
            $result = $this->addToBase($rid, $uid);
        }
        else {
            $result = $this->addToSession($rid);
        }
        return $result;
    }

    /**
     * @param $rid
     * @return array
     */
    public function remove($rid)
    {
        $result = array();
        $uid = $this->modx->user->get('id');
        if ($this->hasSessionContext) {
            $result = $this->removeFromBase($rid, $uid);
        }
        else {
            $result = $this->removeFromSession($rid);
        }

        return $result;
    }

    /**
     * @param $rid
     * @param $uid
     * @return array
     */
    public function addToBase($rid, $uid)
    {
        $fav = $this->modx->newObject('favoriteObject', array(
            'pid' => (int) $rid,
            'uid' =>  $uid
        ));

        $fav->save();

        return $this->success(array(
            'count' => $this->getCount($uid),
            'resources' => $this->getItems($uid)
        ));
    }

    /**
     * @param $rid
     * @param int $uid
     * @return array
     */
    public function addToSession($rid, $uid = 0)
    {
        if (!isset($_SESSION['favorite'])) {
            $_SESSION['favorite'] = array(
                'user' => $uid,
                'resources' => array($rid)
            );
        }
        else {
            $_SESSION['favorite']['resources'][] = $rid;
            $_SESSION['favorite']['user'] = $uid;
        }
        $count = count($_SESSION['favorite']['resources']);
        $_SESSION['favorite']['count'] = $count;
        return $this->success(array(
            'count' => $count,
            'resources' => $_SESSION['favorite']['resources']
        ));
    }

    /**
     * @param $rid
     * @param $uid
     * @return array
     */
    public function removeFromBase($rid, $uid)
    {
        $res = $this->modx->getObject('favoriteObject', array(
            'uid' => $uid,
            'pid' => $rid
        ));

        if ($res->remove() == false) {
            return $this->failure();
        }

        return $this->success(array(
            'count' => $this->getCount($uid),
        ),'Товар убран из Избранное');
    }

    /**
     * @param $rid
     * @param int $uid
     * @return array
     */
    public function removeFromSession($rid, $uid = 0)
    {
        foreach ($_SESSION['favorite']['resources'] as $key => $item) {
            if ($item == $rid) {
                unset($_SESSION['favorite']['resources'][$key]);
            }
        }
        $count = count($_SESSION['favorite']['resources']);
        $_SESSION['favorite']['count'] = $count;
        return $this->success(array(
            'count' => $count,
        ), 'Товар убран из Избранное');
    }

    /**
     * @param int $uid
     * @return array
     */
    public function getItems($uid = 0)
    {
        $items = array();
        if ($this->hasSessionContext) {
            if (empty($uid)) {$uid = $this->modx->user->get('id');}
            $q = $this->modx->newQuery('favoriteObject');
            $q->where(array(
                'uid' => $uid
            ));
            $res = $this->modx->getCollection('favoriteObject', $q);
            if (!empty($res)) {
                foreach ($res as $one) {
                    $items[] = $one->get('pid');
                }
            }
        }
        else {
            if (isset($_SESSION['favorite']['resources'])) {
                $items = $_SESSION['favorite']['resources'];
            }
        }

        return $items;
    }

    /**
     * @param int $uid
     * @return int
     */
    public function getCount($uid = 0)
    {
        $count = 0;
        if ($this->hasSessionContext) {
            if (empty($uid)) {$uid = $this->modx->user->get('id');}
            $count = $this->modx->getCount('favoriteObject', array('uid' => $uid));
        }
        else {
            if (isset($_SESSION['favorite']['count'])) {
                $count = $_SESSION['favorite']['count'];
            }
        }

        return $count;
    }

    /**
     * @param $rid
     * @param int $uid
     * @return bool
     */
    public function has($rid, $uid = 0)
    {
        $res = false;
        if ($this->hasSessionContext) {
            $count = $this->modx->getCount('favoriteObject', array(
                'pid' => $rid,
                'uid' => $uid
            ));
            $res = (boolean) $count;
        }
        else {
            if (isset($_SESSION['favorite']['resources'])) {
                $res = in_array($rid, $_SESSION['favorite']['resources']);
            }
        }
        return $res;
    }

    /**
     * @param array $data
     * @param string $message
     * @return array
     */
    public function success(array $data = array(), $message = '')
    {
        if (empty($message)) {$message = 'Выполненно успешно';}
        return array(
            'success' => true,
            'message' => $message,
            'data' => $data,
            'count' => isset($data['count']) ? $data['count'] : 0
        );
    }

    /**
     * @param array $data
     * @param string $message
     * @return array
     */
    public function failure(array $data = array(), $message = '')
    {
        if (empty($message)) {$message = 'Произошла ошибка';}
        return array(
            'success' => false,
            'message' => $message,
            'data' => $data,
            'count' => 0
        );
    }
}