<?php
$xpdo_meta_map['favoriteObject'] = array(
    'package' => 'favorite',
    'version' => '1.1',
    'table' => 'favorite',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        'pid' => null,
        'uid' => null
    ),
    'fieldMeta' => array(
        'pid' => array(
            'dbtype' => 'integer',
            'precision' => '10',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => null,
        ),
        'uid' => array(
            'dbtype' => 'integer',
            'precision' => '10',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => null,
        )
    ),
    'indexes' => array(
        'uid' => array(
            'alias' => 'uid',
            'primary' => false,
            'unique' => false,
            'type' => 'BTREE',
            'columns' =>
                array (
                    'uid' =>
                        array (
                            'length' => '',
                            'collation' => 'A',
                            'null' => false,
                        ),
                ),
        )
    ),
    'aggregates' => array(
        'Product' => array(
            'class' => 'msProduct',
            'local' => 'pid',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        ),
        'User' => array(
            'class' => 'modUser',
            'local' => 'uid',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        )
    )
);