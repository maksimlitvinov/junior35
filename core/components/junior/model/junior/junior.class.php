<?php
class Junior
{
    /** @var modX $modx */
    public $modx;
    /** @var array $config */
    public $config;
    /** @var array $uri */
    public $uri;
    /** @var int $uri_count */
    public $uri_count;
    /** @var array $exclude */
    public $exclude;
    /** @var array $alloved_sex */
    public $allowed_sex;
    public $brand_url;

    /**
     * Junior constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX $modx, $config = array())
    {
        $this->modx = $modx;
        $corePath = $this->modx->getOption('favorite_core_path', $config, $this->modx->getOption('core_path') . 'components/favorite/');
        $this->config = array_merge(array(
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'catalog_id' => 2,
            'root_id' => 0
        ), $config);

        $this->exclude = array('assets');
        $this->allowed_sex = array(
            $this->modx->getObject('modResource', 76)->get('uri') => 76,
            $this->modx->getObject('modResource', 77)->get('uri') => 77
        );
        $this->brand_url = array($this->modx->getObject('modResource', 3718)->get('uri') => 3718);

        $this->modx->addPackage('junior', $this->config['modelPath']);
    }

    /**
     * Основной метод расширения маршрутизации.
     * Точка входа.
     */
    public function handleRequest()
    {
        $alias = $this->modx->context->getOption('request_param_alias', 'q');
        if (empty($this->modx->request)) {$this->modx->getRequest();}
        $uri = $this->modx->request->getParameters($alias, 'REQUEST');
        if (empty($uri)) {return false;}
        $this->uri = explode('/', $uri);
        $this->uri_count = count($this->uri);
        if (in_array($this->uri[0], $this->exclude)) {return false;}
        else if (isset($this->allowed_sex[$this->uri[0]])) {$this->sex();}
        else if (isset($this->brand_url[$this->uri[0]])) {$this->brand();}
    }

    /**
     * Маршрут для градации по полу
     * @return bool
     */
    private function sex()
    {
        if ($this->uri_count < 2) {
            //$this->modx->log(1, print_r('Недостаточно параметров'.$this->uri_count, 1));
            return false;
        }

        // Если категория не найдена, то выходим
        $category_uri = $this->uri[$this->uri_count - 1];
        if (empty($category_uri)) {$category_uri = $this->uri[$this->uri_count - 2];}
        if (empty($category_uri)) {$this->modx->log(1, print_r('Недостаточно параметров для поиска категории', 1));return false;}
        $category_id = $this->modx->findResource($category_uri . '/');
        if (empty($category_id)) {$this->modx->log(1, print_r('Категория не найдена', 1));return false;}

        //Если в родителях нет каталога, то выходим
        $category_parents = $this->modx->getParentIds($category_id);
        if (!in_array($this->config['catalog_id'], $category_parents)) {$this->modx->log(1, print_r('Не в каталоге', 1));return false;}

        $sex = $this->modx->getObject('modResource', $this->allowed_sex[$this->uri[0]]);
        $where = '(`Data`.`sex` = "' . $sex->get('pagetitle') . '" OR `Data`.`sex` = "Унисекс")';
        $this->sendForward($category_id, $sex, $category_parents, 'chunks/sidebar/sex.tpl', array('ms|sex','resource|parent'), $where);
    }

    private function brand()
    {
        if ($this->uri_count < 2) {
            $this->modx->log(1, print_r('Недостаточно параметров'.$this->uri_count, 1));
            return false;
        }

        $brand_uri = $this->uri[$this->uri_count - 1];
        if (empty($brand_uri)) {$brand_uri = $this->uri[$this->uri_count - 2];}
        if (empty($brand_uri)) {$this->modx->log(1, print_r('Недостаточно параметров для поиска бренда', 1));return false;}
        $page = $this->modx->getObject('modResource', array('uri' => $brand_uri));
        if (empty($page)) {
            $this->modx->log(1, print_r('Страница бренда не найдена ' . $brand_uri, 1));
            return false;
        }
        $vendor = $this->modx->getObject('msVendor', array('resource' => $page->get('id')));
        if (empty($vendor)) {
            $this->modx->log(1, print_r('Бренд не найден', 1));
            return false;
        }
        $where = '(`Data`.`vendor` = ' . $vendor->get('id') . ')';
        $this->sendForward($page->get('id'), $page, array($this->brand_url[$this->uri[0]]), 'chunks/sidebar/vendors.tpl', 'ms|vendor:vendors', $where);
    }

    /**
     * Общий метод установки необходимых плейсхолдеров
     * @param $obj
     * @param $parents
     * @param $chunk
     * @param $type
     * @param $where
     */
    private function setPlaceholders($obj, $parents, $chunk, $type, $where)
    {
        // Добавляем условие выборки
        $this->modx->setPlaceholder('rout_where', $where);
        // Добавляем хлебные крошки
        $this->modx->setPlaceholder('rout_breadcrumbs', $this->getBreadcrumbs($parents, $obj));
        // Устанавливаем тип сайтбара
        $this->modx->setPlaceholder('rout_sideBar', $chunk);
        /* Устанавливаем id страницы типа для сайтбара.
         * Например если это страница "Девочкам", то в сайтбаре будет производиться выборка по этому id
         */
        $this->modx->setPlaceholder('rout_typeId', $obj->get('id'));
        // Устанавливаем исключаемый фильтр
        $this->modx->setPlaceholder('rout_type', $type);
        //
        $this->modx->setPlaceholder('rout_bred_sub', $this->getBreadSub());
    }

    /**
     * Общий метод для подставки страницы
     * @param $page_id
     * @param $obj
     * @param $parents
     * @param $chunk
     * @param $type
     * @param string $where
     */
    private function sendForward($page_id, $obj, $parents, $chunk, $type, $where='')
    {
        $this->setPlaceholders($obj, $parents, $chunk, $type, $where);
        $this->modx->sendForward($page_id);
    }

    /**
     * Генерация хлебных крошек
     * @param $parents
     * @param $obj
     * @return array
     */
    private function getBreadcrumbs($parents, $obj)
    {
        foreach ($parents as $key => $id) {
            if ($id == $this->config['root_id'] || $id == $this->config['catalog_id']) {
                unset($parents[$key]);
            }
        }
        if (isset($this->brand_url[$this->uri[0]])) {
            $breadcrumbs = array($this->config['catalog_id'], $parents[0], $obj->get('id'));
        }
        else {
            $breadcrumbs = array($this->config['catalog_id'], $obj->get('id'));
            if (!empty($parents)) {
                $breadcrumbs = array_merge($breadcrumbs, $parents);
            }
        }

        return $breadcrumbs;
    }

    /**
     * Возвращает id ресурса, который необходимо сиключить из ховера хлебных крошек
     * @return int
     */
    private function getBreadSub()
    {
        $output = 0;
        if (isset($this->allowed_sex[$this->uri[0]])) {
            $output = $this->allowed_sex[$this->uri[0]];
        }

        if (isset($this->brand_url[$this->uri[0]])) {
            $output = $this->brand_url[$this->uri[0]];
        }

        return $output;
    }
}