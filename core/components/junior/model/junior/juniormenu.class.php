<?php

class JuniorMenu {
    public $modx;
    public $pdoTools;
    public $config;

    public function __construct(modX &$modx, array $config = array())
    {
        $this->modx =& $modx;

        $corePath = MODX_CORE_PATH . 'components/junior/';

        $this->config = array_merge(array(
            'modelPath' => $corePath . 'model/',
            'catalog_id' => 2
        ), $config);

        //$this->modx->addPackage('juniorMenu', $this->config['modelPath']);
        $this->modx->loadClass('junior.juniormenu', $this->config['modelPath'], true, true);

        $this->pdoTools = $this->modx->getService('pdoFetch');
    }

    public function initialize() {
        return true;
    }

    public function get()
    {
        return 'Test';
    }

    /**
     * Получает уникальные значения по указанному полю.
     * @param string $field
     * @return array
     */
    public function getUniqValues($field)
    {
        $q = $this->modx->newQuery('msProductData');
        $q->groupby($field);
        $res = $this->modx->getCollection('msProductData', $q);

        $output = array();
        if ($res) {
            foreach ($res as $item) {
                $prod = $item->toArray();
                if (empty($prod[$field])) {continue;}
                $output[] = $prod[$field];
            }
        }

        return $output;
    }

    public function getParentsOnField($field, $key)
    {
        $leftJoin = array(
            'Data' => array('class' => 'msProductData')
        );
        $select = array(
            'msProduct' => 'id,deleted,published,parent',
            'Data' => $field
        );
        $where = array(
            'class_key:=' => 'msProduct',
            'deleted:=' => '0',
            'published:=' => '1'
        );
        if ($field == 'sex') {
            $where[] = '(' . '`Data`.`' . $field . '`="' . $key . '" OR `Data`.`' . $field . '`="Унисекс")';
        }
        else {
            $where['Data.' . $field . ':='] = $key;
        }
        $options = array(
            'class' => 'msProduct',
            'return' => 'data',
            'parents' => '0',
            'leftJoin' => $leftJoin,
            'select' => $select,
            'where' => $where,
            'groupby' => 'parent',
            'limit' => 0,
            'depth' => 0
        );
        $this->pdoTools->setConfig($options, false);
        $res = $this->pdoTools->run();

        $ids_tree = array();
        foreach ($res as $item) {
            $ids = $this->modx->getParentIds($item['id']);

            $parents = array();
            foreach ($ids as $one) {
                if ($one <= $this->config['catalog_id']) {continue;}
                $parents[] = $one;
            }

            if (!empty($parents)) {
                $parents = array_reverse($parents);

                $parents_count = count($parents);
                if ($parents_count > 1) {
                    $ids_tree[$parents[0]][] = $parents[1];
                }
                else {
                    $ids_tree[$parents[0]] = $parents[0];
                }

                /*if ($key == 'Мальчикам' && count($ids) > 3) {
                    var_dump($parents);exit;
                }
                $ids_tree[] = $parents;*/
            }
        }


        /*$cat_tree = $this->modx->getTree(2);
        var_dump(array_search('2152', $cat_tree));
        var_dump($cat_tree);exit;*/
        /*$first_names = array_column($res, 'id');*/
        /*if ($key == 'Мальчикам') {
            var_dump($ids_tree);exit;
        }*/

        return $ids_tree;
    }
}