<?php

interface msCartInterface
{

    /**
     * Initializes cart to context
     * Here you can load custom javascript or styles
     *
     * @param string $ctx Context for initialization
     *
     * @return boolean
     */
    public function initialize($ctx = 'web');


    /**
     * Adds product to cart
     *
     * @param string $type Cart. Available values: size or row
     * @param integer $id Id of MODX resource. It must be an msProduct descendant
     * @param integer $count .A number of product exemplars
     * @param array $options Additional options of the product: color, size etc.
     *
     * @return array|string $response
     */
    public function add($type, $id, $count = 1, $options = array());


    /**
     * Removes product from cart
     *
     * @param string $type Cart
     * @param string $key The unique key of cart item
     *
     * @return array|string $response
     */
    public function remove($type, $key);


    /**
     * Changes products count in cart
     *
     * @param string $type Cart
     * @param string $key The unique key of cart item
     * @param integer $count .A number of product exemplars
     *
     * @return array|string $response
     */
    public function change($type, $key, $count, $success_message);


    /**
     * Cleans the cart
     *
     * @param string $type Cart
     *
     * @return array|string $response
     */
    public function clean($type);


    /**
     * Returns the cart status: number of items, weight, price.
     *
     * @param array $data Additional data to return with status
     *
     * @return array $status
     */
    public function status($data = array());


    /**
     * Returns the cart items
     *
     * @param string $type Cart
     *
     * @return array $cart
     */
    public function get($type = null);


    /**
     * Set all the cart items by one array
     *
     * @param string $type Cart
     * @param array $cart
     *
     * @return void
     */
    public function set($type, $cart = array());

}


class msCartHandlerDouble implements msCartInterface
{
    /** @var modX $modx */
    public $modx;
    /** @var miniShop2 $ms2 */
    public $ms2;
    /** @var array $config */
    public $config;
    /** @var array $cart */
    protected $cart;
    protected $ctx = 'web';
    public $allowed_type = array('size', 'row');
    protected $hasSessionContext;
    public $remains;

    /**
     * msCartHandler constructor.
     *
     * @param miniShop2 $ms2
     * @param array $config
     */
    function __construct(miniShop2 & $ms2, array $config = array())
    {
        $this->ms2 = &$ms2;
        $this->modx = &$ms2->modx;

        $corePath = $this->modx->getOption('mscart_core_path', $config, $this->modx->getOption('core_path') . 'components/mscart/');
        $this->config = array_merge(array(
            'cart' => & $_SESSION['minishop2']['cart'],
            'max_count' => $this->modx->getOption('ms2_cart_max_count', null, 1000, true),
            'allow_deleted' => false,
            'allow_unpublished' => false,
        ), $config);

        $this->modx->addPackage('mscart', $corePath . 'model/');
        $this->hasSessionContext = $this->modx->user->hasSessionContext('web');
        $this->cart =& $this->config['cart'];
        $this->modx->lexicon->load('minishop2:cart');
        if (!$this->modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {
            return false;
        }
        $this->remains = $this->modx->getService('remains');

        if (empty($this->cart) || !is_array($this->cart)) {
            $this->cart = array();
            foreach ($this->allowed_type as $item) {
                $this->cart[$item] = array();
            }
        }
        if ($this->hasSessionContext) {
            $this->restoreCart();
        }
    }

    /**
     * @param string $ctx
     *
     * @return bool
     */
    public function initialize($ctx = 'web')
    {
        $this->ctx = $ctx;
        return true;
    }

    /**
     * Восстанавливает корзину в сессию из Базы.
     */
    protected function restoreCart()
    {
        // TODO Заменить на $this->clean()
        $this->cart = array();
        $cart = $this->modx->getCollection('msCartObject', array('user_id' => $this->modx->user->get('id')));
        if (!empty($cart)){
            foreach ($cart as $item) {
                $type = $item->get('type');
                $key = $item->get('hash');
                if (!isset($this->cart[$type][$key])) {
                    $this->addToSession(array(
                        'type' => $type,
                        'key' => $key,
                        'id' => $item->get('product_id'),
                        'price' => $item->get('price'),
                        'weight' => $item->get('weight'),
                        'count' => $item->get('count'),
                        'options' => $item->get('options'),
                        'user_id' => $item->get('user_id')
                    ));
                }
            }
        }

    }

    /**
     * @param string $type Cart
     * @param int $id
     * @param int $count
     * @param array $options
     *
     * @return array|string
     */
    public function add($type, $id, $count = 1, $options = array())
    {
        if (!in_array($type, $this->allowed_type)) {
            return $this->error('Указан недопустимый тип корзины.');
        }
        if (empty($id) || !is_numeric($id)) {
            return $this->error('ms2_cart_add_err_id');
        }

        $count = intval($count);
        if (is_string($options)) {
            $options = json_decode($options, true);
        }
        if (!is_array($options)) {
            $options = array();
        }

        $filter = array('id' => $id);
        if (!$this->config['allow_deleted']) {
            $filter['deleted'] = 0;
        }
        if (!$this->config['allow_unpublished']) {
            $filter['published'] = 1;
        }

        $success_message = '';

        /** @var msProduct $product */
        if ($product = $this->modx->getObject('modResource', $filter)) {
            if (!($product instanceof msProduct)) {
                return $this->error('ms2_cart_add_err_product', $this->status());
            }
            if ($count > $this->config['max_count'] || $count <= 0) {
                return $this->error('ms2_cart_add_err_count', $this->status(), array('count' => $count));
            }

            /* You can prevent add of product to cart by adding some text to $modx->event->_output
              <?php
                    if ($modx->event->name = 'msOnBeforeAddToCart') {
                        $modx->event->output('Error');
                    }

            // Also you can modify $count and $options variables by add values to $this->modx->event->returnedValues
                <?php
                      if ($modx->event->name = 'msOnBeforeAddToCart') {
                        $values = & $modx->event->returnedValues;
                        $values['count'] = $count + 10;
                        $values['options'] = array('size' => '99');
                    }
            */

            $response = $this->ms2->invokeEvent('msOnBeforeAddToCart', array(
                'product' => $product,
                'count' => $count,
                'options' => $options,
                'cart' => $this,
            ));
            if (!($response['success'])) {
                return $this->error($response['message']);
            }
            $price = $this->getPrice($type, $product);
            $weight = $this->getWeight($type, $product);
            $count = $response['data']['count'];
            $options = $response['data']['options'];
            $user_id = $this->modx->user->get('id');

            $key = md5($id . $price . $weight . (json_encode($options)));

            $count_in_cart = $this->cart[$type][$key]['count'];
            $max_count = $this->getMaxCount($product, $type, $options);
            $new_count = $count_in_cart + $count;
            if ($new_count > $max_count) {
                $new_count = $max_count;
                $success_message = 'В корзину добавлено максимальное количество: ' . $new_count . ' шт.';
            }

            if (isset($this->cart[$type]) && array_key_exists($key, $this->cart[$type])) {
                return $this->change($type, $key, $new_count, $success_message);
            }
            else {
                if ($this->hasSessionContext) {
                    $this->addToBase(array(
                        'product_id' => $id,
                        'type' => $type,
                        'user_id' => $user_id,
                        'count' => $count,
                        'price' => $price,
                        'weight' => $weight,
                        'hash' => $key,
                        'ctx' => $this->modx->context->get('key'),
                        'options' => $options,
                    ));
                }
                $this->addToSession(array(
                    'type' => $type,
                    'key' => $key,
                    'id' => $id,
                    'price' => $price,
                    'weight' => $weight,
                    'count' => $count,
                    'options' => $options,
                    'user_id' => $user_id
                ));

                $response = $this->ms2->invokeEvent('msOnAddToCart', array('key' => $key, 'cart' => $this));
                if (!$response['success']) {
                    return $this->error($response['message']);
                }

                $success_message = 'ms2_cart_add_success';

                return $this->success(
                    $success_message,
                    $this->status(array('key' => $key)),
                    array('count' => $count));
            }
        }

        return $this->error('ms2_cart_add_err_nf', $this->status());
    }

    /**
     * @param msProduct $product
     * @param $type
     * @param array $options
     * @return int
     */
    public function getMaxCount($product, $type, $options = array()) {
        $count = 0;
        if (!in_array($type, $this->allowed_type)) {return $count;}
        if ($type == 'row') {
            $count = $product->get('available_rows');
        }
        else {
            $size = $options['size'];
            $pid = $product->get('id');
            $result = $this->modx->getObject('remainObject', array(
                'product_id' => $pid,
                'size' => $size
            ));
            if (!empty($result)) {
                $count = $result->get('count');
            }
        }

        return $count;
    }

    /**
     * @param array $data
     */
    public function addToBase($data)
    {
        $size = $this->modx->newObject('msCartObject', $data);
        $size->save();
    }

    /**
     * @param array $data
     */
    public function addToSession($data)
    {
        $this->cart[$data['type']][$data['key']] = array(
            'id' => $data['id'],
            'price' => $data['price'],
            'weight' => $data['weight'],
            'count' => $data['count'],
            'options' => $data['options'],
            'user_id' => $data['user_id'],
            'ctx' => $this->modx->context->get('key'),
        );
    }

    /**
     * @param string $type Cart
     * @param string $key
     *
     * @return array|string
     */
    public function remove($type, $key)
    {
        if (array_key_exists($key, $this->cart[$type])) {
            $response = $this->ms2->invokeEvent('msOnBeforeRemoveFromCart', array('key' => $key, 'cart' => $this, 'type' => $type));
            if (!$response['success']) {
                return $this->error($response['message']);
            }
            if ($this->hasSessionContext) {
                $this->removeInBase($type, $key);
            }
            unset($this->cart[$type][$key]);

            $response = $this->ms2->invokeEvent('msOnRemoveFromCart', array('key' => $key, 'cart' => $this, 'type' => $type));
            if (!$response['success']) {
                return $this->error($response['message']);
            }

            return $this->success('ms2_cart_remove_success', $this->status());
        } else {
            return $this->error('ms2_cart_remove_error');
        }
    }

    /**
     * @param $type
     * @param $key
     */
    public function removeInBase($type, $key)
    {
        $res = $this->modx->getObject('msCartObject', array(
            'user_id' => $this->modx->user->get('id'),
            'type' => $type,
            'hash' => $key
        ));
        if (!empty($res)) {
            $res->remove();
        }
    }

    /**
     * @param string $type Cart
     * @param string $key
     * @param int $count
     *
     * @return array|string
     */
    public function change($type, $key, $count, $success_message = '')
    {
        if (array_key_exists($key, $this->cart[$type])) {
            if ($count <= 0) {
                return $this->remove($type, $key);
            } else {
                if ($count > $this->config['max_count']) {
                    return $this->error('ms2_cart_add_err_count', $this->status(), array('count' => $count));
                } else {
                    $response = $this->ms2->invokeEvent('msOnBeforeChangeInCart',
                        array('key' => $key, 'count' => $count, 'cart' => $this, 'type' => $type));
                    if (!$response['success']) {
                        return $this->error($response['message']);
                    }

                    $count = $response['data']['count'];

                    $filter = array('id' => $this->cart[$type][$key]['id']);
                    if (!$this->config['allow_deleted']) {$filter['deleted'] = 0;}
                    if (!$this->config['allow_unpublished']) {$filter['published'] = 1;}
                    $product = $this->modx->getObject('modResource', $filter);
                    $options = isset($this->cart[$type][$key]['options']) ? $this->cart[$type][$key]['options'] : array();
                    $max_count = $this->getMaxCount($product, $type, $options);
                    if ($count > $max_count) {
                        $count = $max_count;
                        $success_message = 'В корзину добавлено максимальное количество: ' . $count . ' шт.';
                    }

                    if ($this->hasSessionContext) {
                        $this->changeInBase($type, $key, $count);
                    }
                    $this->cart[$type][$key]['count'] = $count;
                    $response = $this->ms2->invokeEvent('msOnChangeInCart',
                        array('key' => $key, 'count' => $count, 'cart' => $this, 'type' => $type));
                    if (!$response['success']) {
                        return $this->error($response['message']);
                    }
                }
            }
            $success_message = empty($success_message) ? 'ms2_cart_change_success' : $success_message;
            return $this->success($success_message, $this->status(array('key' => $key)),
                array('count' => $count));
        } else {
            return $this->error('ms2_cart_change_error', $this->status(array()));
        }
    }

    /**
     * @param $type
     * @param $key
     * @param $count
     */
    public function changeInBase($type, $key, $count)
    {
        $res = $this->modx->getObject('msCartObject', array(
            'user_id' => $this->modx->user->get('id'),
            'type' => $type,
            'hash' => $key
        ));
        $res->set('count', $count);
        $res->save();
    }

    /**
     * @param string $type Cart
     *
     * @return array|string
     */
    public function clean($type = null)
    {
        $response = $this->ms2->invokeEvent('msOnBeforeEmptyCart', array('cart' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }

        if ($this->hasSessionContext) {
            $this->cleanFromBase($type);
        }

        $this->cleanSession($type);

        $response = $this->ms2->invokeEvent('msOnEmptyCart', array('cart' => $this));
        if (!$response['success']) {
            return $this->error($response['message']);
        }

        return $this->success('ms2_cart_clean_success', $this->status());
    }

    /**
     * @param $type
     */
    public function cleanSession($type)
    {
        $items = $this->cart;
        if (!empty($type)) {
            $items = $this->cart[$type];
        }

        foreach ($items as $key => $item) {
            if (empty($item['ctx']) || $item['ctx'] == $this->ctx) {
                if (!empty($type)) {unset($this->cart[$type][$key]);}
                else {unset($this->cart[$key]);}
            }
        }
    }

    /**
     * @param $type
     */
    public function cleanFromBase($type)
    {
        $where = array(
            'user_id' => $this->modx->user->get('id')
        );
        if (!empty($type)) {
            $where['type'] = $type;
        }
        $q = $this->modx->newQuery('msCartObject');
        $q->where($where);
        $items = $this->modx->getCollection('msCartObject', $q);
        foreach ($items as $item) {
            $item->remove();
        }
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function status($data = array())
    {
        $status = array(
            'total_count' => 0,
            'total_cost' => 0,
            'total_weight' => 0,
        );
        foreach ($this->allowed_type as $type) {
            $status[$type] = array(
                'total_count' => 0,
                'total_cost' => 0,
                'total_weight' => 0,
            );

            foreach ($this->cart[$type] as $item) {
                if (empty($item['ctx']) || $item['ctx'] == $this->ctx) {
                    // Тотал по типам
                    $status[$type]['total_count'] += $item['count'];
                    $status[$type]['total_cost'] += $item['price'] * $item['count'];
                    $status[$type]['total_weight'] += $item['weight'] * $item['count'];
                    $status[$type]['allow_order'] = $status[$type]['total_cost'] >= $this->getMinOrder($type);
                    // Общий тотал
                    $status['total_count'] += $item['count'];
                    $status['total_cost'] += $item['price'] * $item['count'];
                    $status['total_weight'] += $item['weight'] * $item['count'];
                }
            }
        }
        $status = array_merge($data, $status);

        $response = $this->ms2->invokeEvent('msOnGetStatusCart', array(
            'status' => $status,
            'cart' => $this,
        ));
        if ($response['success']) {
            $status = $response['data']['status'];
        }
        return $status;
    }

    /**
     * Получение статуса аяксом.
     * Например для отображения или скрытия формы заказа.
     * @return array|string
     */
    public function getStatus()
    {
        $status = $this->status();
        return $this->success('', $status);
    }

    /**
     * @param $type
     * @return array
     */
    public function statusType($type)
    {
        $status = array(
            'total_count' => 0,
            'total_cost' => 0,
            'total_weight' => 0,
        );
        if (isset($this->cart[$type]) && in_array($type, $this->allowed_type)) {
            foreach ($this->cart[$type] as $item) {
                if (empty($item['ctx']) || $item['ctx'] == $this->ctx) {
                    $status['total_count'] += $item['count'];
                    $status['total_cost'] += $item['price'] * $item['count'];
                    $status['total_weight'] += $item['weight'] * $item['count'];
                }
            }
        }
        return $status;
    }

    /**
     * @param string $type Cart
     *
     * @return array
     */
    public function get($type = null)
    {
        $cart = array();
        $items = empty($type) ? $this->cart : $this->cart[$type];
        foreach ($items as $key => $item) {
            if (empty($item['ctx']) || $item['ctx'] == $this->ctx) {
                $cart[$key] = $item;
            }
        }

        return $cart;
    }

    /**
     * @param string $type Cart
     * @param array $cart
     */
    public function set($type, $cart = array())
    {
        $this->cart = $cart;
    }

    /**
     * @param $type
     * @param $product
     * @return float|int
     */
    public function getPrice($type, $product)
    {
        $price = $product->getPrice();
        if ($type == 'row') {
            $count_size = $product->get('count_in_row');
            $price *= $count_size;
        }
        return $price;
    }

    /**
     * @param $type
     * @param $product
     * @return mixed
     */
    public function getWeight($type, $product)
    {
        $weight = $product->getWeight();
        if ($type == 'row') {
            $count_size = $product->get('count_in_row');
            $weight *= $count_size;
        }
        return $weight;
    }

    /**
     * @param $type
     * @return string|string[]|null
     */
    public function getMinOrder($type)
    {
        $min = 0;
        if ($type == 'size') {
            $min = $this->modx->getOption('sum_min_reservation_' . $type, array(), 0);
        }
        else {
            $min = $this->modx->getOption('sum_min_order_' . $type, array(), 0);
        }
        return (int) preg_replace("/[^0-9+]/", '', $min);
    }

    public function getProductCost($type, $key)
    {
        $product = $this->cart[$type][$key];
        $data = array(
            'count' => $product['count'],
            'key' => $key,
            'cost' => $product['count'] * $product['price']
        );
        return $this->success('', $data);
    }

    /**
     * Shorthand for MS2 error method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function error($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->error($message, $data, $placeholders);
    }

    /**
     * Shorthand for MS2 success method
     *
     * @param string $message
     * @param array $data
     * @param array $placeholders
     *
     * @return array|string
     */
    public function success($message = '', $data = array(), $placeholders = array())
    {
        return $this->ms2->success($message, $data, $placeholders);
    }

}