<?php
$xpdo_meta_map['msCartObject'] = array(
    'package' => 'mscart',
    'version' => '1.1',
    'table' => 'mscart',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        'type' => null,
        'user_id' => null,
        'product_id' => null,
        'count' => 0,
        'price' => 0.0,
        'weight' => 0.0,
        'hash' => null,
        'ctx' => null,
        'options' => null
    ),
    'fieldMeta' => array(
        'type' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'user_id' => array(
            'dbtype' => 'integer',
            'precision' => '10',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => null,
        ),
        'product_id' => array(
            'dbtype' => 'integer',
            'precision' => '10',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => null,
        ),
        'count' => array(
            'dbtype' => 'int',
            'precision' => '10',
            'phptype' => 'integer',
            'attributes' => 'unsigned',
            'null' => true,
            'default' => 1,
        ),
        'price' => array(
            'dbtype' => 'decimal',
            'precision' => '12,2',
            'phptype' => 'float',
            'null' => true,
            'default' => 0.0,
        ),
        'weight' => array(
            'dbtype' => 'decimal',
            'precision' => '13,3',
            'phptype' => 'float',
            'null' => true,
            'default' => 0.0,
        ),
        'hash' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'ctx' => array(
            'dbtype' => 'varchar',
            'precision' => '255',
            'phptype' => 'string',
            'null' => true,
        ),
        'options' => array(
            'dbtype' => 'text',
            'phptype' => 'json',
            'null' => true,
        )
    ),
    'indexes' => array(
        'user_id' => array(
            'alias' => 'user_id',
            'primary' => false,
            'unique' => false,
            'type' => 'BTREE',
            'columns' =>
                array (
                    'user_id' =>
                        array (
                            'length' => '',
                            'collation' => 'A',
                            'null' => false,
                        ),
                ),
        )
    ),
    'aggregates' => array(
        'Product' => array(
            'class' => 'msProduct',
            'local' => 'product_id',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        ),
        'User' => array(
            'class' => 'modUser',
            'local' => 'user_id',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        )
    )
);