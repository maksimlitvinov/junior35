<?php
class sizeRangeFilter extends mse2FiltersHandler {

    public function getRemainsValues(array $fields, array $ids)
    {
        if (!$this->modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {
            return false;
        }
        /** @var Remains $remains */
        $remains = $this->modx->getService('remains');

        $filters = array();
        $q = $this->modx->newQuery('remainObject');
        $q->where(array(
            'product_id:IN' => $ids,
            'count:>' => '0'
        ));
        $q->select('product_id,' . implode(',', $fields));
        if ($q->prepare() && $q->stmt->execute()) {
            $this->modx->executedQueries++;
            while ($row = $q->stmt->fetch(PDO::FETCH_ASSOC)) {
                foreach ($row as $key => $value) {
                    $v = str_replace('"', '&quot;', trim($value));
                    if ($key == 'product_id') {continue;}
                    else if (isset($filters[$key][$value])) {
                        $filters[$key][$value][$row['product_id']] = $row['product_id'];
                    }
                    else {
                        $filters[$key][$value] = array($row['product_id'] => $row['product_id']);
                    }
                }
            }
        }
        else {
            $this->modx->log(modX::LOG_LEVEL_ERROR, "[mSearch2] Error on get filter params.\nQuery: ".$q->toSQL()."\nResponse: ".print_r($q->stmt->errorInfo(),1));
        }
        return $filters;
    }

    public function buildSizerangeFilter(array $values)
    {
        if (count($values) < 2 && empty($this->config['showEmptyFilters'])) {return array();}

        $results = array();
        foreach ($values as $value => $ids) {
            $value = $value <= 0 ? '0' : '1';
            if (!isset($results[$value])) {
                $results[$value] = array(
                    'title' => 'rrr',
                    'value' => $value,
                    'type' => 'availability',
                    'resource' => array()
                );
            }

            foreach ($ids as $id) {
                $results[$value]['resources'][] = $id;
            }
        }
        unset($results[0]);
        ksort($results);
        return $results;
    }

    public function filterSizerange(array $requested, array $values, array $ids) {
        $matched = array();

        $value = $requested[0];
        $tmp = array_flip($ids);
        foreach ($values as $number => $resources) {
            if ($value && $number > 0) {
                foreach ($resources as $id) {
                    if (isset($tmp[$id])) {
                        $matched[] = $id;
                    }
                }
            }
            elseif (!$value && $number <= 0) {
                foreach ($resources as $id) {
                    if (isset($tmp[$id])) {
                        $matched[] = $id;
                    }
                }
            }
        }

        return $matched;
    }
}