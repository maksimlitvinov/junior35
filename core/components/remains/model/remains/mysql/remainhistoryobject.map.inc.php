<?php
$xpdo_meta_map['remainHistoryObject'] = array(
    'package' => 'remains',
    'version' => '1.1',
    'table' => 'remains_history',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        'product_id' => null,
        'data' => null,
        'old_count' => null,
        'new_count' => null
    ),
    'fieldMeta' => array(
        'product_id' => array(
            'dbtype' => 'integer',
            'precision' => '11',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        ),
        'data' => array(
            'dbtype' => 'datetime',
            'phptype' => 'datetime',
            'null' => true,
            'default' => null
        ),
        'old_count' => array(
            'dbtype' => 'integer',
            'precision' => '11',
            'phptype' => 'integer',
            'null' => true,
            'default' => null
        ),
        'new_count' => array(
            'dbtype' => 'integer',
            'precision' => '11',
            'phptype' => 'integer',
            'null' => true,
            'default' => null
        ),
    ),
    'indexes' => array(),
    'aggregates' => array(
        'Product' => array(
            'class' => 'msProduct',
            'local' => 'product_id',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        )
    )
);