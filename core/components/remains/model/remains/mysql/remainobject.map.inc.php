<?php
$xpdo_meta_map['remainObject'] = array(
    'package' => 'remains',
    'version' => '1.1',
    'table' => 'remains',
    'extends' => 'xPDOSimpleObject',
    'fields' => array(
        'product_id' => 0,
        'size' => '',
        'count' => ''
    ),
    'fieldMeta' => array(
        'product_id' => array(
            'dbtype' => 'integer',
            'precision' => '10',
            'attributes' => 'unsigned',
            'phptype' => 'integer',
            'null' => true,
            'default' => 0,
        ),
        'size' => array(
            'dbtype' => 'varchar',
            'precision' => '100',
            'phptype' => 'string',
            'null' => false,
            'default' => ''
        ),
        'count' => array(
            'dbtype' => 'varchar',
            'precision' => '100',
            'phptype' => 'string',
            'null' => false,
            'default' => ''
        )
    ),
    'indexes' => array(),
    'aggregates' => array(
        'Product' => array(
            'class' => 'msProduct',
            'local' => 'product_id',
            'foreign' => 'id',
            'cardinality' => 'one',
            'owner' => 'foreign'
        )
    )
);