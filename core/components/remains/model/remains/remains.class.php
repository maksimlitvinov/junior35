<?php

class Remains {
    /** @var modX $modx */
    public $modx;
    /** @var array $config */
    public $config;

    /**
     * Remains constructor.
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX &$modx, array $config = array())
    {
        $this->modx = &$modx;

        $corePath = $this->modx->getOption('remains_core_path', $config, $this->modx->getOption('core_path') . 'components/remains/');
        $assetsUrl = $this->modx->getOption('remains_assets_url', $config, $this->modx->getOption('assets_url') . 'components/remains/');
        $connectorUrl = $assetsUrl . 'connector.php';

        $this->config = array_merge(array(
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $connectorUrl,
            'corePath' => $corePath,
            'modelPath' => $corePath . 'model/',
            'processorsPath' => $corePath . 'processors/'
        ), $config);

        $this->modx->addPackage('remains', $this->config['modelPath']);
        $this->modx->lexicon->load('remains:default');
    }

    public function emptyTrash(array $ids)
    {
        $q = $this->modx->newQuery('remainObject');
        $q->where(array(
            'product_id:IN' => $ids
        ));
        $items = $this->modx->getIterator('remainObject', $q);
        $items->rewind();
        if (!$items->valid()) {return true;}
        foreach ($items as $item) {
            $item->remove();
        }
    }
}