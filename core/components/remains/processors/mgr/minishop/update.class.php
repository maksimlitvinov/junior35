<?php
class remainsUpdateMsFieldsProcessor extends modProcessor {
    public $classKey = 'msProduct';

    public function process()
    {
        $props = $this->getProperties();
        $product = $this->modx->getObject('msProduct', $props['id']);
        $product->set($props['name'], $props['val']);
        $product->save();

        return $this->success();
    }
}

return 'remainsUpdateMsFieldsProcessor';