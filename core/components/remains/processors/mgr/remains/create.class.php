<?php

class remainsCreateProcessor extends modObjectCreateProcessor {
    public $objectType = 'remainObject';
    public $classKey = 'remainObject';
    public $languageTopics = array('remains');
    public $permission = 'new_document';

    public function beforeSet()
    {
        $required = array('product_id', 'size');
        foreach ($required as $tmp) {
            if (!$this->getProperty($tmp)) {
                //$this->addFieldError($tmp, $this->modx->lexicon('field_required'));
                $this->addFieldError($tmp, 'Обязательно для заполнения');
            }
        }
        return !$this->hasErrors();
    }
}

return 'remainsCreateProcessor';