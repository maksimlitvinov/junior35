<?php
class remainsGetProcessor extends modObjectGetProcessor {
    public $objectType = 'remainObject';
    public $classKey = 'remainObject';
    public $languageTopics = array('remains:default');
}

return 'remainsGetProcessor';