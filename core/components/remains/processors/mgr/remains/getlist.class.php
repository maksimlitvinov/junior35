<?php
class remainGetListProcessor extends modObjectGetListProcessor {
    public $objectType = 'remainObject';
    public $classKey = 'remainObject';
    public $defaultSortField = 'id';
    public $defaultSortDirection = 'DESC';

    public function prepareQueryBeforeCount(xPDOQuery $c)
    {
        $c->where(array(
            'product_id' => $this->getProperty('product_id')
        ));
        return $c;
    }

    public function prepareRow(xPDOObject $object)
    {
        $arr = $object->toArray();
        $arr['actions'] = array();

        $arr['actions'][] = array(
            'class' => '',
            'button' => true,
            'menu' => true,
            'icon' => 'edit',
            'type' => 'updateItem',
        );
        $arr['actions'][] = array(
            'class' => '',
            'button' => true,
            'menu' => true,
            'icon' => 'trash-o',
            'type' => 'removeItem',
        );

        return $arr;
    }
}

return 'remainGetListProcessor';