<?php
class remainsRemoveProcessor extends modProcessor {
    public $classKey = 'remainObject';


    /** {inheritDoc} */
    public function process() {
        if (!$ids = explode(',', $this->getProperty('ids'))) {
            return $this->failure('Некорректный id');
        }

        $remains = $this->modx->getIterator($this->classKey, array('id:IN' => $ids));
        foreach ($remains as $item) {
            $item->remove();
        }

        return $this->success();
    }

}

return 'remainsRemoveProcessor';