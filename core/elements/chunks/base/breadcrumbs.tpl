{var $tpl = '@INLINE <li><a data-id="{$id}" href="{$link}">{$menutitle}'}
{if $submenu | length > 0}
    {set $tpl = $tpl ~ '{if $id == 2 ?}<ul class="submenu">'}
    {foreach $submenu as $item}
        {set $tpl = $tpl ~ ('<li><a href="' ~ $item | url ~ '">' ~ ($item | resource: 'menutitle') ~ '</a></li>')}
    {/foreach}
    {set $tpl = $tpl ~ '</ul>{/if}'}
{/if}
{set $tpl = $tpl ~ '</a></li>'}

{'!pdoCrumbs' | snippet : [
    'showHome' => '1',
    'tplWrapper' => '@INLINE <nav class="breadcrumbs"><div class="container"><ul>{$output}</ul></div></nav>',
    'tpl' => $tpl,
    'tplCurrent' => '@INLINE <li>{$menutitle}</li>',
    'customParents' => $parents
]}