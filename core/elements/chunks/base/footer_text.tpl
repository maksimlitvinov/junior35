<div class="footer-text-block">
    <div class="container">
        <div class="header-wrapper">
            <h2 class="beautifull-h">Брендовая детская одежда оптом</h2>
            <a href="{'32' | url}" class="pink-color">{'32' | resource : 'longtitle'}</a>
        </div>
        <p>Ищете удобную и практичную детскую одежду? Хотите, чтобы вещи были дешевыми, но в то же время стильными? Детская одежда в интернет-магазине Junior35.ru с возможностью ее доставки в любую точку России – это лучшее решение для заботливых родителей и их непоседливых малышей.</p>
        <p>Наша компания занимается оптово-розничной продажей детской одежды и обуви от самых известных зарубежных производителей. Aosta Betty, Little Maven, Jumping Beans, Jumping Meters, Malwee, Phibee Kids, Carters, Jupa, Huppa, Breeze, Boinc, UOVO, Kigurumi, Cool Club, Zoe Flower, Crockid, Zara Kids, Mothercare, DKNY, Okaidi, Rebel и многие другие.</p>
    </div>
</div>