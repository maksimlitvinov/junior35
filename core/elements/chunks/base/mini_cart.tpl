<a id="msMiniCart" href="{'43' | url}" class="cart-wrapper">
    <div class="product-count ms2_total_count">{$total_count}</div>
    <img src="{'assets_url' | config}layout/app/img/icon_cart_empty.svg" alt="{'43' | resource : 'pagetitle'}">
    <div class="cart-amount">
        <span class="price ms2_total_cost">{$total_cost}</span> ₽
    </div>
</a>