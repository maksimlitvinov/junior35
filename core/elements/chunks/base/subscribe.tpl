<div class="subscribe-block">
    <div class="container">
        <h2 class="yellow-color violet-bg beautifull-h">Быть в курсе новостей</h2>
        {'AjaxForm' | snippet : [
        'form' => '@FILE chunks/forms/subscribe.tpl',
        'hooks' => 'notSpam,FormItSaveForm,email',
        'formFields' => 'name,useremail',
        'fieldNames' => 'name==Имя отправителя,useremail==Email',
        'formName' => 'Подписка на новости',
        'emailTpl' => '@FILE chunks/letters/subscribe.tpl',
        'emailTo' => ('client_email' | config),
        'emailFromName' => $_modx->getPlaceholder('name'),
        'emailReplyTo' => $_modx->getPlaceholder('useremail'),
        'emailSubject' => 'Подписка на новости',
        'submitVar' => 'subscribe',
        'validate' => 'name:required,useremail:required'
        ]}
    </div>
</div>