<div class="col-md-6 blog-block">
    <div class="image">
        <a href="{$uri}">
            <img src="{$img | phpthumbon : 'w=506&h=380&zc=1&q=70'}" alt="{$longtitle ?: $pagetitle}">
        </a>
    </div>
    <h2 class="h1"><a href="{$uri}">{$longtitle ?: $pagetitle}</a></h2>
    <p>{$introtext}</p>
    <p class="date">
        {'!dateAgo' | snippet : [
            'input' => $publishedon
        ]}
    </p>
    <a href="{$uri}" class="button readmore">
        <svg width="10" height="16" viewBox="0 0 10 16" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z" /></svg>
    </a>
</div>