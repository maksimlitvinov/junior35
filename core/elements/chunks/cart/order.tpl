<form class="ms2_form" id="msOrder" action="" method="post">
    <div class="ms2_oder_not_allowed">
        <button class="button bigger" disabled>Оформить заказ</button>
        {if $_modx->resource.alias == 'size'}
            {var $minSumToText = ('sum_min_reservation_' ~ $_modx->resource.alias) | config}
        {else}
            {var $minSumToText = ('sum_min_order_' ~ $_modx->resource.alias) | config}
        {/if}
        <p class="important">Минимальная сумма заказа <br> составляет {$minSumToText} ₽</p>
    </div>
    <div class="ms2_oder_allowed">
        <hr>
        <div id="payments">
            <p><strong>Способ оплаты</strong></p>
            {foreach $payments as $payment index=$index}
                {var $checked = !($order.payment in keys $payments) && $index == 0 || $payment.id == $order.payment}
                <label class="custom-radio">
                    <span class="text">{$payment.name}</span>
                    <input type="radio" name="payment" value="{$payment.id}" id="payment_{$payment.id}"{$checked ? 'checked' : ''}>
                    <span class="checkmark"></span>
                </label>
            {/foreach}
        </div>
        <hr>
        <div id="deliveries">
            <p><strong>Способ доставки</strong></p>
            <select name="delivery">
                {foreach $deliveries as $delivery index=$index}
                    <option value="{$delivery.id}">{$delivery.name}</option>
                {/foreach}
            </select>
            <a href="#" class="choise-point">Выбрать пункт выдачи</a>
        </div>
        <hr>
        <div class="header-wrapper">
            <strong>Данные заказчика</strong>
            <a class="change_user_fields" href="#">Изменить</a>
        </div>

        <div class="user_fields">
            {$_modx->lexicon->load('core:user')}
            {var $userFields = ['receiver','email','phone','city','address','passpotr_data']}
            {foreach $userFields as $field}
                {var $label = $_modx->lexicon($field)}
                {var $value = $_modx->user[$field]}
                {if $field == 'receiver'}
                    {*set $label = $_modx->lexicon('user_full_name')*}
                    {set $label = 'Ф.И.О.'}
                    {set $value = $_modx->user['fullname']}
                {elseif $field == 'phone'}
                    {set $label = $_modx->lexicon('user_mobile')}
                    {set $value = $_modx->user['mobilephone']}
                {/if}

                {if $value ?}
                    <p>
                        <span class="user_field"><strong>{$label}:</strong> <span class="normal-fw">{$value}</span></span>
                        <input type="hidden" name="{$field}" value="{$value}">
                    </p>
                {else}
                    <p>
                        <input type="text" name="{$field}" placeholder="{$label}">
                    </p>
                {/if}
            {/foreach}

            <input name="comment" type="text" placeholder="Примечание">
            <input name="promo_code" type="text" placeholder="Промокод">
        </div>
        <button type="submit" name="ms2_action" value="order/submit" class="button bigger">Оформить заказ</button>
        <div class="text-right">
            <input type="hidden" name="cart_type" value="{$_modx->resource.alias}">
            <a href="{'206' | url}">Как сделать заказ?</a>
        </div>
    </div>
</form>