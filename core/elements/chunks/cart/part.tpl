<div class="cart_part-of-order">
    <div class="text">
        <h2>{$title} ({$total_count})</h2>
        <p>Сумма заказа: <span class="price">{$total_cost} ₽</span></p>
    </div>
    <a href="{$link}">К заказу <span class="arrow">→</span></a>
</div>