<div class="msearch2" id="mse2_mfilter">
    <div class="container">
        <div class="header-wrapper">
            <h1 class="pink-color h2 beautifull-h">{$_modx->resource.pagetitle ?: $modx->resource.longtitle}</h1>
            <p class="pink-color header-wrapper_right-label">Найдено товаров: <span id="mse2_total" class="goods-count">[[+total:default=`0`]]</span></p>
        </div>
        {*<div class="catalogue-filters">
            <form action="[[~[[*id]]]]" method="post" id="mse2_filters">*}
                {*<label id="mse2_sort">
                    <a href="#" class="button price-sort-btn to-hight" data-sort="ms|price">
                        <span class="text">Цена возрастает</span>
                        <svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z"/></svg>
                    </a>
                </label>*}
                {*<div class="mfp-hide">
                    {if $.get['sex'] ?}
                        <input checked type="checkbox" name="sex" value="{$.get['sex']}">
                    {/if}
                </div>*}
                {*$filters*}
                {*<label for="mse2_ms_in_stock" class="custom-checkbox">
                    <input id="mse2_ms_in_stock" type="checkbox" name="in_stock" value="1,9999999">
                    <span>В наличии</span>
                </label>
                {if $_modx->resource.id != '79'}
                    <label for="mse2_ms_new" class="custom-checkbox">
                        <input id="mse2_ms_new" type="checkbox" name="new" value="1">
                        <span>Новинка</span>
                    </label>
                {/if}
                <label for="mse2_ms_old_price" class="custom-checkbox">
                    <input id="mse2_ms_old_price" type="checkbox" name="old_price" value="1,9999999">
                    <span>Акция</span>
                </label>
                <label for="mse2_ms_soon_available" class="custom-checkbox">
                    <input id="mse2_ms_soon_available" type="checkbox" name="soon_available" value="1">
                    <span>Скоро в продаже</span>
                </label>
                <label for="mse2_ms_without_rows" class="custom-checkbox">
                    <input id="mse2_ms_without_rows" type="checkbox" name="without_rows" value="1">
                    <span>Без рядов</span>
                </label>
                <div class="mfp-hide">
                    <input type="checkbox" {$.get['vendor'] ? 'checked' : ''} name="vendor" value="{$.get['vendor']}">
                    <input type="checkbox" {$.get['sex'] ? 'checked' : ''} name="sex" value="{$.get['sex']}">
                </div>*}
            {*</form>
        </div>*}
    </div>

    <div class="container catalogue-content-wrapper">
        <div class="menu-filter-wrapper">
            {var $sideBar = $_modx->getPlaceholder('sideBar')}
            {if $sideBar ?}
                <nav class="sidebar-catalogue-menu">
                    <ul>
                        {include ('file:' ~ $sideBar)}
                    </ul>
                    <div class="show-btn"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z" /></svg></div>
                </nav>
            {/if}
            <form  action="[[~[[*id]]]]" method="post" id="mse2_filters">
                <div class="filter-wrapper">

                    <div class="filter-block round-white-block">
                        {$filters}
                        {if $filters ?}
                            <button type="reset" class="reset-form like-link">Очистить фильтры</button>
                        {/if}
                    </div>
                </div>
            </form>
            <div class="show-btn"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z" /></svg></div>
        </div>

        <div class="content">

            <div class="round-white-block filters-row">
                <div class="filters" id="mse2_selected_wrapper">
                    <ul id="mse2_selected"></ul>
                </div>
                <button type="reset" class="clear like-link">Очистить фильтры</button>
            </div>

            <div id="mse2_results" class="product-cards-wrapper">
                {$results}
            </div>

        </div>

    </div>
    <div class="mse2_pagination">
        {$_modx->getPlaceholder('page.nav')}
    </div>
</div>