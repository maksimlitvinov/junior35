<div class="product-card">
    {if $old_price && $soon_available == 0 ?}
        <div class="bage pink-bg">
            Скидка
        </div>
    {/if}
    {if $new && $soon_available == 0 ?}
        <div class="bage yellow-bg">
            Новинка
        </div>
    {/if}
    {if $soon_available ?}
        <div class="bage violet-bg">
            Скоро
        </div>
    {/if}
    <div data-id="{$id}" class="add-to-favorites {$id | isFavorite ? 'added' : ''}">
        <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M11.5269 0C9.97853 0 8.60223 0.869565 7.91405 2.08696C7.13986 0.869565 5.84947 0 4.30108 0C1.89248 0 0 1.91304 0 4.34783C0 5.65217 0.602214 6.78261 1.46243 7.65217L7.56994 13.8261C7.65597 13.913 7.82801 14 8.00005 14C8.1721 14 8.25812 13.913 8.43016 13.8261L14.5377 7.65217C15.3979 6.86956 16 5.65217 16 4.34783C15.828 2 13.9355 0 11.5269 0Z"/></svg>
    </div>
    <a href="{$uri}"><img src="{$list}" alt="{$pagetitle}" class="product-image"></a>
    {*<a href="{$image}" class="img-popup"><img src="{$list}" alt="{$pagetitle}" class="product-image"></a>*}
    <div class="text">
        <a href="{$uri}" class="name">
            {if $longtitle ?}
                <strong>{$longtitle}</strong> / Арт. {$article}
            {else}
                <strong>{$pagetitle}</strong>
            {/if}
        </a>
        {if $available_sizes > 0 || $available_rows > 0}
            <div class="availability availability-yes">
                в наличии
            </div>
        {else}
            <div class="availability availability-no">
                нет в наличии
            </div>
        {/if}
    </div>
    <div class="price-button-wrapper">
        <div class="price-wrapper">
            {if $old_price ?}
                <span class="old-price"><span class="value">{$old_price}</span> ₽</span>
            {/if}
            <span class="price"><span class="value">{$price}</span> ₽</span>
        </div>
        <a href="{$uri}" class="button">Подробнее</a>
    </div>
</div>