{if $sizes ?}
    <table class="size-table">
        <tr>
            <th>Размер</th>
            <th>Наличие</th>
            <th></th>
        </tr>
        {var $checked = 'checked'}
        {var $sizes_for_row = []}
        {if ($.session['pageType'] | length == 0) || ($.session['pageType'] == 'sizes')}
            {foreach $sizes as $item}
                {set $sizes_for_row[] = $item.size}
            <tr>
                <td>{$item.size}</td>
                {if $item.count > 0}
                    <td>
                        <span class="product__size-count">{$item.count}</span>
                        <span class="">{$product.unit}</span>
                    </td>
                {else}
                    <td>нет</td>
                {/if}
                <td>
                    {if $item.count > 0}
                        <label class="custom-radio">
                            <input data-count="{$item.count}" value="{$item.size}" data-type="size" type="radio" name="options[size]" {$checked}>
                            {if $checked | length > 0}
                                {set $checked = ''}
                                {$_modx->setPlaceholder('max_count', $item.count)}
                                {$_modx->setPlaceholder('cart_type', 'size')}
                            {/if}
                            <span class="checkmark"></span>
                        </label>
                    {else}
                        {*<a class="inform" href="#">Сообщить</a>*}
                    {/if}
                </td>
            </tr>
            {/foreach}
        {/if}
        {if ($.session['pageType'] | length == 0) || ($.session['pageType'] == 'rows')}
            {if $product.available_rows > 0 ?}
                <tr>
                    <td colspan="2" class="pink-color strong">Весь размерный ряд</td>
                    <td>
                        <label class="custom-radio">
                            <input data-count="{$product.available_rows}" value="all" data-type="row" {$checked} type="radio" name="options[size]">
                            {if $checked | length > 0}
                                {set $checked = ''}
                                {$_modx->setPlaceholder('max_count', $product.available_rows)}
                                {$_modx->setPlaceholder('cart_type', 'row')}
                            {/if}
                            <span class="checkmark"></span>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <p>Доступно рядов: {$product.available_rows} {$sizes_for_row | length > 0 ? ('<br />(Размеры: ' ~ ($sizes_for_row | join) ~ ')') : ''}</p>
                    </td>
                </tr>
            {/if}
        {/if}
    </table>

{/if}