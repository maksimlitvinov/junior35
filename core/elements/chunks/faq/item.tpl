<div class="spoiler-block">
	<p class="spoiler-title">{$longtitle ?: $pagetitle}</p>
	<div class="spoiler-content">
		{$content}
	</div>
</div>