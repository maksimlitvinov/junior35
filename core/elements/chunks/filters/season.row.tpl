<label class="custom-checkbox-usual {$disabled}" for="mse2_{$table}{$delimeter}{$filter}_{$idx}">
    <span class="text">{$title} (<sup>{$num}</sup>)</span>
    <input type="checkbox" id="mse2_{$table}{$delimeter}{$filter}_{$idx}" name="{$filter_key}" value="{$value}" {$checked}>
    <span class="checkmark"></span>
</label>