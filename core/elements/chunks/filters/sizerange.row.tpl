<label class="{$disabled}" for="mse2_{$table}{$delimeter}{$filter}_{$idx}">
    {var $title = 'mse2_filter_ms_' ~ $filter}
    <span class="text">{$_modx->lexicon($title)} ({$num})</span>
    <input type="checkbox" id="mse2_{$table}{$delimeter}{$filter}_{$idx}" name="{$filter_key}" value="{$value}" {$checked}>
    <span class="checkmark"></span>
</label>