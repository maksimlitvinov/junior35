<div class="spoiler-block" id="mse2_{$table}{$delimeter}{$filter}">
    <p class="spoiler-title">[[%mse2_filter_[[+table]]_[[+filter]]]]</p>
    <div data-simplebar class="spoiler-content {$total < 8 ? '' : 'own-scrollbar'}">{$rows}</div>
</div>