<label class="custom-checkbox-usual" for="mse2_{$table}{$delimeter}{$filter}_{$idx}">
    {if $filter == 'parent'}
        {set $title = $title | resource : 'pagetitle'}
    {/if}
    <span class="text">{$title} <sup>({$num})</sup></span>
    <input type="checkbox" id="mse2_{$table}{$delimeter}{$filter}_{$idx}" name="{$filter_key}" value="{$value}" {$checked}>
    <span class="checkmark"></span>
</label>