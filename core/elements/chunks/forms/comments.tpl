<div class="popup-block mfp-hide" id="review-popup">
    <div id="comment-form-placeholder">
        <form id="comment-form" action="" method="post">
            <h3>Отзыв</h3>
            <p><strong>{$_modx->resource.longtitle}</strong> / Арт. {$_modx->resource.article}</p>
            <input type="hidden" name="thread" value="{$thread}"/>
            <input type="hidden" name="parent" value="0"/>
            <input type="hidden" name="id" value="0"/>
            <input type="hidden" name="form_key" value="{$formkey}">
            <input type="text" placeholder="Имя" value="{$_modx->user.fullname}">
            <input type="email" placeholder="Email" value="{$_modx->user.email}">
            <textarea name="text" placeholder="Отзыв"></textarea>
            <p>Нажимая на кнопку "Отправить", я даю согласие на <a href="#">обработку персональных данных</a></p>
            <input type="submit" href="#" class="button" value="Отправить">
        </form>
    </div>
</div>