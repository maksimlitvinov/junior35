<div class="popup-block mfp-hide" id="login-popup">
    <form class="office-form" action="{$_modx->resource.uri}" method="post">
        <h3>Вход</h3>
        <input type="text" name="username" placeholder="Логин или E-mail">
        <input type="password" name="password" placeholder="Пароль">
        <a href="#forget-password-popup" class="popup-btn forget-password">Забыли пароль?</a>
        {*<a href="{'41' | url}" class="forget-password">Забыли пароль?</a>*}
        <div class="buttons-wrapper">
            <input type="hidden" name="action" value="auth/formLogin"/>
            <input type="hidden" name="return" value=""/>
            <button type="submit" class="button enter-btn">Войти</button>
            <a href="#register-popup" class="register-btn popup-btn">Регистрация</a>
        </div>
    </form>
</div>

<div class="popup-block mfp-hide" id="forget-password-popup">
    <form class="office-form" action="">
        <h3>Восстановление пароля</h3>
        <p>Введите адрес электронной почты, указанный Вами при регистрации. </p>
        <input type="text" name="username" placeholder="Логин или E-mail">
        <p class="error"></p>
        <input type="hidden" name="action" value="auth/formLogin"/>
        <input type="hidden" name="return" value=""/>
        <button type="submit" class="button ">Восстановить пароль</button>
    </form>

    <div style="display: none;" class="success-text">
        <h3>Запрос отправлен</h3>
        <p>На Ваш e-mail будут высланы инструкции по восстановлению пароля</p>
        <!-- <button>Ок</button> -->
    </div>
</div>

<div class="popup-block mfp-hide" id="register-popup">
    <form class="office-form" action="{$_modx->resource.uri}" method="post">
        <h3>Регистрация</h3>
        <input name="fullname" type="text" placeholder="ФИО">
        <div class="two-cols">
            <input name="mobilephone" type="tel" placeholder="Телефон">
            <input name="email" type="email" placeholder="Email">
        </div>
        <input type="text" name="username" placeholder="Логин" value=""/>
        <small class="small-text">Если не заполнить, то логином будет являться почта</small>
        <div class="two-cols">
            <input name="password" type="password" placeholder="Пароль">
        </div>
        <p class="small-text">Нажимая на кнопку «Зарегистрироваться», вы соглашаетесь с <a target="_blank" class="link" href="{'85' | url}">Пользовательским соглашением и условиями продажи.</a></p>
        <div class="buttons-wrapper">
            <input type="hidden" name="action" value="auth/formRegister"/>
            <button type="submit" class="button register-btn">Зарегистрироваться</button>
            <a href="#login-popup" class="enter-btn popup-btn">Уже зарегистрированны?</a>
        </div>

    </form>
</div>