<form action="" method="post" id="office-profile-form">
    <input type="hidden" name="username" value="{$username}">
    <input type="text" name="fullname" value="{$fullname}" placeholder="ФИО" title="ФИО">
    <div class="two-cols">
        <input type="text" name="mobilephone" value="{$mobilephone}" placeholder="телефон" title="телефон">
        <input type="email" name="email" value="{$email}" placeholder="email" title="email">
    </div>
    <input type="text" name="passpotr_data" value="{$passpotr_data}" placeholder="Паспорт (серия, номер)" title="Паспорт (серия, номер)">
    <input type="text" name="city" value="{$city}" placeholder="Город доставки" title="Город доставки">
    <input type="text" name="address" value="{$address}" placeholder="Адрес доставки" title="Адрес доставки">
    <div class="password-wrapper">
        <input type="password" name="specifiedpassword" placeholder="Пароль" title="Пароль">
        <div class="show-password-btn" title="показать/скрыть пароль"></div>
    </div>
    <button type="submit">Сохранить</button>
</form>