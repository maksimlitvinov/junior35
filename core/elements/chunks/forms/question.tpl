<div class="popup-block mfp-hide" id="ask-question-popup">
    <form>
        <h3>Задать вопрос</h3>
        <input type="text" placeholder="Имя" title="Имя" name="name">
        <input type="email" placeholder="Email" title="Email" name="email">
        <input type="email" placeholder="Email" title="Email" name="useremail">
        <textarea placeholder="Вопрос" title="Вопрос" name="message"></textarea>
        <p class="small-text">Нажимая на кнопку "Отправить", я даю согласие на <a href="{'85' | url}">обработку персональных данных</a></p>
        <input type="hidden" name="question" value="1">
        <button name="submit" type="submit">Отправить</button>
    </form>
</div>