<div class="row" id="office-auth-form">
    <div class="col-md-12 office-auth-login-wrapper">
        <h1>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>

        <form style="background-color: #FFF;padding-top: 15px;padding-bottom: 15px;" method="post" class="form-horizontal office-form" id="office-auth-login">
            <div class="form-group">
                {*<label for="office-auth-login-email" class="col-md-3 control-label">
                    {'office_auth_login_username' | lexicon}&nbsp;<span class="red">*</span>
                </label>*}
                <div class="col-md-12">
                    <input style="width: 100%;" type="text" name="username" placeholder="{'office_auth_login_username' | lexicon}" class="form-control"
                           id="office-auth-login-username" value=""/>
                    <p class="help-block">
                        <small>{'office_auth_login_username_desc' | lexicon}</small>
                    </p>
                </div>
            </div>
            {*<div class="form-group hidden">
                <label for="office-auth-login-phone-code" class="col-md-3 control-label">
                    {'office_auth_login_phone_code' | lexicon}
                </label>
                <div class="col-md-8">
                    <input type="text" name="phone_code" class="form-control" id="office-auth-login-phone-code"
                           value="" readonly/>
                    <p class="help-block">
                        <small>{'office_auth_login_phone_code_desc' | lexicon}</small>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label for="office-auth-login-password" class="col-md-3 control-label">
                    {'office_auth_login_password' | lexicon}
                </label>
                <div class="col-md-8">
                    <input type="password" name="password" placeholder="" class="form-control"
                           id="office-login-form-password" value=""/>
                    <p class="help-block">
                        <small>{'office_auth_login_password_desc' | lexicon}</small>
                    </p>
                </div>
            </div>*}
            <div class="form-group">
                <input type="hidden" name="action" value="auth/formLogin"/>
                <input type="hidden" name="return" value=""/>
                <div class="col-sm-offset-3 col-sm-8">
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
            </div>
        </form>

        {if $providers?}
            <label>{'office_auth_login_ha' | lexicon}</label>
            <div>{$providers}</div>
            <p class="help-block">
                <small>{'office_auth_login_ha_desc' | lexicon}</small>
            </p>
        {/if}
        {if $error?}
            <div class="alert alert-block alert-danger alert-error">{$error}</div>
        {/if}
    </div>
</div>