<div class="review-block {$parent > 0 ? 'answer' : ''} ticket-comment{$comment_new}"  id="comment-{$id}" data-parent="{$parent}"
     data-newparent="{$new_parent}" data-id="{$id}">
    <div class="top-part">
        <div class="img">
            <img src="{$avatar}" alt="{$fullname}">
        </div>
        <div class="text">
            <p class="small-text">
                {*<span class="date">22 января 2019 </span>*}
                <span class="date">{$date_ago} </span>
                {*<span class="time pink-color">11:58:46</span>*}
            </p>
            <p class="big-text strong">{$fullname}</p>
        </div>
    </div>
    <div class="bottom-part">
        <p>{$text}</p>
    </div>
</div>
{$children}