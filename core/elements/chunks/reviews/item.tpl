<div class="col-md-6">
    <div class="review-block">
        <div class="top-part">
            <div class="img">
                <img src="/assets/layout/app/img/icon_review.png" alt="">
            </div>
            <div class="text">
                <p class="small-text">
                    <span class="date">
                        {'!dateAgo' | snippet : [
                            'input' => $publishedon,
                            'dateFormat' => 'd F Y'
                        ]}
                    </span>
                    <span class="time pink-color">{$publishedon | date : 'H:i'}</span>
                </p>
                <p class="big-text strong">{$pagetitle}</p>
            </div>
        </div>
        <div class="bottom-part">
            {$content}
        </div>
    </div>
</div>