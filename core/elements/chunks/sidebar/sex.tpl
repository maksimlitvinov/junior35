{var $rid = $_modx->getPlaceholder('rout_typeId') ?: $_modx->resource.id}
{if $rid == '76' ?}
    {*  Мальчикам *}
    {var $icon = '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.8205 5.74905L15.2892 0.282593C15.1407 0.103607 14.9203 0 14.6877 0C11.7534 0 12.3818 2.96738 10.0002 2.96738C7.64929 2.96738 8.21066 0 5.31271 0C5.08016 0 4.85967 0.103607 4.7112 0.282593L0.179954 5.74905C-0.0490805 6.02524 -0.060372 6.42212 0.152641 6.71112L2.34014 9.6785C2.62823 10.0694 3.20394 10.1077 3.53963 9.74854L4.53146 8.68759V19.2188C4.53146 19.6503 4.88119 20 5.31271 20H8.51583C8.94735 20 9.29708 19.6503 9.29708 19.2188V17.0326H10.7033V19.2188C10.7033 19.6503 11.0531 20 11.4846 20H14.6877C15.1192 20 15.469 19.6503 15.469 19.2188V8.68759L16.4608 9.74854C16.794 10.1051 17.3704 10.0719 17.6603 9.6785L19.8478 6.71112C20.0608 6.42212 20.0495 6.02524 19.8205 5.74905ZM16.9604 7.99484L15.2584 6.17432C14.7759 5.65826 13.9065 6.00006 13.9065 6.70792V18.4375H12.2658V16.2514C12.2658 15.8199 11.9161 15.4701 11.4846 15.4701H8.51583C8.08431 15.4701 7.73458 15.8199 7.73458 16.2514V18.4375H6.09396V6.70792C6.09396 6.00143 5.22543 5.6572 4.74203 6.17432L3.04006 7.99484L1.77282 6.27579L5.64672 1.60233C7.01788 1.9339 6.9103 4.52988 10.0002 4.52988C13.1054 4.52988 12.9751 1.93573 14.3535 1.60233L18.2274 6.27579L16.9604 7.99484Z" /></svg>'}
{elseif $rid == '77'}
    {* Девочкам *}
    {var $icon = '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19.8593 5.7515L15.328 0.282745C15.1797 0.103607 14.9591 0 14.7265 0C11.7767 0 12.4322 2.96875 10.039 2.96875C7.69038 2.96875 8.24687 0 5.35152 0C5.11882 0 4.89833 0.103607 4.74986 0.282745L0.218612 5.7515C-0.0102696 6.02783 -0.0215611 6.42456 0.191299 6.71341L2.3788 9.68216C2.6675 10.074 3.24351 10.1106 3.5786 9.7522L4.16438 9.12521L2.94322 16.5807C2.90126 16.8369 2.9893 17.097 3.17805 17.2751C5.04069 19.0323 7.47722 20 10.039 20C12.6007 20 15.0373 19.0323 16.9 17.2751C17.0887 17.097 17.1768 16.8367 17.1348 16.5807L15.9136 9.12537L16.4994 9.7522C16.8359 10.1122 17.4116 10.0725 17.6992 9.68216L19.8867 6.71341C20.0996 6.42456 20.0883 6.02783 19.8593 5.7515ZM16.9992 7.99835L15.2973 6.17706C14.7714 5.61432 13.831 6.07697 13.9555 6.83685L15.5238 16.4119C13.9958 17.7211 12.0637 18.4375 10.039 18.4375C8.01417 18.4375 6.08211 17.7211 4.55409 16.4119L6.12239 6.83685C6.24706 6.07666 5.3065 5.61447 4.78068 6.17706L3.07887 7.99835L1.81148 6.27823L5.68568 1.60248C7.09529 1.94351 6.87297 4.53125 10.039 4.53125C13.148 4.53125 13.0122 1.93634 14.3923 1.60248L18.2666 6.27823L16.9992 7.99835Z" /></svg>'}
{/if}

<li>
    {$icon}
    <a href="{$rid | url}">{$rid | resource : 'menutitle'}</a>
    {var $menuItems = '@FILE snippets/catalog/getMenu.php' | snippet : [
        'field' => 'sex',
        'resource' => $rid
    ]}
    {if $menuItems | length > 0}
        <ul class="submenu">
            {foreach $menuItems as $key => $item}
                {if $item | iterable}
                    <li class="has-second-submenu {$_modx->resource.id == $key ? 'active' : ''}">
                        <a href="{$rid | resource : 'alias'}/{$key | url}">{$key | resource : 'menutitle'}</a>
                        <ul class="second-submenu">
                            {foreach $item as $id}
                                <li {$_modx->resource.id == $id ? 'class="active"' : ''}><a href="{$rid | resource : 'alias'}/{$id | url}">{$id | resource : 'menutitle'}</a></li>
                            {/foreach}
                        </ul>
                    </li>
                {else}
                    <li {$_modx->resource.id == $item ? 'class="active"' : ''}><a href="{$rid | resource : 'alias'}/{$item | url}">{$item | resource : 'menutitle'}</a></li>
                {/if}
            {/foreach}
        </ul>
    {/if}
</li>