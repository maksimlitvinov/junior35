<?php
$date_format = $modx->getOption('date_format', $scriptProperties, 'd.m.Y');
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$date_start = $modx->getOption('date', $scriptProperties, date($date_format));
$type = $modx->getOption('type', $scriptProperties, null);
$reserv_days = $modx->getOption('reserv_day_' . $type, $scriptProperties, 0);
if (empty($type) || empty($reserv_days)) {return false;}
$date_end = date( $date_format, strtotime( $date_start . ' +' . $reserv_days . ' days' ) );
if (empty($tpl)) {return $date_end;}
$pdoTools = $modx->getService('pdoTools');
return $pdoTools->getChunk($tpl, array('date_end' => $date_end));