<?php
/** @var modX $modx */
/** @var array $scriptProperties */
/** @var miniShop2 $miniShop2 */
/** @var pdoFetch $pdoFetch */
/** @var Remains $remains */
$pdoFetch = $modx->getService('pdoFetch');
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
if (!$modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {
    return false;
}
$remains = $modx->getService('remains');

$tpl = $modx->getOption('tpl', $scriptProperties, null);
$type = $modx->getOption('type', $scriptProperties, null);
if (empty($type)) {return false;}

$status = $miniShop2->cart->statusType($type);
$cart = $miniShop2->cart->get($type);

// Подготавливаем общую информацию о карзине
if ($status['total_count'] > 0) {
    $status['total_cost'] = $miniShop2->formatPrice($status['total_cost']);
    $status['total_weight'] = $miniShop2->formatWeight($status['total_cost']);
}

// Подготавливаем товары
$where = array(
    'msProduct.id:IN' => array()
);
// Include products properties
$leftJoin = array(
    'Data' => array(
        'class' => 'msProductData',
    ),
    'Vendor' => array(
        'class' => 'msVendor',
        'on' => 'Data.vendor = Vendor.id',
    ),
);
// Select columns
$select = array(
    'msProduct' => !empty($includeContent)
        ? $modx->getSelectColumns('msProduct', 'msProduct')
        : $modx->getSelectColumns('msProduct', 'msProduct', '', array('content'), true),
    'Data' => $modx->getSelectColumns('msProductData', 'Data', '', array('id'), true),
    'Vendor' => $modx->getSelectColumns('msVendor', 'Vendor', 'vendor.', array('id'), true),
);
// Include products thumbnails
if (!empty($includeThumbs)) {
    $thumbs = array_map('trim', explode(',', $includeThumbs));
    if (!empty($thumbs[0])) {
        foreach ($thumbs as $thumb) {
            $leftJoin[$thumb] = array(
                'class' => 'msProductFile',
                'on' => "`{$thumb}`.product_id = msProduct.id AND `{$thumb}`.parent != 0 AND `{$thumb}`.path LIKE '%/{$thumb}/%' AND `{$thumb}`.rank = 0",
            );
            $select[$thumb] = "`{$thumb}`.url as '{$thumb}'";
        }
        $pdoFetch->addTime('Included list of thumbnails: <b>' . implode(', ', $thumbs) . '</b>.');
    }
}

foreach ($cart as $item) {
    $where['msProduct.id:IN'][] = $item['id'];
}
$where['msProduct.id:IN'] = array_unique($where['msProduct.id:IN']);
// Add user parameters
foreach (array('where', 'leftJoin', 'select') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = $scriptProperties[$v];
        if (!is_array($tmp)) {
            $tmp = json_decode($tmp, true);
        }
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}
$default = array(
    'class' => 'msProduct',
    'where' => $where,
    'leftJoin' => $leftJoin,
    'select' => $select,
    'sortby' => 'msProduct.id',
    'sortdir' => 'ASC',
    'groupby' => 'msProduct.id',
    'limit' => 0,
    'return' => 'data',
    'nestedChunkPrefix' => 'minishop2_',
);
// Merge all properties and run!
$pdoFetch->setConfig(array_merge($default, $scriptProperties), false);
$res = $pdoFetch->run();
$tmp = array();
foreach ($res as $one) {
    $tmp[$one['id']] = $one;
}
$products = array();
$tottal = array();
foreach ($cart as $hash => $item) {
    if (!isset($tmp[$item['id']])) {continue;}
    $product = $tmp[$item['id']];

    $product['hash'] = $hash;
    $product['count'] = $item['count'];
    $product['old_price'] = $miniShop2->formatPrice(
        $product['price'] > $item['price']
            ? $product['price']
            : $product['old_price']
    );
    $product['price'] = $miniShop2->formatPrice($item['price']);
    $product['weight'] = $miniShop2->formatWeight($item['weight']);
    $product['cost'] = $miniShop2->formatPrice($item['count'] * $item['price']);

    // Additional properties of product in cart
    if (!empty($item['options']) && is_array($item['options'])) {
        $product['options'] = $item['options'];
        foreach ($item['options'] as $option => $value) {
            $product['option.' . $option] = $value;
        }
    }

    $product['max_count'] = 0;
    if ($type == 'size') {
        $remObj = $modx->getObject('remainObject', array(
            'product_id' => $product['id'],
            'size' => $product['option.size']
        ));
        if ($remObj) {
            $product['max_count'] = $remObj->get('count');
        }
    }
    elseif ($type == 'row') {
        $product['max_count'] = $product['available_rows'];
    }

    // Add option values
    $options = $modx->call('msProductData', 'loadOptions', array(&$modx, $product['id']));
    $products[] = array_merge($product, $options);

    // Count total
    $total['count'] += $item['count'];
    $total['cost'] += $item['count'] * $item['price'];
    $total['weight'] += $item['count'] * $item['weight'];
}

$output = array(
    'status' => $status,
    'products' => $products,
    'total' => $total
);

if (empty($tpl)) {
    return $output;
}

return $pdoFetch->getChunk($tpl, $output);