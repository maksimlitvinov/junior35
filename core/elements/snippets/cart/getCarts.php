<?php
/** @var modX $modx */
/** @var array $scriptProperties */
/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
/** @var pdoFetch $pdoFetch */
if (!$modx->loadClass('pdofetch', MODX_CORE_PATH . 'components/pdotools/model/pdotools/', false, true)) {
    return false;
}
$pdoFetch = new pdoFetch($modx, $scriptProperties);
$pdoFetch->addTime('pdoTools loaded.');

$tpl = $modx->getOption('tpl', $scriptProperties, 'tpl.msCart');

$cart = array(
    'row' => array(),
    'size' => array()
);
$cart = array_merge($cart, $miniShop2->cart->get());
$status = $miniShop2->cart->status();

$is_empty = !((boolean) $status['total_count']);

$types = $miniShop2->cart->allowed_type;
$statusType = array();
$tmp = array();
$products = array(
    'row' => array(),
    'size' => array()
);
$where = array(
    'msProduct.id:IN' => array()
);
$total = array();
$titles = array();
$links = array();
// Include products properties
$leftJoin = array(
    'Data' => array(
        'class' => 'msProductData',
    ),
    'Vendor' => array(
        'class' => 'msVendor',
        'on' => 'Data.vendor = Vendor.id',
    ),
);
// Select columns
$select = array(
    'msProduct' => !empty($includeContent)
        ? $modx->getSelectColumns('msProduct', 'msProduct')
        : $modx->getSelectColumns('msProduct', 'msProduct', '', array('content'), true),
    'Data' => $modx->getSelectColumns('msProductData', 'Data', '', array('id'), true),
    'Vendor' => $modx->getSelectColumns('msVendor', 'Vendor', 'vendor.', array('id'), true),
);
// Include products thumbnails
if (!empty($includeThumbs)) {
    $thumbs = array_map('trim', explode(',', $includeThumbs));
    if (!empty($thumbs[0])) {
        foreach ($thumbs as $thumb) {
            $leftJoin[$thumb] = array(
                'class' => 'msProductFile',
                'on' => "`{$thumb}`.product_id = msProduct.id AND `{$thumb}`.parent != 0 AND `{$thumb}`.path LIKE '%/{$thumb}/%' AND `{$thumb}`.rank = 0",
            );
            $select[$thumb] = "`{$thumb}`.url as '{$thumb}'";
        }
        $pdoFetch->addTime('Included list of thumbnails: <b>' . implode(', ', $thumbs) . '</b>.');
    }
}

foreach ($types as $type) {
    $statusType[$type] = $miniShop2->cart->statusType($type);
    $where['msProduct.id:IN'] = array();
    foreach ($cart[$type] as $item) {
        $where['msProduct.id:IN'][] = $item['id'];
    }
    $where['msProduct.id:IN'] = array_unique($where['msProduct.id:IN']);

    // Add user parameters
    foreach (array('where', 'leftJoin', 'select') as $v) {
        if (!empty($scriptProperties[$v])) {
            $tmp = $scriptProperties[$v];
            if (!is_array($tmp)) {
                $tmp = json_decode($tmp, true);
            }
            if (is_array($tmp)) {
                $$v = array_merge($$v, $tmp);
            }
        }
        unset($scriptProperties[$v]);
    }
    $pdoFetch->addTime('Conditions prepared');

    $default = array(
        'class' => 'msProduct',
        'where' => $where,
        'leftJoin' => $leftJoin,
        'select' => $select,
        'sortby' => 'msProduct.id',
        'sortdir' => 'ASC',
        'groupby' => 'msProduct.id',
        'limit' => 0,
        'return' => 'data',
        'nestedChunkPrefix' => 'minishop2_',
    );
    // Merge all properties and run!
    $pdoFetch->setConfig(array_merge($default, $scriptProperties), false);
    $res = $pdoFetch->run();

    // Подготавливаем товары
    foreach ($res as $one) {
        $tmp[$one['id']] = $one;
    }
    foreach ($cart[$type] as $hash => $item) {
        if (!isset($tmp[$item['id']])) {continue;}

        $product = $tmp[$item['id']];
        $product['hash'] = $hash;
        $product['count'] = $item['count'];
        $product['old_price'] = $miniShop2->formatPrice(
            $product['price'] > $item['price']
                ? $product['price']
                : $product['old_price']
        );
        $product['price'] = $miniShop2->formatPrice($item['price']);
        $product['weight'] = $miniShop2->formatWeight($item['weight']);
        $product['cost'] = $miniShop2->formatPrice($item['count'] * $item['price']);

        // Additional properties of product in cart
        if (!empty($item['options']) && is_array($item['options'])) {
            $product['options'] = $item['options'];
            foreach ($item['options'] as $option => $value) {
                $product['option.' . $option] = $value;
            }
        }

        // Add option values
        $options = $modx->call('msProductData', 'loadOptions', array(&$modx, $product['id']));
        $products[$type][] = array_merge($product, $options);
    }

    // Подготавливаем $total
    $total[$type] = array(
        'count' => $statusType[$type]['total_count'],
        'cost' => $miniShop2->formatPrice($statusType[$type]['total_cost']),
        'weight' => $miniShop2->formatWeight($statusType[$type]['total_weight'])
    );

    //$titles[$type] = $modx->lexicon('ms2_cart_title_' . $type);
    $obj = $modx->getObject('modResource', array('alias' => $type));
    $titles[$type] = $obj->get('pagetitle');
    $links[$type] = $obj->get('uri');
}

$output = $pdoFetch->getChunk($tpl, array(
    'total' => $total,
    'products' => $products,
    'is_empty' => $is_empty,
    'titles' => $titles,
    'links' => $links
));

if ($modx->user->hasSessionContext('mgr') && !empty($showLog)) {
    $output .= '<pre class="msCartLog">' . print_r($pdoFetch->getTime(), true) . '</pre>';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}