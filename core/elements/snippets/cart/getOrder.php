<?php

/** @var modX $modx */
/** @var array $scriptProperties */
/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
/** @var pdoFetch $pdoFetch */
if (!$modx->loadClass('pdofetch', MODX_CORE_PATH . 'components/pdotools/model/pdotools/', false, true)) {
    return false;
}
$pdoFetch = new pdoFetch($modx, $scriptProperties);
$pdoFetch->addTime('pdoTools loaded.');

if (!$modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {
    return false;
}

$orderId = $modx->getOption('orderId', $scriptProperties, null);
if (empty($orderId)) {return array();}
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$userId = $modx->getOption('userId', $scriptProperties, $modx->user->get('id'));
if (empty($userId)) {return array();}

$order = $modx->getObject('msOrder', array(
    'id' => $orderId,
    'user_id' => $userId
));

if (empty($order)) {return $order;}

$where = array(
    'msOrderProduct.order_id' => $order->get('id')
);
// Include products properties
$leftJoin = array(
    'msProduct' => array(
        'class' => 'msProduct',
        'on' => 'msProduct.id = msOrderProduct.product_id',
    ),
    'Data' => array(
        'class' => 'msProductData',
        'on' => 'msProduct.id = Data.id',
    ),
    'Vendor' => array(
        'class' => 'msVendor',
        'on' => 'Data.vendor = Vendor.id',
    ),
);
// Select columns
$select = array(
    'msProduct' => !empty($includeContent)
        ? $modx->getSelectColumns('msProduct', 'msProduct')
        : $modx->getSelectColumns('msProduct', 'msProduct', '', array('content'), true),
    'Data' => $modx->getSelectColumns('msProductData', 'Data', '', array('id'),
            true) . ',`Data`.`price` as `original_price`',
    'Vendor' => $modx->getSelectColumns('msVendor', 'Vendor', 'vendor.', array('id'), true),
    'OrderProduct' => $modx->getSelectColumns('msOrderProduct', 'msOrderProduct', '', array('id'), true).', `msOrderProduct`.`id` as `order_product_id`',
);
// Include products thumbnails
if (!empty($includeThumbs)) {
    $thumbs = array_map('trim', explode(',', $includeThumbs));
    if (!empty($thumbs[0])) {
        foreach ($thumbs as $thumb) {
            $leftJoin[$thumb] = array(
                'class' => 'msProductFile',
                'on' => "`{$thumb}`.product_id = msProduct.id AND `{$thumb}`.parent != 0 AND `{$thumb}`.path LIKE '%/{$thumb}/%'",
            );
            $select[$thumb] = "`{$thumb}`.url as '{$thumb}'";
        }
        $pdoFetch->addTime('Included list of thumbnails: <b>' . implode(', ', $thumbs) . '</b>.');
    }
}
// Tables for joining
$default = array(
    'class' => 'msOrderProduct',
    'where' => $where,
    'leftJoin' => $leftJoin,
    'select' => $select,
    'joinTVsTo' => 'msProduct',
    'sortby' => 'msOrderProduct.id',
    'sortdir' => 'asc',
    'groupby' => 'msOrderProduct.id',
    'fastMode' => false,
    'limit' => 0,
    'return' => 'data',
    'decodeJSON' => true
);
// Merge all properties and run!
$pdoFetch->setConfig(array_merge($default, $scriptProperties), true);
$products = $pdoFetch->run();

$products_count = 0;
foreach ($products as &$product) {

    $products_count += $product['count'];
    $max_count = 0;
    if ($order->get('cart_type') == 'row') {
        $max_count = $product['available_rows'];
    }
    else {
        $remains = $modx->getObject('remainObject', array(
            'product_id' => $product['id'],
            'size' => $product['options']['size']
        ));
        if (empty($remains)) {
            $max_count = 0;
        } else {
            $max_count = $remains->get('count');
        }
    }
    $product['max_count'] = $max_count;
    $product['price'] = $miniShop2->formatPrice($product['price']);
    $product['cost'] = $miniShop2->formatPrice($product['cost']);
    $product['weight'] = $miniShop2->formatWeight($product['weight']);
}

$status = ($payment = $order->getOne('Status')) ? $payment->toArray() : array();
$address = ($tmp = $order->getOne('Address')) ? $tmp->toArray() : array();
$delivery = ($tmp = $order->getOne('Delivery')) ? $tmp->toArray() : array();
$payment = ($payment = $order->getOne('Payment')) ? $payment->toArray() : array();

$order = $order->toArray();
if (!empty($order['shipment_date']) && strtotime($order['shipment_date']) < 0) {$order['shipment_date'] = null;}
if (!empty($order['payment_date']) && strtotime($order['payment_date']) < 0) {$order['payment_date'] = null;}
if (!empty($order['date_refund']) && strtotime($order['date_refund']) < 0) {$order['date_refund'] = null;}
if (!empty($order['date_end_reservation']) && strtotime($order['date_end_reservation']) < 0) {$order['date_end_reservation'] = null;}
$order['cost'] = $miniShop2->formatPrice($order['cost']);

$output = array(
    'order' => $order,
    'products_count' => $products_count,
    'products' => $products,
    'address' => $address,
    'delivery' => $delivery,
    'payment' => $payment,
    'status' => $status
);

if (empty($tpl)) {return $output;}

return $pdoFetch->getChunk($tpl, $output);