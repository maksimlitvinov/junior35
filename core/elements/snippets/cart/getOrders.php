<?php
/** @var modX $modx */
/** @var array $scriptProperties */
$uid = $modx->getOption('uid', $scriptProperties, 0);
$tpl = $modx->getOption('tpl', $scriptProperties, null);

if (empty($uid)) {
    $uid = $modx->user->get('id');
}
if (empty($uid)) {return array();}

/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
/** @var pdoFetch $pdoFetch */
$pdoFetch = $modx->getService('pdoFetch');

$where = array(
    'msOrder.user_id' => $uid
);

$leftJoin = array(
    'msOrderStatus' => array(
        'class' => 'msOrderStatus',
        'on' => 'msOrder.status = msOrderStatus.id',
    )
);
$select = array(
    'msOrder' => $modx->getSelectColumns('msOrder', 'msOrder'),
    'msOrderStatus' => 'name as status_name, color as status_color'
);

// Add user parameters
foreach (array('where', 'leftJoin', 'select') as $v) {
    if (!empty($scriptProperties[$v])) {
        $tmp = $scriptProperties[$v];
        if (!is_array($tmp)) {
            $tmp = json_decode($tmp, true);
        }
        if (is_array($tmp)) {
            $$v = array_merge($$v, $tmp);
        }
    }
    unset($scriptProperties[$v]);
}

$default = array(
    'class' => 'msOrder',
    'where' => $where,
    'leftJoin' => $leftJoin,
    'select' => $select,
    'sortby' => 'msOrder.createdon',
    'sortdir' => 'desc',
    'limit' => 0,
    'return' => 'data',
    'decodeJSON' => true
);
$pdoFetch->setConfig(array_merge($default, $scriptProperties), true);
$rows = $pdoFetch->run();

if (empty($tpl)) {return $rows;}

return $pdoFetch->getChunk($tpl, array('orders' => $rows));