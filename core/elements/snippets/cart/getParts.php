<?php
/** @var modX $modx */
/** @var array $scriptProperties */
$excludeType = $modx->getOption('excludeType', $scriptProperties, '');
$excludeType = explode(',', $excludeType);
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$type = $modx->resource->get('alias');
$excludeType[] = $type;

/** @var pdoFetch $pdoFetch */
$pdoFetch = $modx->getService('pdoFetch');


/** @var miniShop2 $miniShop2 */
$miniShop2 = $modx->getService('miniShop2');
$miniShop2->initialize($modx->context->key);
$types = $miniShop2->cart->allowed_type;

$where = array(
    'deleted' => 0,
    'published' => 1
);
$options = array(
    'class' => 'modDocument',
    'return' => 'data',
    'where' => $where
);
$pdoFetch->setConfig(array_merge($options, $scriptProperties), false);
$res = $pdoFetch->run();

if (empty($res)) {return false;}

$totals = array();
foreach ($res as $item) {
    if (!in_array($item['alias'], $types) || in_array($item['alias'], $excludeType)) {continue;}
    $one = $miniShop2->cart->statusType($item['alias']);
    if ($one['total_count'] == 0) {continue;}
    $one['title'] = empty($item['longtitle']) ? $item['pagetitle'] : $item['longtitle'];
    $one['link'] = $item['uri'];
    $one['total_cost'] = $miniShop2->formatPrice($one['total_cost']);
    $one['total_weight'] = $miniShop2->formatWeight($one['total_cost']);
    $totals[] = $one;
}

if (empty($tpl)) { return $totals;}

$output = '';
foreach ($totals as $one) {
    $output .= $pdoFetch->getChunk($tpl, $one);
}
return $output;