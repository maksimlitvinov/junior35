<?php
if (!$modx->loadClass('juniormenu', MODX_CORE_PATH . 'components/junior/model/junior/', true, true)) {
    return false;
}
$juniorMenu = new JuniorMenu($modx);

//$tpl = $modx->getOption('tpl', $scriptProperties, null);
$field = $modx->getOption('field', $scriptProperties, 'sex');
$resource = $modx->getOption('resource', $scriptProperties, null);

$res = $modx->getObject('modResource', $resource);

return $juniorMenu->getParentsOnField($field, $res->get('pagetitle'));

/*$menuHead = $juniorMenu->getUniqValues('sex');

$ids_tree = array();
foreach ($menuHead as $item) {
    $ids_tree[$item] = $juniorMenu->getParentsOnField($field, $item);
}*/

//var_dump($ids_tree);