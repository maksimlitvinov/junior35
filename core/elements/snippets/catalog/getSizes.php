<?php
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$pid = $modx->getOption('pid', $scriptProperties, null);

if (empty($pid)) {
    $pid = $modx->resource->get('id');
}
$product = $modx->getObject('modResource', $pid);
if ($product->get('class_key') != 'msProduct') {return false;}

if (!$modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {
    return false;
}
$remains = $modx->getService('remains');

$q= $modx->newQuery('remainObject');
$q->where(array(
    'product_id' => $pid
));
$result = $modx->getCollection('remainObject', $q);

$output = array(
    'sizes' => array(),
    'product' => array()
);
$i = 0;
foreach ($result as $item) {
    if ($i == 0) {
        $output['product'] = $item->getOne('Product')->toArray();
    }
    $output['sizes'][$i] = $item->toArray();
    $output['sizes'][$i]['idx'] = $i;
    $i++;
}

if (!empty($tpl)) {
    $pdoFetch = $modx->getService('pdoFetch');
    return $pdoFetch->getChunk($tpl, $output);
}

return $output;