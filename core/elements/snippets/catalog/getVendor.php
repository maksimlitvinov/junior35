<?php
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$resource = $modx->getOption('resource', $scriptProperties, array());

/** @var pdoFetch $pdoFetch */
$pdoFetch = $modx->getService('pdoFetch');

$where = array(
    'resource' => $resource
);
$vendor = $pdoFetch->getObject('msVendor', $where);

if (!empty($tpl)) {
    return $pdoFetch->getChunk($tpl, $vendor);
}

return $vendor;