<?php
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$pdoFetch = $modx->getService('pdoFetch');
$vendors = $pdoFetch->getCollection('msVendor');

if (!empty($tpl)) {
    $output = '';
    foreach ($vendors as $vendor) {
        $output .= $pdoFetch->getChunk($tpl, $vendor);
    }
    return $output;
}

return $vendors;