<?php
$tpl = $modx->getOption('tpl', $scriptProperties, null);
$type = $modx->getOption('type', $scriptProperties, null);
$pdoFetch = $modx->getService('pdoFetch');

$where = array(
    'published' => 1,
    'deleted' => 0
);
if (empty($type)) {
    $where['Data.vendor:!='] = 0;
}

$q = $modx->newQuery('msProduct');
$q->leftJoin('msProductData', 'Data');
$q->where($where);
$q->groupby('Data.vendor');
$vendors = $modx->getIterator('msProduct', $q);

if (!empty($tpl)) {
    $output = '';
} else {
    $output = array();
}

foreach ($vendors as $vendor) {
    if (!empty($tpl)) {
        $params = $vendor->toArray();
        $output .= $pdoFetch->getChunk($tpl, $params);
    } else {
        $output[] = array(
            'resource' => $vendor->get('vendor.resource'),
            'id' => $vendor->get('vendor.id'),
            'name' => $vendor->get('vendor.name'),
            'logo' => $vendor->get('vendor.logo')
        );
    }
}

return $output;