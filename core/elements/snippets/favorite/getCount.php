<?php
if (!$modx->loadClass('favorite', MODX_CORE_PATH . 'components/favorite/model/favorite/', true, true)) {
    return false;
}
/** @var Favorite $favorite */
$favorite = $modx->getService('favorite');

return $favorite->getCount();