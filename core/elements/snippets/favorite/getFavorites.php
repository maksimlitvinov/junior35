<?php
$returnIds = $modx->getOption('returnIds', $scriptProperties, null);

/** @var Favorite $favorite */
$favorite = $modx->getService('favorite');

$output = '';
$res = $favorite->getItems();
if (!empty($res)) {
    $output .= implode(',', $res);
}
return $output;