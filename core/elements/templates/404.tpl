{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="page_404">
{/block}

{block 'template'}
		<div class="container">
			<div class="wrapper">
				<img src="{$layout}img/404.png" class="img-responsive" alt="">
				<div class="text">
					<h1>404</h1>
					<p class="strong big-text">Ссылка привела на необитаемый остров.</p>
					<p class="strong big-text">Воспользуйтесь меню или <a href="{'site_url' | config}">вернитесь</a></p>
				</div>
			</div>
		</div>
{/block}