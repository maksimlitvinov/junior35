{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body>
{/block}

{block 'template'}
	<div class="container">
		<div class="header-wrapper">
			<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
		</div>

		<div class="content-with-image-left round-white-block">
			<div class="image">
				<img src="{$_modx->resource.img | phpthumbon : 'w=224&h=317&bg=FFFFFF&zc=1'}" alt="{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}" class="img-responsive">
			</div>
			<div class="content">
				{$_modx->resource.content}
			</div>
		</div>
	</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}

