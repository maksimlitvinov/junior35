{extends 'file:templates/base/base.tpl'}

{block 'btnLogin'}{/block}

{block 'template'}
    <div class="container">
        {'!officeAuth' | snippet : [
            'groups' => 'Buyers',
            'tplLogin' => '@FILE chunks/forms/restorePass.tpl',
            'loginResourceId' => '109'
        ]}
    </div>
{/block}

{block 'formLogin'}{/block}