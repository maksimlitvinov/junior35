{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="blog-inside-page">
{/block}

{block 'template'}
	<div class="container">
		<div class="header-wrapper">
			<h2 class="pink-color beautifull-h">{$_modx->resource.parent | resource : 'longtitle'}</h2>
			<a href="{$_modx->resource.parent | url}" class="pink-color strong">Все статьи</a>
		</div>

		<div class="content-with-sidebar round-white-block">
			<div class="content">
				<div class="img round-image">
					<img src="{$_modx->resource.img | phpthumbon : 'w=788&h=591&zc=1&q=70'}" alt="{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}">
				</div>
				<h1>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
				{$_modx->resource.content}
				<p class="small-text">
					{'!dateAgo' | snippet : [
						'input' => $publishedon
					]}
				</p>
				<div class="share">
					<!-- uSocial -->
					<script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
					<div class="uSocial-Share" data-pid="bade25067a2a0f919c23b6d4ae55c80d" data-type="share" data-options="round,style1,default,absolute,horizontal,size32,counter0" data-social="vk,fb,ok,twi" data-mobile="vi,wa,sms"></div>
					<!-- /uSocial -->
				</div>
			</div>
			<div class="sidebar">
				<nav>
					<ul>
						{var $where = '{"id:!=":"' ~ $_modx->resource.id ~ '"}'}
						{'!pdoResources' | snippet : [
							'parents' => $_modx->resource.parent,
							'limit' => '3',
							'where' => $where,
							'sortby' => '{"publishedon":"DESC"}',
							'tpl' => '@INLINE <li>
								<a href="{$uri}" class="strong">{$longtitle ?: $pagetitle}</a>
								<p class="small-text">{"!dateAgo" | snippet : [
									"input" => $publishedon
								]}</p>
							</li>'
						]}
						{*<li>
							<a href="#" class="strong">7 советов по воспитанию маленького ребёнка</a>
							<p class="small-text">2 января 2002</p>
						</li>
						<li>
							<a href="#" class="strong">7 советов по воспитанию маленького ребёнка</a>
							<p class="small-text">2 января 2002</p>
						</li>
						<li>
							<a href="#" class="strong">7 советов по воспитанию маленького ребёнка</a>
							<p class="small-text">2 января 2002</p>
						</li>*}
					</ul>
				</nav>
			</div>
		</div>
	</div>

	{include 'file:chunks/base/subscribe.tpl'}

	{include 'file:chunks/base/footer_text.tpl'}
{/block}
