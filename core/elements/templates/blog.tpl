{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="blog-page">
{/block}

{block 'template'}
	<div id="pdopage" class="container">
		<div class="header-wrapper">
			<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
		</div>

		<div class="row rows round-white-block">
			{'!pdoPage' | snippet : [
				'parents' => $_modx->resource.id,
				'limit' => '4',
				'includeTVs' => 'img',
				'processTVs' => '1',
				'tvPrefix' => '',
				'sortby' => '{"publishedon":"DESC"}',
				'tpl' => '@FILE chunks/blog/item.tpl',
				'ajax' => '1',
				'ajaxMode' => 'scroll'
			]}
		</div>
		{$_modx->getPlaceholder('page.nav')}
	</div>

	{include 'file:chunks/base/subscribe.tpl'}

	{include 'file:chunks/base/footer_text.tpl'}
{/block}