{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body>
{/block}

{block 'template'}
		<div class="container">
			<div class="header-wrapper">
				<h2 class="pink-color h2 beautifull-h">Бренды</h2>
				<a href="{$_modx->resource.parent | url}" class="pink-color strong">Все бренды</a>
			</div>

			<div class="content-with-image-left round-white-block">
				<div class="image">
					{'@FILE snippets/catalog/getVendor.php' | snippet : [
						'resource' => $_modx->resource.id,
						'tpl' => '@INLINE <img src="{$logo}" alt="{$name}" class="img-responsive">'
					]}
				</div>
				<div class="content">
					<h1>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
					{$_modx->resource.content}
					<div class="center">
						<a href="{$_modx->resource.parent | url}" class="button" style="margin-top: 32px;">Все бренды</a>
					</div>
				</div>

			</div>


		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}
