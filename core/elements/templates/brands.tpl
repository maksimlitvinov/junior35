{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body>
{/block}

{block 'template'}
		<div class="container">
			<div class="header-wrapper">
				<h2 class="pink-color beautifull-h">{$_modx->resource.pagetitle ?: $_modx->resource.longtitle}</h2>
			</div>

			<div class="round-white-block">
				<div class="row">
					{var $vendors = '@FILE snippets/catalog/getVendorIndex.php' | snippet}
					{foreach $vendors as $vendor}
						{if $vendor.resource && $vendor.logo ?}
							<div class="col-xs-6 col-sm-4 col-md-3 brand-card">
								<a href="{$vendor.resource | url}"><img src="{$vendor.logo}" alt="{$vendor.name}"></a>
							</div>
						{/if}
					{/foreach}
					{*'@FILE snippets/catalog/getVendors.php' | snippet : [
						'tpl' => '@INLINE <div class="col-xs-6 col-sm-4 col-md-3 brand-card">
							<a href="{$_pls["vendor.resource"] | url}"><img src="{$_pls["vendor.logo"]}" alt="{$_pls["vendor.name"]}"></a>
						</div>'
					]*}
				</div>
			</div>
		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}
