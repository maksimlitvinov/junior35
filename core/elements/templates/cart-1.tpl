{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="cart-page-step1">
{/block}

{block 'template'}
		<div id="msCart" class="container">
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
				<form method="post" data-cart="">
					<button type="submit" name="ms2_action" value="cart/clean" class="pink-color like-link">Очистить корзину</button>
				</form>
			</div>
			{'@FILE snippets/cart/getCarts.php' | snippet : [
				'tpl' => '@FILE chunks/cart/carts.tpl',
				'includeThumbs' => 'cart'
			]}
		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}
