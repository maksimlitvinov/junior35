{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="cart-page-step2">
{/block}

{block 'template'}
	<div class="container">
		{if !$.get['msorder'] ?}
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
				<form method="post" class="ms2_form" data-cart="{$_modx->resource.alias}">
					<button type="submit" name="ms2_action" value="cart/clean" class="pink-color like-link">Очистить корзину</button>
				</form>
			</div>

			{'@FILE snippets/cart/getParts.php' | snippet : [
				'parents' => $_modx->resource.parent,
				'tpl' => '@FILE chunks/cart/part.tpl'
			]}

			<div class="content-wrapper">
				<div id="msCart" class="left-col">
					<div class="round-white-block content">
						{var $cart = '@FILE snippets/cart/getCart.php' | snippet : [
							'type' => $_modx->resource.alias,
							'includeThumbs' => 'cart'
						]}
						{* Заголовок *}
						<div class="header-wrapper">
							<h2>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle} (<span class="ms2_type_total_count">{$cart.total.count}</span>)</h2>
							{*'@FILE snippets/cart/getBookingOrderDate.php' | snippet : [
								'type' => $_modx->resource.alias,
								'tpl' => '@INLINE <p><strong>Бронь заказа до {$date_end}</strong></p>'
							]*}
						</div>
						{* Список товаров *}
						<div class="cart-items-table">
							{foreach $cart.products as $product}
								<div class="tr" id="{$product.hash}" {if $product.max_count <= 0 ?} style="opacity: .5;" {/if}>
									<div class="cart-item-view">
										<img src="{$product.cart}" alt="{$product.pagetitle}">
										<div class="text">
											<p class="articul">{$product.article}</p>
											<p class="strong"><a href="{$product.uri}">{$product.longtitle ?: $product.pagetitle}</a></p>
											{if $product['option.size'] ?}
												{if $product['option.size'] == 'all' ?}
													<p class="size">Весь размерный ряд</p>
												{else}
													<p class="size">Размер: {$product['option.size']}</p>
												{/if}
											{/if}
										</div>
									</div>
									<div class="price-col">
										<span class="">{$product.price}</span> ₽ / {$product.unit}
									</div>
									<div class="count-col">
										<form method="post" class="ms2_form" role="form">
											<input type="hidden" name="key" value="{$product.hash}"/>
											<input type="hidden" name="type" value="{$_modx->resource.alias}">
											<input type="hidden" name="max_count" value="{$product.max_count}">
											<div class="count-block">
												<div class="ammount">
													<button class="plus">+</button>
													<input type="number" name="count" value="{$product.max_count <= 0 ? 0 : $product.count}">
													<button class="minus">⚊</button>
												</div>
											</div>
											<button class="ms-refresh-btn" type="submit" name="ms2_action" value="cart/change">&#8635;</button>
										</form>
									</div>
									<div class="total-col pink-color">
										<span class="ms2_prod_total_cost">{$product.cost}</span> ₽
									</div>
									<form method="post" class="ms2_form text-md-right">
										<input type="hidden" name="key" value="{$product.hash}">
										<input type="hidden" name="type" value="{$_modx->resource.alias}">
										<button class="remove like-link" type="submit" name="ms2_action" value="cart/remove">
											<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0.92888 13.6568C0.538355 14.0474 0.538355 14.6805 0.92888 15.0711C1.3194 15.4616 1.95257 15.4616 2.34309 15.0711L7.99995 9.4142L13.6568 15.0711C14.0473 15.4616 14.6805 15.4616 15.071 15.0711C15.4616 14.6805 15.4616 14.0474 15.071 13.6568L9.41417 7.99999L15.071 2.34314C15.4615 1.95261 15.4615 1.31945 15.071 0.928925C14.6805 0.538401 14.0473 0.538401 13.6568 0.928925L7.99995 6.58577L2.34311 0.928927C1.95258 0.538402 1.31942 0.538402 0.928895 0.928927C0.538371 1.31945 0.538371 1.95262 0.928896 2.34314L6.58574 7.99999L0.92888 13.6568Z"/></svg>
										</button>
									</form>
								</div>
							{/foreach}
						</div>
					</div>
				</div>
				{* Правая колонка *}
				<div class="sidebar round-white-block">
					<h2>Итого</h2>
					<p class="normal-fw">
						Количество товаров: <span class="ms2_type_total_count">{$cart.total.count}</span>
					</p>
					<div class="header-wrapper">
						<strong>Сумма заказа:</strong>
						<strong class="price big-text pink-color"><span class="ms2_type_total_cost">{$cart.total.cost}</span> ₽</strong>
					</div>

					{'!msOrder' | snippet : [
						'tpl' => '@FILE chunks/cart/order.tpl'
					]}

				</div>
			</div>
		{else}
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">Заказ успешно оформлен</h1>
			</div>
			<div class="round-white-block content">
				<p>В ближайшее время наш менеджер скоро свяжется с Вами, для уточнения деталей.</p>
				<p>А пока мы обрабатываем Ваш заказ предлагаем посмотреть разделы <a href="{'34' | url}">{'34' | resource : 'menutitle'}</a> и <a href="{'35' | url}">{'35' | resource : 'menutitle'}</a> или раздел <a href="{'80' | url}">{'80' | resource : 'menutitle'}</a></p>
                {if $_modx->isAuthenticated() ?}
                    <p style="text-align: center"><strong>ИЛИ</strong></p>
                    <p style="text-align: center">
                        <a href="{'110' | url}?id={$.get['msorder']}" class="button">Перейти к заказу</a>
                    </p>
                {/if}
			</div>
		{/if}
	</div>


	{include 'file:chunks/base/footer_text.tpl'}
{/block}

{block 'scriptAfter'}
	<script src="{'assets_url' | config}components/cdek/widget/widjet.js" id="ISDEKscript"></script>
	<script>var cdekConfig = { apiKey : '{'ya_api_key' | config}', defaultCity : '{$_modx->user.city ?: 'auto'}' }</script>
	<script src="{'assets_url' | config}components/cdek/widget/custom.js"></script>
{/block}