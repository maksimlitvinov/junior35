{extends 'file:templates/base/base.tpl'}

{block 'template'}

    {var $where = ['(`Data`.`available_sizes` > 0 OR `Data`.`available_rows` > 0 OR `Data`.`soon_available` > 0)']}
    {$_modx->setPlaceholder('sideBar', 'chunks/sidebar/vendors.tpl')}

    {var $breadSub = [469,470,76,77]}

    {var $parents = '2,' ~ $_modx->resource.id}
    {include 'file:chunks/base/breadcrumbs.tpl' parents=$parents submenu=$breadSub}

    {var $mFilter = '!juniorFilter' | snippet : [
        'element' => 'msProducts@cat',
        'limit' => '30',
        'maxLimit' => '5000',
        'toArray' => '1',
        'tpl' => '@FILE chunks/catalog/productList.tpl',
        'tplOuter' => '@FILE chunks/catalog/filterOuter.tpl',
        'parents' => '2',
        'includeThumbs' => 'list',
        'where' => $where,
        'ajax' => '1',
        'ajaxMode' => 'default',
        'ajaxHistory' => '1',
        'pageLimit' => '7',
        'aliases' => '
            ms|season==season,
            ms|sex==sex,
            ms|available_rows==available_rows,
            ms|available_sizes==available_sizes,
            remains|size==size,
            resource|parent==parent
        ',
        'filters' => '
            ms|season,
            ms|sex,
            ms|available_rows:sizerange,
            ms|available_sizes:sizerange,
            remains|size,
            resource|parent
        ',
        'tplFilter.outer.season' => '@FILE chunks/filters/spoiler.outer.tpl',
        'tplFilter.row.season' => '@FILE chunks/filters/spoiler.row.tpl',
        'tplFilter.outer.sex' => '@FILE chunks/filters/spoiler.outer.tpl',
        'tplFilter.row.sex' => '@FILE chunks/filters/spoiler.row.tpl',
        'tplFilter.outer.available_rows' => '@FILE chunks/filters/sizerange.outer.tpl',
        'tplFilter.row.available_rows' => '@FILE chunks/filters/sizerange.row.tpl',
        'tplFilter.outer.available_sizes' => '@FILE chunks/filters/sizerange.outer.tpl',
        'tplFilter.row.available_sizes' => '@FILE chunks/filters/sizerange.row.tpl',
        'tplFilter.outer.size' => '@FILE chunks/filters/spoiler.outer.tpl',
        'tplFilter.row.size' => '@FILE chunks/filters/spoiler.row.tpl',
        'tplFilter.outer.parent' => '@FILE chunks/filters/spoiler.outer.tpl',
        'tplFilter.row.parent' => '@FILE chunks/filters/spoiler.row.tpl',
        'tplPageWrapper' => '@INLINE <nav class="pagination"><div class="container"><ul>{$first}{$prev}{$pages}{$next}{$last}</ul></div></nav>',
        'tplPage' => '@INLINE <li><a href="{$href}">{$pageNo}</a></li>',
        'tplPageActive' => '@INLINE <li class="active"><a href="{$href}">{$pageNo}</a></li>',
        'tplPageFirst' => '@INLINE <li class="first"><a href="{$href}"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M8.79244 8.7921C9.01095 8.57348 9.12044 8.28676 9.12044 8.00001C9.12044 7.71336 9.0112 7.42665 8.79265 7.20792L1.91295 0.328386C1.47531 -0.109462 0.765763 -0.109462 0.328306 0.328386C-0.109152 0.765808 -0.109152 1.47514 0.328306 1.91278L6.41564 8.00001L0.328093 14.0872C-0.109364 14.5249 -0.109364 15.2343 0.328093 15.6717C0.765551 16.1094 1.4751 16.1094 1.91273 15.6717L8.79244 8.7921ZM14.7924 8.7921C15.011 8.57348 15.1204 8.28676 15.1204 8.00001C15.1204 7.71336 15.0112 7.42665 14.7927 7.20792L7.91295 0.328386C7.47531 -0.109462 6.76576 -0.109462 6.32831 0.328386C5.89085 0.765808 5.89085 1.47514 6.32831 1.91278L12.4156 8.00001L6.32809 14.0872C5.89064 14.5249 5.89064 15.2343 6.32809 15.6717C6.76555 16.1094 7.4751 16.1094 7.91273 15.6717L14.7924 8.7921Z"/></svg></a></li>',
        'tplPageFirstEmpty' => '@INLINE <li class="first disabled"><a href="{$href}"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M8.79244 8.7921C9.01095 8.57348 9.12044 8.28676 9.12044 8.00001C9.12044 7.71336 9.0112 7.42665 8.79265 7.20792L1.91295 0.328386C1.47531 -0.109462 0.765763 -0.109462 0.328306 0.328386C-0.109152 0.765808 -0.109152 1.47514 0.328306 1.91278L6.41564 8.00001L0.328093 14.0872C-0.109364 14.5249 -0.109364 15.2343 0.328093 15.6717C0.765551 16.1094 1.4751 16.1094 1.91273 15.6717L8.79244 8.7921ZM14.7924 8.7921C15.011 8.57348 15.1204 8.28676 15.1204 8.00001C15.1204 7.71336 15.0112 7.42665 14.7927 7.20792L7.91295 0.328386C7.47531 -0.109462 6.76576 -0.109462 6.32831 0.328386C5.89085 0.765808 5.89085 1.47514 6.32831 1.91278L12.4156 8.00001L6.32809 14.0872C5.89064 14.5249 5.89064 15.2343 6.32809 15.6717C6.76555 16.1094 7.4751 16.1094 7.91273 15.6717L14.7924 8.7921Z"/></svg></a></li>',
        'tplPageLast' => '@INLINE <li class="last"><a href="{$href}"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M8.79244 8.7921C9.01095 8.57348 9.12044 8.28676 9.12044 8.00001C9.12044 7.71336 9.0112 7.42665 8.79265 7.20792L1.91295 0.328386C1.47531 -0.109462 0.765763 -0.109462 0.328306 0.328386C-0.109152 0.765808 -0.109152 1.47514 0.328306 1.91278L6.41564 8.00001L0.328093 14.0872C-0.109364 14.5249 -0.109364 15.2343 0.328093 15.6717C0.765551 16.1094 1.4751 16.1094 1.91273 15.6717L8.79244 8.7921ZM14.7924 8.7921C15.011 8.57348 15.1204 8.28676 15.1204 8.00001C15.1204 7.71336 15.0112 7.42665 14.7927 7.20792L7.91295 0.328386C7.47531 -0.109462 6.76576 -0.109462 6.32831 0.328386C5.89085 0.765808 5.89085 1.47514 6.32831 1.91278L12.4156 8.00001L6.32809 14.0872C5.89064 14.5249 5.89064 15.2343 6.32809 15.6717C6.76555 16.1094 7.4751 16.1094 7.91273 15.6717L14.7924 8.7921Z"></svg></a></li>',
        'tplPageLastEmpty' => '@INLINE <li class="last disabled"><a href="{$href}"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M8.79244 8.7921C9.01095 8.57348 9.12044 8.28676 9.12044 8.00001C9.12044 7.71336 9.0112 7.42665 8.79265 7.20792L1.91295 0.328386C1.47531 -0.109462 0.765763 -0.109462 0.328306 0.328386C-0.109152 0.765808 -0.109152 1.47514 0.328306 1.91278L6.41564 8.00001L0.328093 14.0872C-0.109364 14.5249 -0.109364 15.2343 0.328093 15.6717C0.765551 16.1094 1.4751 16.1094 1.91273 15.6717L8.79244 8.7921ZM14.7924 8.7921C15.011 8.57348 15.1204 8.28676 15.1204 8.00001C15.1204 7.71336 15.0112 7.42665 14.7927 7.20792L7.91295 0.328386C7.47531 -0.109462 6.76576 -0.109462 6.32831 0.328386C5.89085 0.765808 5.89085 1.47514 6.32831 1.91278L12.4156 8.00001L6.32809 14.0872C5.89064 14.5249 5.89064 15.2343 6.32809 15.6717C6.76555 16.1094 7.4751 16.1094 7.91273 15.6717L14.7924 8.7921Z"></svg></a></li>',
        'tplPageNext' => '@INLINE <li class="next"><a href="{$href}"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z"/></svg></a></li>',
        'tplPageNextEmpty' => '@INLINE <li class="next disabled"><a href="{$href}"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z"/></svg></a></li>',
        'tplPagePrev' => '@INLINE <li class="prev"><a href="{$href}"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z"/></svg></a></li>',
        'tplPagePrevEmpty' => '@INLINE <li class="prev disabled"><a href="{$href}"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z"/></svg></a></li>',
        'tplPageSkip' => '@INLINE <li class="dots">...</li>'
    ]}


    <div class="msearch2" id="mse2_mfilter">
        <div class="container">
            <div class="header-wrapper">
                <h1 class="pink-color h2 beautifull-h">{$_modx->resource.pagetitle ?: $modx->resource.longtitle}</h1>
                <p class="pink-color header-wrapper_right-label">Найдено товаров: <span id="mse2_total" class="goods-count">{$mFilter.total}</span></p>
            </div>
            <button class="button filter-button">Фильтры</button>
        </div>

        <div class="container catalogue-content-wrapper">
            <div class="menu-filter-wrapper">
                {var $sideBar = $_modx->getPlaceholder('sideBar')}
                {if $sideBar ?}
                    <nav class="sidebar-catalogue-menu">
                        <ul>
                            {include ('file:' ~ $sideBar)}
                        </ul>
                    </nav>
                {/if}
                <form  action="{$_modx->resource.uri}" method="post" id="mse2_filters">
                    <div class="filter-wrapper">

                        <div class="filter-block round-white-block">
                            {$mFilter.filters.available_rows}
                            {$mFilter.filters.available_sizes}
                        </div>
                        <div class="filter-block round-white-block">
                            {if $mFilter.filters.parent ?}
                                {$mFilter.filters.parent}
                            {/if}
                            {$mFilter.filters.season}
                            {$mFilter.filters.sex}
                            {$mFilter.filters.size}
                            {if $mFilter.filters | length > 0 ?}
                                <button type="reset" class="reset-form like-link">Очистить фильтры</button>
                            {/if}
                        </div>
                    </div>
                </form>
                <div class="close-btn"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M0.92888 13.6568C0.538355 14.0474 0.538355 14.6805 0.92888 15.0711C1.3194 15.4616 1.95257 15.4616 2.34309 15.0711L7.99995 9.4142L13.6568 15.0711C14.0473 15.4616 14.6805 15.4616 15.071 15.0711C15.4616 14.6805 15.4616 14.0474 15.071 13.6568L9.41417 7.99999L15.071 2.34314C15.4615 1.95261 15.4615 1.31945 15.071 0.928925C14.6805 0.538401 14.0473 0.538401 13.6568 0.928925L7.99995 6.58577L2.34311 0.928927C1.95258 0.538402 1.31942 0.538402 0.928895 0.928927C0.538371 1.31945 0.538371 1.95262 0.928896 2.34314L6.58574 7.99999L0.92888 13.6568Z"/></svg></div>
            </div>

            <div class="content">

                <div class="round-white-block filters-row">
                    <div class="filters" id="mse2_selected_wrapper">
                        <ul id="mse2_selected"></ul>
                    </div>
                    {*<button type="reset" class="clear like-link">Очистить фильтры</button>*}
                </div>

                <div id="mse2_results" class="product-cards-wrapper">
                    {$mFilter.results}
                </div>

            </div>

        </div>
        <div class="mse2_pagination">
            {$_modx->getPlaceholder('page.nav')}
        </div>
        {if $_modx->getPlaceholder('pageCount') > 1}
            <div class="center">
                <a href="#" data-limit="5000" class="button type2 pag_btn">Смотреть все</a>
            </div>
        {/if}
    </div>


    {include 'file:chunks/base/footer_text.tpl'}
{/block}

{block 'scriptAfter'}
    <script>
        $(document).ready(function () {
            $(document)
                .on('click', '.pag_btn', function (e) {
                    e.preventDefault();
                    var hash = mSearch2.Hash.get();
                    var limit = $(this).data('limit');
                    hash.limit = $(this).data('limit');
                    mSearch2.Hash.set(hash);
                    mse2Config.limit = limit;
                    mSearch2.load(hash);
                })
                .on('mse2_load', function(e, data) {
                    var $btn_all = $('.pag_btn');
                    if (data.data.pages == data.data.page) {
                        $btn_all.hide();
                    }
                    else {
                        $btn_all.show();
                    }
                });
        });
    </script>
{/block}