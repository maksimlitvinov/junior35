{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body>
{/block}

{block 'template'}
		<div class="container">
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
			</div>

			<div class="content-with-image-left round-white-block">
				<div class="image">
					<img src="{$_modx->resource.img}" alt="{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}" class="img-responsive">
				</div>
				<div class="content">
					<p class="strong">Время работы интернет - магазина (по Московскому времени)</p>
					<p><span class="semistrong">{'time_work' | config}</span></p>

					<p class="important-2">Оформление заказов доступно в любое время</p>
					<p></p>
					<p class="strong">Если возникли вопросы, Вы можете связаться с нами любым удобным способом: </p>
					<p>Телефон: <span class="semistrong">{'phone' | config}</span></p>
					<p>WhatsApp или Viber: <span class="semistrong">{'viber' | config}</span></p>
					<p>E-mail: <span class="semistrong">{'client_email' | config}</span></p>
					<div class="wave-border"></div>
					<h2 class="big-text">Розничные магазины в г. Вологда</h2>
					{var $addresses = ('37' | resource : 'addresses') | fromJSON}
					{foreach $addresses as $item}
						<p class="strong">{$item.address}</p>
						{if $item.weekday ?}
							<p><span class="semistrong">Пн. - Пт.: </span>{$item.weekday}</p>
						{/if}
						{if $item.saturday ?}
							<p><span class="semistrong">Сб.: </span>{$item.saturday}</p>
						{/if}
						{if $item.sunday ?}
							<p><span class="semistrong">Вс.: </span>{$item.sunday}</p>
						{/if}
						<a href="{'37' | url}">Смотреть на карте</a>
						{if !$item@last ?}
							<p></p>
						{/if}
					{/foreach}
				</div>
			</div>
		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}
