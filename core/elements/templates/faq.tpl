{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
<body>
{/block}

{block 'template'}
	<div class="container">
		<div class="header-wrapper">
			<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
		</div>

		<div class="content-with-image-left round-white-block">
			<div class="image">
				<img src="{$_modx->resource.img | phpthumbon : 'w=224&h=168&zc=1&q=70'}" alt="{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}" class="img-responsive">
			</div>
			<div class="content">
				{'!pdoResources' | snippet : [
					'parents' => $_modx->resource.id,
					'limit' => '0',
					'includeContent' => '1',
					'tpl' => '@FILE chunks/faq/item.tpl'
				]}
			</div>
		</div>
	</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}