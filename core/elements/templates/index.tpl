{extends 'file:templates/base/base.tpl'}

{block 'template'}
    <div class="first-screen-pic">
        <div class="container">
            <div class="picture-block">
                <img src="{$layout}img/first-screen-pic.png" alt="">
            </div>
            <div class="text-block">
                <h2 class="pink-color beautifull-h">Брендовая</h2>
                <h2 class="yellow-color violet-bg beautifull-h">Детская</h2>
                <h2 class="violet-color beautifull-h">Одежда</h2>
                <p>Оптовый интернет-магазин</p>
                <a href="#getPrice" class="button tooltip__btn">Выгрузить каталог товаров</a>
            </div>
        </div>
    </div>

    {*var $where = ['`Data`.`new` != 0 AND `Data`.`soon_available` = 0 AND (`Data`.`available_sizes` > 0 OR `Data`.`available_rows` > 0)']*}
    {var $where = ['`Data`.`new` != 0 AND (`Data`.`available_sizes` > 0 OR `Data`.`available_rows` > 0)']}
    {var $news_products = '!msProducts' | snippet : [
        'parents' => '2',
        'includeThumbs' => 'list',
        'limit' => '8',
        'sortby' =>'{"publishedon":"DESC"}',
        'where' => $where,
        'tpl' => '@FILE chunks/catalog/productList.tpl'
    ]}
    {if $news_products ?}
        <div class="new-items-block">
            <div class="container">
                <div class="header-wrapper">
                    <h2 class="pink-color beautifull-h">{'79' | resource : 'longtitle'}</h2>
                    <a href="{'79' | url}" class="pink-color">Все новинки</a>
                </div>
                <div class="product-cards-wrapper">
                    {$news_products}
                </div>
            </div>
        </div>
    {/if}

    {include 'file:chunks/base/subscribe.tpl'}

    <div class="advantages-block">
        <div class="container">
            <div class="row justify-center">
                {foreach $_modx->resource.benefit | fromJSON as $item}
                    <div class="col-md-4 advantage">
                        <img src="{$item.image | getPath}" alt="{$item.desc}">
                        <p>{$item.desc}</p>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>

    <div class="our-brands">
        <div class="container">
            <div class="header-wrapper">
                <h2 class="beautifull-h">{'82' | resource : 'longtitle'}</h2>
                <a href="{'82' | url}" class="pink-color">Все бренды</a>
            </div>
            <p>{'82' | resource : 'introtext'}</p>
            <div class="four-items-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {var $vendors = '@FILE snippets/catalog/getVendorIndex.php' | snippet}
                        {foreach $vendors as $vendor}
                            {if $vendor.logo ?}
                                <div class="swiper-slide">
                                    <img src="{$vendor.logo}" alt="{$vendor.name}">
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                </div>
                <div class="swiper-navigation">
                    <!-- Add Arrows -->
                    <div class="my-swiper-button-next">
                        <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1.5 9.3999H18.9L12.4 3C11.8 2.4 11.8 1.4999 12.4 0.899902C13 0.299902 13.9 0.299902 14.5 0.899902L23.6 9.8999C23.9 10.1999 24 10.6 24 11C24 11.4 23.8 11.7999 23.6 12.0999L14.5 21.0999C14.2 21.3999 13.8 21.5 13.5 21.5C13.1 21.5 12.7 21.3999 12.4 21.0999C11.8 20.4999 11.8 19.6 12.4 19L18.9 12.5999H1.5C0.700001 12.5999 0 11.8999 0 11.0999C0 10.0999 0.700001 9.3999 1.5 9.3999Z" /></svg>
                    </div>
                    <div class="my-swiper-button-prev">
                        <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M22.5 9.3999H5.10001L11.6 3C12.2 2.4 12.2 1.4999 11.6 0.899902C11 0.299902 10.1 0.299902 9.5 0.899902L0.399994 9.8999C0.0999939 10.1999 0 10.6 0 11C0 11.4 0.199994 11.7999 0.399994 12.0999L9.5 21.0999C9.8 21.3999 10.2 21.5 10.5 21.5C10.9 21.5 11.3 21.3999 11.6 21.0999C12.2 20.4999 12.2 19.6 11.6 19L5.10001 12.5999H22.5C23.3 12.5999 24 11.8999 24 11.0999C24 10.0999 23.3 9.3999 22.5 9.3999Z"/></svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}