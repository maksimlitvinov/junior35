{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="news-inside-page">
{/block}

{block 'template'}
	<div class="container">
		<div class="header-wrapper">
			<h2 class="pink-color beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h2>
			<a href="{'36' | url}" class="pink-color strong">Все новости</a>
		</div>

		<div class="content-with-image-left round-white-block">
			<div class="image one-item-slider">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						{foreach $_modx->resource.small_slider | fromJSON as $item}
							<div class="swiper-slide">
								<img src="{($item.image | getPath) | phpthumbon : 'w=416&h=364&zc=1'}" alt="{$item.name}">
							</div>
						{/foreach}
					</div>
					<div class="swiper-navigation">
						<!-- Add Arrows -->
						<div class="my-swiper-button-prev">
							<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M22.5 9.3999H5.10001L11.6 3C12.2 2.4 12.2 1.4999 11.6 0.899902C11 0.299902 10.1 0.299902 9.5 0.899902L0.399994 9.8999C0.0999939 10.1999 0 10.6 0 11C0 11.4 0.199994 11.7999 0.399994 12.0999L9.5 21.0999C9.8 21.3999 10.2 21.5 10.5 21.5C10.9 21.5 11.3 21.3999 11.6 21.0999C12.2 20.4999 12.2 19.6 11.6 19L5.10001 12.5999H22.5C23.3 12.5999 24 11.8999 24 11.0999C24 10.0999 23.3 9.3999 22.5 9.3999Z"/></svg>
						</div>
						<div class="swiper-counter">
							<span class="now"></span>/<span class="total"></span>
						</div>
						<div class="my-swiper-button-next">
							<svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1.5 9.3999H18.9L12.4 3C11.8 2.4 11.8 1.4999 12.4 0.899902C13 0.299902 13.9 0.299902 14.5 0.899902L23.6 9.8999C23.9 10.1999 24 10.6 24 11C24 11.4 23.8 11.7999 23.6 12.0999L14.5 21.0999C14.2 21.3999 13.8 21.5 13.5 21.5C13.1 21.5 12.7 21.3999 12.4 21.0999C11.8 20.4999 11.8 19.6 12.4 19L18.9 12.5999H1.5C0.700001 12.5999 0 11.8999 0 11.0999C0 10.0999 0.700001 9.3999 1.5 9.3999Z" /></svg>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<p class="date small-text">{'!dateAgo' | snippet : [
					'input' => $_modx->resource.publishedon,
					'dateFormat' => 'd F Y'
					]}</p>
				<h1>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
				{$_modx->resource.content}
			</div>
		</div>
	</div>
	{include 'file:chunks/base/subscribe.tpl'}

	{include 'file:chunks/base/footer_text.tpl'}
{/block}