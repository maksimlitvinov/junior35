{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body class="news-page">
{/block}

{block 'template'}
		<div id="pdopage" class="container">
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
			</div>

			<div class="row rows round-white-block">
				{'!pdoPage' | snippet : [
					'parents' => $_modx->resource.id,
					'limit' => '6',
					'ajax' => '1',
					'ajaxMode' => 'default',
					'ajaxHistory' => '1',
					'pageLimit' => '7',
					'includeTVs' => 'img',
					'prepareTVs' => 'img',
					'tpl' => '@FILE chunks/news/item.tpl'
				]}
			</div>
			{$_modx->getPlaceholder('page.nav')}
		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}