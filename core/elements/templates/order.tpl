{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
    <body class="cabinet-order-page">
{/block}

{block 'template'}
    <div class="container">
        <div class="header-wrapper">
            <h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
            <a href="{'109' | url}" class="pink-color">Вернуться к списку</a>
        </div>

        {if $.get['id'] ?}
            {var $result = '@FILE snippets/cart/getOrder.php' | snippet : [
                'orderId' => $.get['id'],
                'includeThumbs' => 'cart'
            ]}
            {if $result | length > 0 ?}
                <div class="content-with-sidebar">
                    <div class="content round-white-block">
                        <div class="header-wrapper">
                            <h2 class="big-text">Заказ № {$result.order.num}</h2>
                            <p>{$result.order.createdon | date : 'd.m.Y'}</p>
                        </div>
                        <div class="header-wrapper">
                            {if $result.order.cart_type == 'row'}
                                <p><strong>Товары рядами (<span class="order-product-count">{$result.products_count}</span>)</strong></p>
                            {else}
                                <p><strong>Товары поштучно (<span class="order-product-count">{$result.products_count}</span>)</strong></p>
                            {/if}
                            <p><strong>Бронь заказа до {$result.order.date_end_reservation | date : 'd.m.Y'}</strong></p>
                        </div>
                        <div class="cart-items-table">
                            {foreach $result.products as $product}
                                <div class="tr order__row">
                                    <div class="cart-item-view">
                                        <img src="{$product.cart}" alt="{$product.pagetitle}">
                                        <div class="text">
                                            <p class="articul">{$product.article}</p>
                                            <p class="strong"><a href="{$product.uri}">{$product.name}</a></p>
                                            {if $result.order.cart_type  == 'row' ?}
                                                <p class="size">Весь размерный ряд</p>
                                            {else}
                                                <p class="size">Размер: {$product.options.size}</p>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="price-col">
                                        <span>{$product.price} ₽</span> / {$product.unit}
                                    </div>
                                    <div class="count-col">
                                        <div class="count-block">
                                            {if $result.status.id == 1 ?}
                                                <form action="{$uri}" type="post" class="ms2_form product__order">
                                                    <div class="ammount">
                                                        <button class="plus__order">+</button>
                                                        <input type="number" name="count__order" value="{$product.count}">
                                                        <button class="minus__order">⚊</button>
                                                    </div>
                                                    <input type="hidden" name="max_count" value="{$product.max_count}">
                                                    <input type="hidden" name="ms2_action" value="order/updateProduct">
                                                    <input type="hidden" name="product" value="{$product.order_product_id}">
                                                </form>
                                            {else}
                                                <div class="ammount">
                                                    <p class="ammount__text">{$product.count}</p>
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <div class="total-col pink-color">
                                        <span class="order__product-cost">{$product.cost}</span> ₽
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    </div>

                    <div class="sidebar round-white-block">
                        <h2 class="big-text">Итого</h2>
                        <p class="normal-fw">
                            Количество товаров: <span class="order-product-count">{$result.products_count}</span>
                        </p>
                        <div class="header-wrapper">
                            <strong>Сумма заказа:</strong>
                            <strong class="price big-text pink-color"><span class="order-product-cost">{$result.order.cost}</span> ₽</strong>
                        </div>
                        <hr>
                        <div class="table-wrapper">
                            <table class="without-borders">
                                <tr>
                                    <td>Статус заказа:</td>
                                    <td><span style="color:#{$result.status.color}">{$result.status.name}</span></td>
                                </tr>
                                <tr>
                                    <td>Оплата заказа:</td>
                                    <td><strong>{$result.order.payment_date ? 'Оплачен' : 'Не оплачен'}</strong></td>
                                </tr>
                                <tr>
                                    <td>Дата оплаты:</td>
                                    <td><strong>{$result.order.payment_date ?: '-'}</strong></td>
                                </tr>
                                <tr>
                                    <td>Дата отгрузки:</td>
                                    <td><strong>{$result.order.shipment_date ?: '-'}</strong></td>
                                </tr>
                                <tr>
                                    <td>Склад отгрузки:</td>
                                    <td><strong>{$result.order.shipment_warehouse ?: '-'}</strong></td>
                                </tr>
                                <tr>
                                    <td>Номер отправления:</td>
                                    <td><strong>{$result.order.departure_number ?: '-'}</strong></td>
                                </tr>
                            </table>
                        </div>
                        <hr>
                            {if
                                ($result.order.cart_type == 'size' && ($result.order.cost | preg_replace : '/[^0-9+]/': '') >= (('sum_min_order_size' | config) | preg_replace : '/[^0-9+]/': ''))
                                ||
                                ($result.order.cart_type == 'row' && ($result.order.cost | preg_replace : '/[^0-9+]/': '') >= (('sum_min_order_row' | config) | preg_replace : '/[^0-9+]/': ''))
                            }
                                <p>
                                    <strong>Дополнительные способы оплаты</strong>
                                </p>
                                <p><a href="#payment-dc" class="button popup-btn">Реквизиты расчетного счета</a></p>
                                <p><a href="#payment-cart-sb" class="button popup-btn">Реквизиты карта Сбербанк</a></p>
                                <p><a href="#payment-cart-vtb" class="button popup-btn">Реквезиты карта ВТБ</a></p>
                                <p><a href="#requisites-popup" class="button popup-btn">Получить счет (для юридических лиц)</a></p>
                                <hr>
                            {/if}
                        <p>
                            <strong>Детали заказа</strong>
                        </p>
                        <p>Способ оплаты: <span class="normal-fw">{$result.payment.name}</span></p>
                        <p>Способ доставки: <span class="normal-fw">{$result.delivery.name}</span></p>
                        {if $result.order.promo_code ?}
                            <p>Промокод: <span class="normal-fw">{$result.order.promo_code}</span></p>
                        {/if}
                    </div>

                </div>
            {/if}
        {/if}

    </div>

    {include 'file:chunks/base/footer_text.tpl'}
{/block}

{block 'modals'}
    <div id="payment-dc" class="popup-block mfp-hide">
        <h3>Наши реквизиты</h3>
        {'detail_company' | config}
    </div>
    <div id="payment-cart-sb" class="popup-block mfp-hide">
        <h3>Номер карты Сбербанка</h3>
        <p>{'sb_cart_number' | config}</p>
        <p>{'sb_cart_holder' | config}</p>
    </div>
    <div id="payment-cart-vtb" class="popup-block mfp-hide">
        <h3>Номер карты ВТБ</h3>
        <p>{'vtb_cart_number' | config}</p>
        {if 'vtb_cart_holder' | config ?}
            <p>{'vtb_cart_holder' | config}</p>
        {/if}
    </div>
    <div id="requisites-popup" class="popup-block mfp-hide">
        <form action="{$_modx->resource.uri}" type="POST" class="ms2_form">
			<h3>Получить счет</h3>
			<textarea placeholder="Реквизиты организации" title="Реквизиты организации">{$result.order.rekvez}</textarea>
			<div class="spoiler-block">
				<p class="spoiler-title">Пример заполнения</p>
				<div class="spoiler-content">
					<p>ООО “Метра” г. Вологда ИНН 3525329697 КПП 352501001 ОГРН 114352014270 р/с 40702810012000012837 Вологодское отделение №8638 ПАО СБЕРБАНК БИК 041909644 к/с 30101910900000000644</p>
				</div>
			</div>
			<p class="small-text">Нажимая на кнопку "Отправить", я даю согласие на <a href="{85 | url}">обработку персональных данных</a></p>
			<input type="hidden" name="ms2_action" value="order/addRekvez">
            <input type="hidden" name="order" value="{$.get['id']}">
            <input class="button to-order-rekvez" type="submit" value="Отправить">
		</form>
		{*
        <h3>Получить счет</h3>
        <form action="{$_modx->resource.uri}" type="POST" class="ms2_form">
            <div>
                <label>Реквизиты организации:</label>
                <textarea name="rekvez">{$result.order.rekvez}</textarea>
            </div>
            <div>
                <p>Например:</p>
                <p>ООО "МЕТРА" г. Вологда ИНН 3525329697 КПП 352501001 ОГРН 1143525014270 р/с 40702810012000012837 ВОЛОГОДСКОЕ ОТДЕЛЕНИЕ № 8638 ПАО СБЕРБАНК БИК 041909644 к/с 30101810900000000644</p>
            </div>
            <input type="hidden" name="ms2_action" value="order/addRekvez">
            <input type="hidden" name="order" value="{$.get['id']}">
            <input class="button to-order-rekvez" type="submit" value="Запросить счет">
        </form>
        *}
    </div>
{/block}