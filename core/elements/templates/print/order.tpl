<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <!-- <base href="/"> -->
    <base href="{'site_url' | config}">
    <title>Заказ № {$num}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Custom Browsers Color Start -->
    <meta name="theme-color" content="#000">
    <!-- Custom Browsers Color End -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <style>
        @media print {  a[href]:after { content: none !important; }  }
    </style>
</head>

<body>
<div class="container-fluid">
    <h3 class="text-center">Заказ № {$num}</h3>
    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-6"><b>ФИО:</b></div>
                <div class="col-xs-6">{$order.receiver}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Телефон:</b></div>
                <div class="col-xs-6">{$order.phone}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>E-mail:</b></div>
                <div class="col-xs-6">{$user.email}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Способ оплаты:</b></div>
                <div class="col-xs-6">{$order.payment}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Способ доставки:</b></div>
                <div class="col-xs-6">{$order.delivery}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Паспортные данные:</b></div>
                <div class="col-xs-6">{$user.passpotr_data ?: '-'}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Адрес доставки:</b></div>
                <div class="col-xs-6">{$order.city} {$order.street}</div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="row">
                <div class="col-xs-6"><b>Статус заказа:</b></div>
                <div>{$order.status}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Дата отгрузки:</b></div>
                <div class="col-xs-6">{$order.shipment_date ?: '-'}</div>
            </div>
            <div class="row">
                <div class="col-xs-6"><b>Номер отправления:</b></div>
                <div class="col-xs-6">{$order.departure_number ?: '-'}</div>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>№</th>
                <th>Фото</th>
                <th>Наименование</th>
                <th>Кол-во</th>
                <th>Цена за шт.</th>
                <th>Сумма</th>
                <th>Дополнительно</th>
            </tr>
        </thead>
        <tbody>
            {foreach $products as $product index=$index}
                <tr>
                    <td>{$index + 1}</td>
                    <td><img src="{$product.thumb}" alt="" height="70"></td>
                    <td>
                        <a href="{$product.product_id | url}" target="_blank">{$product.name}</a>
                        <div>(Размер: {$product.options.size})</div>
                    </td>
                    <td>{$product.count}</td>
                    <td>{$product.price} руб.</td>
                    <td>{$product.cost} руб.</td>
                    <td>В заказе</td>
                </tr>
            {/foreach}
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>Итого</td>
                <td>{$order.cost} руб.</td>
                <td></td>
            </tr>
        </tbody>
    </table>


    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <div class="form-group">
                    <label class="col-xs-4 control-label">Примечания к заказу</label>
                    <div class="col-xs-8">{$order.comment}</div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>