{extends 'file:templates/base/base.tpl'}

{block 'template'}
    {*{if $.session['pageType'] == 'rows'}
        {include 'file:chunks/base/breadcrumbs.tpl' parents='2,469'}
    {elseif $.session['pageType'] == 'sizes'}
        {include 'file:chunks/base/breadcrumbs.tpl' parents='2,470'}
    {else}
        {include 'file:chunks/base/breadcrumbs.tpl'}
    {/if}*}


    <div id="msProduct" class="container">
        <div class="item-main-block">
            <div class="header-wrapper">
                <h1>{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
                {if $_modx->resource.old_price ?}
                    <div class="bage pink-bg">Скидка</div>
                {/if}
            </div>
            <p class="articul">Арт. {$_modx->resource.article}</p>

            <div class="product-wrapper">
                <div class="top">
                    <div class="product-gallery">
                        <div data-id="{$_modx->resource.id}" class="add-to-favorites">
                            <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M11.5269 0C9.97853 0 8.60223 0.869565 7.91405 2.08696C7.13986 0.869565 5.84947 0 4.30108 0C1.89248 0 0 1.91304 0 4.34783C0 5.65217 0.602214 6.78261 1.46243 7.65217L7.56994 13.8261C7.65597 13.913 7.82801 14 8.00005 14C8.1721 14 8.25812 13.913 8.43016 13.8261L14.5377 7.65217C15.3979 6.86956 16 5.65217 16 4.34783C15.828 2 13.9355 0 11.5269 0Z"/></svg>
                        </div>
                        <div class="swiper-container gallery-thumbs">
                            <div class="swiper-wrapper">
                                {'msGallery' | snippet : [
                                    'tpl' => '@INLINE {if $files ?}
                                    {foreach $files as $file}
                                    <div class="swiper-slide">
                                        <img src="{$file.prod_thumb}" alt="{$file.name}">
                                    </div>
                                    {/foreach}{/if}'
                                ]}
                            </div>
                        </div>
                        <div class="swiper-container gallery-items">
                            <div class="swiper-wrapper">
                                {'msGallery' | snippet : [
                                    'tpl' => '@INLINE {if $files ?}
                                    {foreach $files as $file}
                                    <div class="swiper-slide">
                                        <img src="{$file.prod}" alt="{$file.name}">
                                    </div>
                                    {/foreach}{/if}'
                                ]}
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <table class="without-borders product-options">
                            {if $fabric ?}
                                <tr>
                                    <td class="title">Материал:</td>
                                    <td>{$fabric}</td>
                                </tr>
                            {/if}
                            {if $_pls['vendor.name'] ?}
                                <tr>
                                    <td class="title">Бренд:</td>
                                    <td>{$_pls['vendor.name']}</td>
                                </tr>
                            {/if}
                            {if $made_in ?}
                                <tr>
                                    <td class="title">Страна бренда:</td>
                                    <td>{$made_in}</td>
                                </tr>
                            {/if}
                        </table>
                        <a href="{'209' | url}">Таблица размеров</a>
                        <div class="price-block">
                            <div class="prices">
                                {if $old_price ?}
                                    <div class="old-price">{$old_price} ₽</div>
                                {/if}
                                <div class="new-price">{$price} ₽</div>
                            </div>
                            <p class="text">/ {$unit}</p>
                        </div>

                        {var $has_in_sklad = ($available_rows + $available_sizes) > 0}


                        {if $soon_available == 0 || $has_in_sklad}
                            <form class="form-to-cart ms2_form" action="{$_modx->resource.uri}" method="post">
                                <input type="hidden" name="id" value="{$_modx->resource.id}">
                                {'@FILE snippets/catalog/getSizes.php' | snippet : [
                                    'tpl' => '@FILE chunks/catalog/sizes.tpl'
                                ]}
                                {var $max_count = $_modx->getPlaceholder('max_count')}
                                {var $cart_type = $_modx->getPlaceholder('cart_type')}
                                {var $noPaymentsOrders = '@FILE snippets/cart/getOrders.php' | snippet : [
                                    'where' => '{"msOrder.status:IN":[1,5]}'
                                ]}
                                {if $noPaymentsOrders | length > 0}
                                    {foreach $noPaymentsOrders as $item}
                                        <div data-type="{$item.cart_type}" class="count-button-wrapper product__order {$cart_type == $item.cart_type ? '' : 'mfp-hide'}">
                                            <div class="count-block">
                                                <div class="ammount">
                                                    <button class="plus__order">+</button>
                                                    <input type="number" name="count__order" value="1">
                                                    <button class="minus__order">⚊</button>
                                                </div>
                                            </div>
                                            <input type="hidden" name="order" value="{$item['id']}">
                                            <input type="hidden" name="max_count" value="{$max_count}">
                                            <button type="submit" class="to-order" name="ms2_action" value="order/addProduct">В заказ {$item['num']}</button>
                                        </div>
                                    {/foreach}
                                {/if}
                                <div class="count-button-wrapper">
                                    <div class="count-block">
                                        <div class="ammount">
                                            <button class="plus">+</button>
                                            <input type="number" name="count" value="1">
                                            <button class="minus">⚊</button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="type" value="{$cart_type}">
                                    <input type="hidden" name="max_count" value="{$max_count}">
                                    <button type="submit" class="to-cart" name="ms2_action" value="cart/add">В корзину</button>
                                </div>
                            </form>
                        {/if}

                        {*<a href="{'206' | url}">Как сделать заказ?</a>*}
                        <div class="share">
                            <p>Поделиться:</p>
                            <!-- uSocial -->
                            <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                            <div class="uSocial-Share" data-pid="bade25067a2a0f919c23b6d4ae55c80d" data-type="share" data-options="round,style1,default,absolute,horizontal,size32,counter0" data-social="vk,fb,ok,twi" data-mobile="vi,wa,sms"></div>
                            <!-- /uSocial -->
                        </div>

                    </div>
                </div>

                <div class="tabs">
                    <ul class="tabs__caption">
                        <li class="active">Описание</li>
                        {'!TicketComments' | snippet : [
                        'allowGuestEdit' => 0,
                        'autoPublish' => 0,
                        'autoPublishGuest' => 0,
                        'formBefore' => 0,
                        'tplCommentForm' => '@INLINE ',
                        'tplCommentDeleted' => '@INLINE ',
                        'tplCommentFormGuest' => '@INLINE ',
                        'tplLoginToComment' => '@INLINE ',
                        'tplComments' => '@INLINE <li>Отзывы <span id="comment-total">{$total}</span></li>',
                        'tplCommentGuest' => '@INLINE ',
                        'tplCommentAuth' => '@INLINE ',
                        ]}
                    </ul>
                    <div class="tabs__content active">
                        {$_modx->resource.content}
                    </div>
                    <div class="tabs__content">
                            <div class="item-page-reviews-wrapper">
                                {'!TicketComments' | snippet : [
                                    'allowGuestEdit' => 0,
                                    'autoPublish' => 0,
                                    'autoPublishGuest' => 0,
                                    'formBefore' => 0,
                                    'tplCommentForm' => '@INLINE ',
                                    'tplCommentDeleted' => '@INLINE ',
                                    'tplCommentFormGuest' => '@INLINE ',
                                    'tplLoginToComment' => '@INLINE ',
                                    'tplComments' => '@INLINE <div class="reviews">{$comments}</div>',
                                    'tplCommentGuest' => '@FILE chunks/reviews/comment.tpl',
                                    'tplCommentAuth' => '@FILE chunks/reviews/comment.tpl',
                                ]}
                                {if $_modx->user.id > 0 ?}
                                    <a href="#review-popup" class="popup-btn button pink-bg">Оставить отзыв</a>
                                {else}
                                    <a href="#login-popup" class="popup-btn button pink-bg">Оставить отзыв</a>
                                {/if}
                            </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="similar-products">
            <h2>Похожие товары</h2>
            <div class="product-cards-wrapper">
                {var $likeWhere = [
                '`Data`.`sex` = "' ~ $sex ~ '" AND (`Data`.`available_sizes` > 0 OR `Data`.`available_rows` > 0)'
                ]}
                {var $likeExclude = '-' ~ $_modx->resource.id}
                {'!msProducts' | snippet : [
                    'parents' => $_modx->resource.parent,
                    'resources' => $likeExclude,
                    'limit' => '4',
                    'where' => $likeWhere,
                    'includeThumbs' => 'list',
                    'tpl' => '@FILE chunks/catalog/productList.tpl'
                ]}
            </div>
        </div>
    </div>

    {include 'file:chunks/base/footer_text.tpl'}

{/block}

{block 'modals'}
    {'!TicketComments' | snippet : [
        'allowGuestEdit' => 0,
        'autoPublish' => 0,
        'autoPublishGuest' => 0,
        'formBefore' => 0,
        'tplCommentAuth' => '@INLINE ',
        'tplLoginToComment' => '@INLINE ',
        'tplComments' => '@INLINE ',
        'tplCommentGuest' => '@INLINE ',
        'tplCommentFormGuest' => '@INLINE ',
        'tplCommentDeleted' => '@INLINE ',
        'tplCommentAuth' => '@INLINE ',
        'tplCommentForm' => '@FILE chunks/forms/comments.tpl'
    ]}
{/block}