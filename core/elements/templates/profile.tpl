{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
    <body class="cabinet-page">
{/block}

{block 'template'}
    <div class="container">
        <div class="header-wrapper">
            <h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
        </div>

        <div class="round-white-block">
            <div class="row">
                <div class="col-md-6 left-col">
                    <h3 class="big-text">Личные данные</h3>
                    {'!officeProfile' | snippet : [
                        'tplProfile' => '@FILE chunks/forms/profile.tpl',
                        'profileFields' => 'fullname,mobilephone,email,city,address,passpotr_data'
                    ]}
                </div>
                <div class="col-md-6 right-col">
                    {var $orders = '@FILE snippets/cart/getOrders.php' | snippet}
                    {if $orders ?}
                        <h3 class="big-text">Мои заказы</h3>
                        <div class="table-wrapper">
                            <table>
                                <tr>
                                    <th>№</th>
                                    <th>Дата</th>
                                    <th>Статус</th>
                                </tr>
                                {foreach $orders as $item}
                                    <tr>
                                        <td><a href="{'110' | url}?id={$item.id}">{$item.num}</a></td>
                                        <td>{$item.createdon | date : 'd.m.Y'}</td>
                                        <td><span style="color:#{$item.status_color}">{$item.status_name}</span></td>
                                    </tr>
                                {/foreach}
                            </table>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>

    {include 'file:chunks/base/footer_text.tpl'}
{/block}