{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<div class="main-content">
{/block}

{block 'template'}

		<div class="container">
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $modx->resource.pagetitle}</h1>
			</div>

			<div class="round-white-block">
				<div class="row reviews-wrapper">
					{'!getTickets' | snippet : [
						'parents' => $_modx->resource.id,
						'limit' => '0',
						'includeTVs' => 'img',
						'processTVs' => '1',
						'includeContent' => '1',
						'tpl' => '@FILE chunks/reviews/item.tpl'
					]}
				</div>

				{'!TicketForm' | snippet : [
					'allowFiles' => '0',
					'allowedFields' => 'parent,pagetitle,content,longtitle',
					'tplFormCreate' => '@FILE chunks/forms/reviews.tpl',
					'redirectUnpublished' => $_modx->resource.id
				]}
				{*<div class="row review-form-wrapper">
					<div class="col-md-6">
						<p class="big-text strong">Ваш отзыв</p>
						<form>
							<input type="text" placeholder="Ваше имя" title="Ваше имя">
							<input type="email" placeholder="Email" title="Email">
							<textarea placeholder="Ваш отзыв" title="Ваш отзыв"></textarea>
							<p>Нажимая на кнопку "Отправить", я даю согласие на <a href="#">обработку персональных данных</a></p>
							<button>Отправить</button>
						</form>
					</div>
				</div>*}

			</div>


		</div>

		{include 'file:chunks/base/footer_text.tpl'}
{/block}