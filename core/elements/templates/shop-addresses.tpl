{extends 'file:templates/base/base.tpl'}

{block 'openBody'}
	<body>
{/block}

{block 'template'}
		<div class="container">
			<div class="header-wrapper">
				<h1 class="pink-color h2 beautifull-h">{$_modx->resource.longtitle ?: $_modx->resource.pagetitle}</h1>
			</div>

			<div class="map-wrapper round-white-block">
				<div class="addresses-block map_addresses">
					{foreach $_modx->resource.addresses | fromJSON as $item}
						{var $checked = ''}
						{var $hidden = 'hidden'}
						{if $item@first ?}
							{set $checked = 'checked'}
							{set $hidden = ''}
						{/if}
						<label class="custom-radio map_address">
							<span class="text strong">{$item.address}</span>
							<input type="radio" name="address-choose" {$checked}>
							<input type="hidden" name="address" value="{$item.address | htmlent}">
							<span class="checkmark"></span>
						</label>
						<div class="additional-text {$hidden}">
							<span>Пн.-Пт.: {$item.weekday}</span><br>
							<span>Сб: {$item.saturday}</span><br>
							<span>Вс: {$item.sunday}</span>
						</div>
						{if !$item@last ?}<hr>{/if}
					{/foreach}
				</div>
				<div id="ya-map-1" class="map"></div>
			</div>
		</div>

	{include 'file:chunks/base/footer_text.tpl'}
{/block}

{block 'scriptAfter'}
	<script src="https://api-maps.yandex.ru/2.1/?apikey={'ya_api_key' | config}&lang=ru_RU"></script>
	<script src="{'assets_url' | config}components/map/js/default.js"></script>
{/block}