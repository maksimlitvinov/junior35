miniShop2.combo.status_refund = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            id: 'rs-items',
            fields: ['value',{name: 'rs', type: 'string'}],
            data: [
                ['', 'Возврат не требуется'],
                ['process', 'Ожидается возврат'],
                ['finish', 'Возврат произведен']
            ]
        }),
        mode: 'local',
        displayField: 'rs',
        valueField: 'value',
        hiddenName: 'status_refund',
    });
    miniShop2.combo.status_refund.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.status_refund, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-status-refund', miniShop2.combo.status_refund);

miniShop2.combo.shipment_warehouse = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            id: 'shipment_warehouse-item',
            fields: ['value',{name: 'sw', type: 'string'}],
            data: [
                ['', 'Не указан'],
                ['vologda', 'Вологда'],
                ['vladivostok', 'Владивосток']
            ]
        }),
        mode: 'local',
        displayField: 'sw',
        valueField: 'value',
        hiddenName: 'shipment_warehouse',
    });
    miniShop2.combo.shipment_warehouse.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.shipment_warehouse, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-shipment-warehouse', miniShop2.combo.shipment_warehouse);

// Гриды ордеров
var msPrintDocs = function (config) {
    config = config || {};
    msPrintDocs.superclass.constructor.call(this, config);
};
Ext.extend(msPrintDocs, Ext.Component, {
    grid: {},
    formatSize: function (value) {
        if (value === 'all') {
            return 'Ряд';
        }
        return value;
    }
});
Ext.reg('msPrintDocs', msPrintDocs);
msPrintDocs = new msPrintDocs();

// Orders window
Ext.onReady(function () {
    Ext.ComponentMgr.onAvailable('minishop2-window-order-update', function (config) {
        // Добавляем вкладку "Возврат"
        this.fields.items.splice(3, 0, {
            layout: 'form',
            title: 'Возврат',
            hideMode: 'offsets',
            bodyStyle: 'padding:5px 0;',
            defaults: {msgTarget: 'under', border: false},
            items: getRefund()
        });
        function getRefund () {
            var fields = [
                {
                    layout: 'column',
                    defaults: {msgTarget: 'under', border: false},
                    style: 'padding:15px 5px;text-align:center;',
                    items: [
                        {
                            columnWidth: .5,
                            layout: 'form',
                            items: [{
                                xtype: 'datefield',
                                name: 'date_refund',
                                fieldLabel: 'Дата возврата',
                                format: 'd-m-Y',
                                emptyText: 'Выберите дату',
                                anchor: '95%'
                            }]
                        },
                        {
                            columnWidth: .5,
                            layout: 'form',
                            items: [{
                                xtype: 'minishop2-combo-status-refund',
                                name: 'status_refund',
                                fieldLabel: 'Статус возврата',
                                emptyText: 'Не требуется',
                                anchor: '95%'
                            }]
                        },
                        {
                            columnWidth: 1,
                            layout: 'form',
                            items: [{
                                xtype: 'textfield',
                                name: 'card_number_refund',
                                fieldLabel: 'Номер карты для возврата',
                                anchor: '95%',
                            }]
                        }
                    ]
                }
            ];
            return fields;
        }

        // Добавление в рамку на вкладке "Заказ"
        this.fields.items[0].items[2].items[0].items.push({
            xtype: 'displayfield',
            name: 'cart_type',
            fieldLabel: 'Тик корзины',
            anchor: '100%',
            submitValue: false,
            listeners: {
                render: {
                    fn: function (el) {
                        if (el.value == 'row') {
                            el.value = 'Ряды'
                        }
                        else if (el.value == 'size') {
                            el.value = 'Размеры';
                        }
                    }
                }
            }
        });
        this.fields.items[0].items[2].items[1].items.push({
            xtype: 'displayfield',
            name: 'first_warning_message',
            fieldLabel: 'Первое предупреждение',
            anchor: '100%',
            submitValue: false,
            listeners: {
                render: {
                    fn: function (el) {
                        if (el.value) {
                            el.value = 'Отправлено'
                        }
                        else {
                            el.value = 'Не отправлено';
                        }
                    }
                }
            }
        });
        this.fields.items[0].items[2].items[2].items.push({
            xtype: 'displayfield',
            name: 'second_warning_letter',
            fieldLabel: 'Второе предупреждение',
            anchor: '100%',
            submitValue: false,
            listeners: {
                render: {
                    fn: function (el) {
                        if (el.value) {
                            el.value = 'Отправлено'
                        }
                        else {
                            el.value = 'Не отправлено';
                        }
                    }
                }
            }
        });

        // Добавляем поля под рамку на вкладке "Заказ"
        this.fields.items[0].items.splice(3, 0, {
            layout: 'column',
            defaults: {msgTarget: 'under', border: false},
            style: 'padding:15px 5px;text-align:center;',
            items: [
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'textfield',
                        name: 'promo_code',
                        fieldLabel: 'Промокод',
                        anchor: '95%',
                    }]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'datefield',
                        name: 'shipment_date',
                        fieldLabel: 'Дата отгрузки',
                        //format: MODx.config.manager_date_format,
                        format: 'd-m-Y',
                        emptyText: 'Выберите дату',
                        anchor: '95%'
                    }]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'minishop2-combo-shipment-warehouse',
                        name: 'shipment_warehouse',
                        fieldLabel: 'Склад отгрузки',
                        anchor: '95%',
                    }]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'textfield',
                        name: 'departure_number',
                        fieldLabel: 'Номер отправления',
                        anchor: '95%',
                    }]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'datefield',
                        name: 'payment_date',
                        fieldLabel: 'Дата оплаты',
                        format: 'd-m-Y',
                        emptyText: 'Выберите дату',
                        anchor: '95%',
                    }]
                },
                {
                    columnWidth: .5,
                    layout: 'form',
                    items: [{
                        xtype: 'datefield',
                        name: 'date_end_reservation',
                        fieldLabel: 'Дата окончания бронирования',
                        format: 'd-m-Y',
                        emptyText: 'Выберите дату',
                        anchor: '95%',
                    }]
                }
            ]
        });
    });
});

// Расширяем гриды ордеров
if (typeof miniShop2.grid.Orders !== "undefined") {
    msPrintDocs.grid.msOrders = function (config) {
        Ext.applyIf(config, {
            url: '/assets/components/extminishop/connector.php',
            baseParams: {
                action: 'mgr/orders/getlist',
                sort: 'id',
                dir: 'desc',
            },
            multi_select: true,
            changed: false,
            stateful: true,
            stateId: config.id,
        });
        msPrintDocs.grid.msOrders.superclass.constructor.call(this, config);
    };
    Ext.extend(msPrintDocs.grid.msOrders, Ext.ComponentMgr.types['minishop2-grid-orders'], {

        formatType: function (value) {
            if (value === 'row') {
                value =  'Ряды';
            }
            else {
                value = 'Размеры';
            }
            return value;
        },
        getColumns: function () {
            var all = {
                id: {width: 35},
                customer: {width: 100, renderer: function(val, cell, row) {
                        return miniShop2.utils.userLink(val, row.data['user_id'], true);
                    }},
                num: {width: 50},
                receiver: {width: 100},
                cart_type: {width: 50, renderer: this.formatType},
                createdon: {width: 75, renderer: miniShop2.utils.formatDate},
                updatedon: {width: 75, renderer: miniShop2.utils.formatDate},
                cost: {width: 50, renderer: this._renderCost},
                cart_cost: {width: 50},
                delivery_cost: {width: 75},
                weight: {width: 50},
                status: {width: 75},
                delivery: {width: 75},
                payment: {width: 75},
                //address: {width: 50},
                context: {width: 50},
                actions: {width: 75, id: 'actions', renderer: miniShop2.utils.renderActions, sortable: false},
            };

            var fields = this.getFields();
            var columns = [];
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                if (all[field]) {
                    Ext.applyIf(all[field], {
                        header: _('ms2_' + field),
                        dataIndex: field,
                        sortable: true,
                    });
                    columns.push(all[field]);
                }
            }

            return columns;
        },

        invoice: function (btn, e, row) {
            if (typeof(row) != 'undefined') {
                this.menu.record = row.data;
            }
            var id = this.menu.record.id;

            MODx.Ajax.request({
                url: '/assets/components/extminishop/connector.php',
                params: {
                    action: 'mgr/orders/invoice',
                    id: id
                },
                listeners: {
                    success: {
                        fn: function (r) {
                            window.open(r.object.invoice);
                        }, scope: this
                    }
                }
            });
        },
        torg12: function (btn, e, row) {
            if (typeof(row) != 'undefined') {
                this.menu.record = row.data;
            }
            var id = this.menu.record.id;

            MODx.Ajax.request({
                url: '/assets/components/extminishop/connector.php',
                params: {
                    action: 'mgr/orders/torg12',
                    id: id
                },
                listeners: {
                    success: {
                        fn: function (r) {
                            window.open(r.object.torg12);
                        }, scope: this
                    }
                }
            });
        },
        print: function (btn, e, row) {
            if (typeof(row) != 'undefined') {
                this.menu.record = row.data;
            }
            var id = this.menu.record.id;

            MODx.Ajax.request({
                url: '/assets/components/extminishop/connector.php',
                params: {
                    action: 'mgr/orders/print',
                    id: id
                },
                listeners: {
                    success: {
                        fn: function (r) {
                            var win = open("", "", "width=1024,height=400,status=yes,toolbar=yes,menubar=yes");
                            win.document.open();
                            win.document.write(r.object.doc);
                            win.document.close();
                            win.print();
                        }, scope: this
                    }
                }
            });
        }
    });
    Ext.reg('minishop2-grid-orders', msPrintDocs.grid.msOrders);
}

// Расширяем окно статуса
if (typeof miniShop2.window.CreateStatus !== "undefined") {
    Ext.override(miniShop2.window.CreateStatus, {
        getFields: function (config) {
            return [
                {xtype: 'hidden', name: 'id', id: config.id + '-id'},
                {xtype: 'hidden', name: 'color', id: config.id + '-color'},
                {
                    xtype: 'textfield',
                    id: config.id + '-name',
                    fieldLabel: _('ms2_name'),
                    name: 'name',
                    anchor: '99%',
                }, {
                    xtype: 'colorpalette', fieldLabel: _('ms2_color'),
                    id: config.id + '-color-palette',
                    listeners: {
                        select: function (palette, color) {
                            Ext.getCmp(config.id + '-color').setValue(color)
                        },
                        beforerender: function (palette) {
                            var newcolors = ['00BAB4','F66F66','333333','671A88'].filter(item => {
                                return palette.colors.indexOf(item) === -1;
                            });
                            palette.colors = palette.colors.concat(newcolors);
                            if (config.record['color'] != undefined) {
                                palette.value = config.record['color'];
                            }
                        }
                    },
                }, {
                    layout: 'column',
                    items: [{
                        columnWidth: .5,
                        layout: 'form',
                        items: [{
                            xtype: 'xcheckbox',
                            id: config.id + '-email-user',
                            boxLabel: _('ms2_email_user'),
                            name: 'email_user',
                            listeners: {
                                check: {
                                    fn: function (checkbox) {
                                        this.handleStatusFields(checkbox);
                                    }, scope: this
                                },
                                afterrender: {
                                    fn: function (checkbox) {
                                        this.handleStatusFields(checkbox);
                                    }, scope: this
                                }
                            },
                        }, {
                            xtype: 'textfield',
                            id: config.id + '-subject-user',
                            fieldLabel: _('ms2_subject_user'),
                            name: 'subject_user',
                            anchor: '99%',
                        }, {
                            xtype: 'minishop2-combo-chunk',
                            fieldLabel: _('ms2_body_user'),
                            name: 'body_user',
                            id: config.id + '-body-user',
                            anchor: '99%',
                        }],
                    }, {
                        columnWidth: .5,
                        layout: 'form',
                        items: [{
                            xtype: 'xcheckbox',
                            id: config.id + '-email-manager',
                            boxLabel: _('ms2_email_manager'),
                            name: 'email_manager',
                            listeners: {
                                check: {
                                    fn: function (checkbox) {
                                        this.handleStatusFields(checkbox);
                                    }, scope: this
                                },
                                afterrender: {
                                    fn: function (checkbox) {
                                        this.handleStatusFields(checkbox);
                                    }, scope: this
                                }
                            },
                        }, {
                            xtype: 'textfield',
                            id: config.id + '-subject-manager',
                            fieldLabel: _('ms2_subject_manager'),
                            name: 'subject_manager',
                            anchor: '99%',
                        }, {
                            xtype: 'minishop2-combo-chunk',
                            id: config.id + '-body-manager',
                            fieldLabel: _('ms2_body_manager'),
                            name: 'body_manager',
                            anchor: '99%',
                        }],
                    }]
                }, {
                    xtype: 'textarea',
                    id: config.id + '-description',
                    fieldLabel: _('ms2_description'),
                    name: 'description',
                    anchor: '99%',
                }, {
                    xtype: 'checkboxgroup',
                    hideLabel: true,
                    columns: 3,
                    items: [{
                        xtype: 'xcheckbox',
                        id: config.id + '-active',
                        boxLabel: _('ms2_active'),
                        name: 'active',
                        checked: parseInt(config.record['active']),
                    }, {
                        xtype: 'xcheckbox',
                        id: config.id + '-final',
                        boxLabel: _('ms2_status_final'),
                        description: _('ms2_status_final_help'),
                        name: 'final',
                        checked: parseInt(config.record['final']),
                    }, {
                        xtype: 'xcheckbox',
                        id: config.id + '-fixed',
                        boxLabel: _('ms2_status_fixed'),
                        description: _('ms2_status_fixed_help'),
                        name: 'fixed',
                        checked: parseInt(config.record['fixed']),
                    }, {
                        xtype: 'xcheckbox',
                        id: config.id + '-return_remains',
                        boxLabel: 'При удалении заказа остатки возвращаются',
                        name: 'return_remains',
                        checked: parseInt(config.record['return_remains']),
                    }]
                }
            ];
        }
    });
}

// Расширяем гриды статусов
if (typeof miniShop2.grid.Status !== "undefined") {
    Ext.override(miniShop2.grid.Status, {
        getFields: function () {
            return [
                'id', 'name', 'description', 'color', 'email_user', 'email_manager',
                'subject_user', 'subject_manager', 'body_user', 'body_manager', 'active',
                'final', 'fixed', 'return_remains', 'rank', 'editable', 'actions'
            ];
        },
        getColumns: function (config) {
            return [
                {header: _('ms2_id'), dataIndex: 'id', width: 30},
                {header: _('ms2_name'), dataIndex: 'name', width: 50, renderer: this._renderColor},
                {header: _('ms2_email_user'), dataIndex: 'email_user', width: 50, renderer: this._renderBoolean},
                {header: _('ms2_email_manager'), dataIndex: 'email_manager', width: 50, renderer: this._renderBoolean},
                {header: _('ms2_status_final'), dataIndex: 'final', width: 50, renderer: this._renderBoolean},
                {header: _('ms2_status_fixed'), dataIndex: 'fixed', width: 50, renderer: this._renderBoolean},
                {
                    header: 'При удалении возвращать остатки',
                    dataIndex: 'return_remains',
                    width: 50,
                    renderer: this._renderBoolean
                },
                {header: _('ms2_rank'), dataIndex: 'rank', width: 35, hidden: true},
                {
                    header: _('ms2_actions'),
                    dataIndex: 'actions',
                    id: 'actions',
                    width: 50,
                    renderer: miniShop2.utils.renderActions
                }
            ];
        }
    });
}

// Расширяем гриды товаров в заказе
if (typeof miniShop2.grid.OrderProducts !== "undefined") {
    msPrintDocs.grid.OrderProducts = function (config) {
        Ext.applyIf(config, {
            baseParams: {
                action: 'mgr/orders/product/getlist',
                order_id: config.order_id,
            },
            cls: 'minishop2-grid',
            multi_select: false,
            stateful: true,
            stateId: config.id,
            pageSize: Math.round(MODx.config['default_per_page'] / 4),
        });
        msPrintDocs.grid.OrderProducts.superclass.constructor.call(this, config);
    };

    Ext.extend(msPrintDocs.grid.OrderProducts, Ext.ComponentMgr.types['minishop2-grid-order-products'], {
        getColumns: function () {
            var fields = {
                //id: {hidden: true, sortable: true, width: 40},
                product_id: {hidden: true, sortable: true, width: 40},
                name: {
                    header: _('ms2_name'),
                    width: 100,
                    renderer: function (value, metaData, record) {
                        return miniShop2.utils.productLink(value, record['data']['product_id']);
                    }
                },
                product_weight: {header: _('ms2_product_weight'), width: 50},
                product_price: {header: _('ms2_product_price'), width: 50},
                product_article: {width: 50},
                weight: {sortable: true, width: 50},
                price: {sortable: true, header: _('ms2_product_price'), width: 50},
                count: {sortable: true, width: 50},
                cost: {width: 50},
                options: {width: 100},
                actions: {width: 75, id: 'actions', renderer: miniShop2.utils.renderActions, sortable: false},
            };

            var columns = [];
            for (var i = 0; i < miniShop2.config['order_product_fields'].length; i++) {
                var field = miniShop2.config['order_product_fields'][i];
                if (fields[field]) {
                    Ext.applyIf(fields[field], {
                        header: _('ms2_' + field),
                        dataIndex: field
                    });
                    columns.push(fields[field]);
                }
                else if (/^option_/.test(field)) {
                    if (field === 'option_size') {
                        columns.push(
                            {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50, renderer: msPrintDocs.formatSize}
                        );
                    }
                    else {
                        columns.push(
                            {header: _(field.replace(/^option_/, 'ms2_')), dataIndex: field, width: 50}
                        );
                    }
                }
                else if (/^product_/.test(field)) {
                    if (field === 'product_thumb') {
                        columns.push(
                            {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75, renderer: miniShop2.utils.renderImage}
                        );
                    }
                    else {
                        columns.push(
                            {header: _(field.replace(/^product_/, 'ms2_')), dataIndex: field, width: 75}
                        );
                    }
                }
                else if (/^category_/.test(field)) {
                    columns.push(
                        {header: _(field.replace(/^category_/, 'ms2_')), dataIndex: field, width: 75}
                    );
                }
            }

            return columns;
        },
    });
    Ext.reg('minishop2-grid-order-products', msPrintDocs.grid.OrderProducts);
}
