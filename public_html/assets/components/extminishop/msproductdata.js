miniShop2.combo.sex = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['unit', 'display'],
            data: [
                ['boys', 'Мальчикам'],
                ['girls', 'Девочкам'],
                ['unisex', 'Унисекс']
            ]
        }),
        mode: 'local',
        displayField: 'display',
        valueField: 'unit'
    });
    miniShop2.combo.sex.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.sex, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-sex', miniShop2.combo.sex);

miniShop2.combo.season = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            id: 1,
            fields: ['unit', 'display'],
            data: [
                ['Все','Все'],
                ['Лето','Лето'],
                ['Осень/Весна','Осень/Весна'],
                ['Зима','Зима']
            ]
        }),
        mode: 'local',
        name: 'season',
        hiddenName: 'season',
        displayField: 'display',
        valueField: 'unit'
    });
    miniShop2.combo.season.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.season, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-season', miniShop2.combo.season);

miniShop2.combo.made_in = function (config) {
    config = config || {};
    Ext.applyIf(config,{
        store: new Ext.data.ArrayStore({
            id: 1,
            fields: ['unit', 'display'],
            data: [
                ['Китай','Китай'],
                ['США','США'],
                ['Англия','Англия'],
                ['Турция','Турция'],
                ['Узбекистан','Узбекистан']
            ]
        }),
        mode: 'local',
        name: 'made_in',
        hiddenName: 'made_in',
        displayField: 'display',
        valueField: 'unit'
    });
    miniShop2.combo.made_in.superclass.constructor.call(this, config);
};
Ext.extend(miniShop2.combo.made_in, MODx.combo.ComboBox);
Ext.reg('minishop2-combo-made_in', miniShop2.combo.made_in);

miniShop2.plugin.extminishop = {
    // Изменение полей для панели товара
    getFields: function (config) {
        return {
            sex: {
                xtype: 'minishop2-combo-sex',
                description: '<b>[[+sex]]</b><br />' + _('ms2_product_sex_help'),
            },
            age: {
                xtype: 'minishop2-combo-autocomplete',
                description: '<b>[[+age]]</b><br />' + _('ms2_product_age_help'),
            },
            unit: {
                xtype: 'minishop2-combo-autocomplete',
                description: '<b>[[+unit]]</b><br />' + _('ms2_product_unit_help'),
            },
            vendor_skuid: {
                xtype: 'minishop2-combo-autocomplete',
                description: '<b>[[+vendor_skuid]]</b><br />' + _('ms2_product_vendor_skuid_help'),
            },
            vendor_url: {
                xtype: 'minishop2-combo-autocomplete',
                description: '<b>[[+vendor_url]]</b><br />' + _('ms2_product_vendor_url_help'),
            },
            fabric: {
                xtype: 'minishop2-combo-autocomplete',
                description: '<b>[[+fabric]]</b><br />' + _('ms2_product_fabric_help')
            },
            soon_available: {
                xtype: 'xcheckbox',
                inputValue: 1,
                checked: parseInt(config.record.soon_available),
                description: '<b>[[+soon_available]]</b><br />' + _('ms2_product_soon_available_help')
            },
            parsing_data: {
                xtype: 'minishop2-xdatetime',
                value: config.record.parsing_data,
                description: '<b>[[*parsing_data]]</b><br/>' + _('ms2_product_parsing_data_help')
            },
            season: {
                xtype: 'minishop2-combo-season',
                description: '<b>[[+season]]</b><br />' + _('ms2_product_season_help')
            },
            made_in: {
                xtype: 'minishop2-combo-made_in',
                description: '<b>[[+made_in]]</b><br />' + _('ms2_product_made_in_help')
            },
        }
    },
    // Изменение колонок таблицы товаров в категории
    getColumns: function () {
        return {
            sex: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-sex',
                    name: 'sex'
                }
            },
            age: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-autocomplete',
                    name: 'age'
                }
            },
            unit: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-autocomplete',
                    name: 'unit'
                }
            },
            vendor_skuid: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-autocomplete',
                    name: 'vendor_skuid'
                }
            },
            vendor_url: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-autocomplete',
                    name: 'vendor_url'
                }
            },
            fabric: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-autocomplete',
                    name: 'fabric'
                }
            },
            soon_available: {
                width: 50,
                sortable: true,
                editor: {
                    xtype: 'combo-boolean',
                    renderer: 'boolean'
                }
            },
            parsing_data: {
                width: 50,
                sortable: true,
                editor: {xtype: 'minishop2-xdatetime', timePosition: 'below'},
                renderer: miniShop2.utils.formatDate
            },
            season: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-season',
                    name: 'season'
                }
            },
            made_in: {
                width: 100,
                sortable: false,
                editor: {
                    xtype: 'minishop2-combo-made_in',
                    name: 'made_in'
                }
            }
        }
    }
};