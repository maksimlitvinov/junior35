<?php
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
require_once MODX_CORE_PATH . 'model/modx/modx.class.php';
$modx = new modX();
$modx->initialize('web');
$modx->getService('error', 'error.modError', '', '');

if (!$modx->loadClass('favorite', MODX_CORE_PATH . 'components/favorite/model/favorite/', true, true)) {
    return false;
}
/** @var Favorite $favorite */
$favorite = $modx->getService('favorite');

if (!isset($_REQUEST['action']) || !method_exists($favorite, $_REQUEST['action'])) {return false;}
$action = $_REQUEST['action'];

if (!isset($_REQUEST['rid']) || empty($_REQUEST['rid'])) {return false;}
$rid = $_REQUEST['rid'];

$result = $favorite->{$action}($rid);

echo $modx->toJSON($result);

@session_write_close();
exit;