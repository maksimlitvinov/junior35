var Favorites = {

    sel: {
        vaf_btn: '.add-to-favorites',
        vaf_count: '.vaf_count',
        product_card: '.product-card'
    },

    classes: {
        added: 'added'
    },

    setup: function (config) {
        this.config = config;
        this.url = config.actionUrl || '/assets/components/favorite/action.php';
    },

    init: function (config) {
        this.setup(config);
        this.load();
    },

    load: function () {
        var self = this;

        $(document).on('click', this.sel.vaf_btn, function (e) {
            e.preventDefault();
            var resource = $(this).data('id');
            if (resource == undefined) {return false;}
            if ($(this).hasClass(self.classes.added)) {
                self.remove(resource, this);
            }
            else {
                self.add(resource, this);
            }
        });
    },

    add: function (rid, el) {
        this.send({
            action: 'add',
            rid: rid
        }, el);
    },

    remove: function (rid, el) {
        this.send({
            action: 'remove',
            rid: rid
        }, el);
    },

    send: function (data, el) {
        if (data.action === null) {
            console.error('Не указан экшен');
            return false;
        }
        $.ajax({
            url: this.url,
            type: 'POST',
            dataType: 'json',
            cache: 'false',
            context: $(el),
            data: data,
            success: function (response) {
                Favorites.success(response, this);
            }
        });
    },

    success: function (response, el) {
        $(el).toggleClass(Favorites.classes.added);
        $(Favorites.sel.vaf_count).text(response.count);
        if (Favorites.config.current_page == Favorites.config.fav_page) {
            $(el).closest(Favorites.sel.product_card).remove();
            if (response.count == 0) {
                location.reload();
            }
        }
    }
};

$(document).ready(function () {
    favoriteConfig = favoriteConfig || {};
    Favorites.init(favoriteConfig);
});