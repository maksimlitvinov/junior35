<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');


$products = $modx->getIterator('msProductData', array(
    'vendor_skuid:IS NOT' => NULL,
    'vendor_skuid:!=' => ''
));

if (!$modx->loadClass('remains', MODX_CORE_PATH . 'components/remains/model/remains/', true, true)) {return false;}
$remains = $modx->getService('remains');

foreach ($products as $product) {
    $itemId = 'abb-' . $product->get('vendor_skuid');
    $url = 'http://otapi.net/OtapiWebService2.asmx/GetItemFullInfo?instanceKey=6e062944-5a2d-47cc-b1e0-1629e8345c94'
        . '&language=en'
        . '&itemId=' . $itemId
        . '&sessionId=&blockList=';

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HEADER, 0);

    $result = curl_exec($curl);
    if ($result === FALSE) {
        echo "cURL Error: " . curl_error($curl); die();
    }

    $xmlObject = simplexml_load_string($result);
    $in_stock = 0;

    if ((string)$xmlObject->ErrorCode == 'Ok') {
        $in_stock = (int)$xmlObject->OtapiItemFullInfo->MasterQuantity;
    }

    if ($in_stock == 0) {
        $product->set('in_stock', $in_stock);
        $product->set('available_rows', $in_stock);
    }
    else {
        $count_in_row = $product->get('count_in_row');
        $product->set('in_stock', $in_stock);
        $product->set('available_rows', (int)($in_stock / $count_in_row));
    }
    $product->save();

    curl_close($curl);
}