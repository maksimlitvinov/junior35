<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

if (!$modx->loadClass('ExtOrder', MODX_CORE_PATH . 'components/extorder/model/extorder/', true, true)) {return false;}
/** @var ExtOrder $extOrder */
$extOrder = $modx->getService('ExtOrder');

/** @var array|null $orders */
$orders = $modx->getCollection('msOrder', array(
    'status:IN' => array(1,2),
    'payment_date:IS' => NULL,
    'cart_type' => 'size'
));

if (empty($orders)) {return false;}

/** @var msOrder $order */
foreach ($orders as $order) {

    /** @var DateInterval $di */
    $di = new DateInterval('P1D');
    /** @var DateTime $date_end_reservation */
    $date_end_reservation = (new DateTime($order->get('date_end_reservation')))->add($di);
    /** @var DateTime $now_date */
    $now_date = new DateTime();

    if ($now_date < $date_end_reservation) {continue;}

    // Костыль! Типа событие.
    //$extOrder->removeOrder($order);
    // Удаляем заказ с товарами
    //$order->remove();

    $order->set('status', 4);
    $order->save();
}