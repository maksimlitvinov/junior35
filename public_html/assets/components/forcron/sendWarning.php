<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/** @var pdoTools $pdoTools */
$pdoTools = $modx->getService('pdoTools');
if (!$modx->loadClass('ExtOrder', MODX_CORE_PATH . 'components/extorder/model/extorder/', true, true)) {return false;}
/** @var ExtOrder $extOrder */
$extOrder = $modx->getService('ExtOrder');

$reserv_day_size = $modx->getOption('reserv_day_size', array(), 5);
$reserv_day_row = $modx->getOption('reserv_day_row', array(), 0);

$reserv_day_size_first = round($reserv_day_size / 2);
if (!empty($reserv_day_row)) {
    $reserv_day_row_first = round($reserv_day_row / 2);
}

// Первое письмо
$qFirst = $modx->newQuery('msOrder');
$qFirst->where(array(
    'status:IN' => array(1,2),
    'first_warning_message IS NULL',
    'cart_type' => 'size'
));
$orders_first = $modx->getCollection('msOrder', $qFirst);
if (!empty($orders_first)) {
    /** @var msOrder $item */
    foreach ($orders_first as $item) {
        $di_first = new DateInterval('P0D');
        $date_end_reservation = new DateTime($item->get('date_end_reservation'));


        if (empty($reserv_day_row) && $item->get('cart_type') == 'row') {continue;}

        if (!empty($reserv_day_row)) {
            if ($item->get('cart_type') == 'row') {$di_first->d += $reserv_day_row_first;}
            else {$di_first->d += $reserv_day_size_first;}
        }
        else {
            $di_first->d += $reserv_day_size_first;
        }

        $tmp_first = (new DateTime)->add($di_first);

        if ($tmp_first >= $date_end_reservation) {
            $text_first_latter = $modx->getOption('tpl_letter_fist_warning', array(), '');
            $tpl_first_latter = $pdoTools->getChunk('@INLINE ' . $text_first_latter, array(
                'date_rm' => date('d.m.Y', strtotime($item->get('date_end_reservation'))),
                'order_id' => $item->get('id'),
                'num' => $item->get('num'),
                'date_warn' => date('d.m.Y', strtotime($item->get('date_end_reservation') . ' - ' . $reserv_day_size_first . 'day'))
            ));
            $extOrder->sendWarning($item, $tpl_first_latter, 'first_warning_message');
        }
    }
}

// Второе письмо
$qSc = $modx->newQuery('msOrder');
$qSc->where(array(
    'status:IN' => array(1,2),
    'second_warning_letter:IS' => NULL,
    'first_warning_message' => '1',
    'cart_type' => 'size'
));
$orders_sc = $modx->getCollection('msOrder', $qSc);
if (!empty($orders_sc)) {
    foreach ($orders_sc as $item) {

        if (empty($reserv_day_row) && $item->get('cart_type') == 'row') {continue;}

        $now = new DateTime();
        $tmp_sc = new DateTime($item->get('date_end_reservation'));
        $tmp_sc = $tmp_sc->add(new DateInterval('P1D'));
        $result = $now->diff($tmp_sc);

        if ($result->d == 0) {
            $text_sc_latter = $modx->getOption('tpl_letter_sc_warning', array(), '');
            $tpl_sc_latter = $pdoTools->getChunk('@INLINE ' . $text_sc_latter, array(
                'data_rm' => date('d.m.Y', strtotime($item->get('date_end_reservation'))),
                'order_id' => $item->get('id'),
                'num' => $item->get('num')
            ));
            $extOrder->sendWarning($item, $tpl_sc_latter, 'second_warning_letter');
        }
    }
}

// Письмо при статусе "Предоплата"
$qPrepay = $modx->newQuery('msOrder');
$qPrepay->where(array(
    'status' => 5,
    'cart_type' => 'size'
));
$orders_prepay = $modx->getCollection('msOrder', $qPrepay);
if (!empty($orders_prepay)) {
    foreach ($orders_prepay as $item) {
        $date_end_reservation = new DateTime($item->get('date_end_reservation'));
        $now_date = new DateTime();

        if ($now_date >= $date_end_reservation) {
            $di_prepay = new DateInterval('P3D');
            $text_prepay_latter = $modx->getOption('tpl_letter_prepay_warning', array(), '');
            $tpl_prepay_latter = $pdoTools->getChunk('@INLINE ' . $text_prepay_latter, array(
                'data_rm' => date('d.m.Y', strtotime($item->get('date_end_reservation'))),
                'order_id' => $item->get('id'),
            ));
            $extOrder->sendWarning($item, $tpl_prepay_latter, '', (new DateTime)->add($di_prepay)->format('Y-m-d'));
        }
    }
}