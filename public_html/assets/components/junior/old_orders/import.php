<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

$q = $modx->newQuery('modUserProfile');
$q->where(array('old_id:>' => 0));
$profiles = $modx->getIterator('modUserProfile', $q);

$date_format = 'Y-m-d G:i:s';
$statuses = array(
    'Новый' => 1,
    'В работе' => 2,
    'Отменен' => 4,
    'Отправлен' => 3,
);
$deliverys = array(
    'Почта России' => 5,
    'Курьерская служба СДЭК' => 1,
    'Транспортная компания ПЭК' => 7,
    'Транспортная компания Энергия' => 4,
    'Транспортная компания Деловые Линии' => 3,
    'Транспортная компания КИТ' => 6,
    'Курьерская служба Boxberry' => 8
);
$payments = array(
    'Банковской картой' => 3,
    'Оплата на расчетный счет' => 4,
);

/** @var modUserProfile $profile */
foreach ($profiles as $profile) {
    $url = 'http://old.junior35.ru/get/order.php?user=' . $profile->get('old_id');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot (http://mysite.ru)');
    $data = curl_exec($ch);
    curl_close($ch);

    $orders = $modx->fromJSON($data);

    foreach ($orders as $order) {
        $date_create = date($date_format, $order['DATE_CREATE_UNIX']);
        $date_update = date($date_format, $order['TIMESTAMP_X_UNIX']);
        $status = isset($statuses[$order['PROPERTY_STATUS_VALUE_VALUE']]) ? $statuses[$order['PROPERTY_STATUS_VALUE_VALUE']] : 6;
        $delivery = isset($deliverys[$order['PROPERTY_DELIVERY_TYPE_VALUE_VALUE']]) ? $deliverys[$order['PROPERTY_DELIVERY_TYPE_VALUE_VALUE']] : 0;
        $payment = isset($payments[$order['PROPERTY_PAYMENT_TYPE_VALUE_VALUE']]) ? $payments[$order['PROPERTY_PAYMENT_TYPE_VALUE_VALUE']] : 0;

        $cart_type = 'size';
        $items = array();
        foreach ($order['PROPERTY_ITEMS_DESCRIPTION'] as $i => $item) {
            $item = unserialize(htmlspecialchars_decode($item));
            $item['id'] = $order['PROPERTY_ITEMS_VALUE'][$i];
            if ($item['size'] == 'all') {$cart_type = 'row';}
            $items[] = $item;
        }

        /** @var msOrder $msorder */
        $msorder = $modx->newObject('msOrder');

        $msorder->set('createdon', $date_create);
        $msorder->set('updatedon', $date_update);
        $msorder->set('user_id', $profile->get('internalKey'));
        $msorder->set('num', $order['ID']);
        $msorder->set('cost', $order['PROPERTY_AMOUNT_VALUE']);
        $msorder->set('cart_cost', $order['PROPERTY_AMOUNT_VALUE']);
        $msorder->set('status', $status);
        $msorder->set('delivery', $delivery);
        $msorder->set('payment', $payment);
        $msorder->set('context', 'web');
        $msorder->set('cart_type', $cart_type);
        if (!empty($order['PROPERTY_PROMOCODE_VALUE'])) {
            $msorder->set('promo_code', $order['PROPERTY_PROMOCODE_VALUE']);
        }
        if (!empty($order['PROPERTY_PAYMENT_DATE_VALUE'])) {
            $msorder->set('payment_date', date($date_format, strtotime($order['PROPERTY_PAYMENT_DATE_VALUE'])));
        }
        if (!empty($order['PROPERTY_DATE_SEND_VALUE'])) {
            $msorder->set('shipment_date', date($date_format, strtotime($order['PROPERTY_DATE_SEND_VALUE'])));
        }
        if (!empty($order['PROPERTY_SHIPPING_WAREHOUSE_VALUE'])) {
            $msorder->set('shipment_warehouse', $order['PROPERTY_SHIPPING_WAREHOUSE_VALUE']);
        }
        if (!empty($order['PROPERTY_SEND_NUMBER_VALUE'])) {
            $msorder->set('departure_number', $order['PROPERTY_SEND_NUMBER_VALUE']);
        }
        if (!empty($order['PROPERTY_CARD_NUMBER_FOR_RETURN_VALUE'])) {
            $msorder->set('card_number_refund', $order['PROPERTY_CARD_NUMBER_FOR_RETURN_VALUE']);
        }
        if (!empty($order['PROPERTY_RETURN_STATUS_VALUE'])) {
            $msorder->set('status_refund', $order['PROPERTY_RETURN_STATUS_VALUE']);
        }
        if (!empty($order['PROPERTY_RETURN_DATE_VALUE'])) {
            $msorder->set('date_refund', date($date_format, strtotime($order['PROPERTY_RETURN_DATE_VALUE'])));
        }
        if (!empty($order['PROPERTY_WARNING_LETTER_1_VALUE'])) {
            $msorder->set('first_warning_message', $order['PROPERTY_WARNING_LETTER_1_VALUE']);
        }
        if (!empty($order['PROPERTY_WARNING_LETTER_2_VALUE'])) {
            $msorder->set('second_warning_letter', $order['PROPERTY_WARNING_LETTER_2_VALUE']);
        }

        /** @var msOrderAddress $msorderaddress */
        $msorderaddress = $modx->newObject('msOrderAddress');
        $msorderaddress->set('user_id', $profile->get('internalKey'));
        $msorderaddress->set('createdon', $date_create);
        $msorderaddress->set('updatedon', $date_update);
        $fio = $order['PROPERTY_LASTNAME_VALUE'] . ' ' . $order['PROPERTY_FIRSTNAME_VALUE'] . ' ' . $order['PROPERTY_SECONDNAME_VALUE'];
        $msorderaddress->set('receiver', $fio);
        $msorderaddress->set('phone', $order['PROPERTY_PHONE_VALUE']);
        $msorderaddress->set('street', $order['PROPERTY_ADDRESS_VALUE']);

        $msorder->addOne($msorderaddress);
        $msorder->save();
        $msorderaddress->save();

        // Добавляем товары
        foreach ($items as $product) {
            $msorderproduct = $modx->newObject('msOrderProduct');
            $msproduct = $modx->getObject('msProductData', array('old_id' => $product['id']));
            if (empty($msproduct)) {continue;}
            $res = $modx->getObject('msProduct', $msproduct->get('id'));
            $msorderproduct->set('product_id', $msproduct->get('id'));
            $msorderproduct->set('count', $product['quantity']);
            $msorderproduct->set('price', $product['amount']);
            $msorderproduct->set('cost', $product['amount'] * $product['quantity']);
            $msorderproduct->set('name', $res->get('pagetitle'));
            $msorderproduct->set('options', $modx->toJSON(array('size'=>$product['size'])));
            $msorderproduct->addOne($msorder);
            $msorderproduct->save();
        }

    }

}






