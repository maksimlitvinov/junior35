<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

$date_format = 'Y-m-d G:i:s';
$q = $modx->newQuery('msProduct');
$q->where(array('class_key' => 'msProduct','published:>' => '0'));
$q->sortby('publishedon','ASC');
$products = $modx->getCollection('modResource', $q);
$start_date = '01.04.2020 00:00:00';

$i = 1;
foreach ($products as $product) {
    $pd = $product->get('publishedon');
    if ($pd == 0) {$pd = $start_date;}
    $npd = (int)strtotime($pd . " + " . (string)$i . " second");
    $product->set('publishedon', $npd);
    $product->save();
    $i++;
}