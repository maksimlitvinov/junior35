<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

$ch = curl_init();
// GET запрос указывается в строке URL
curl_setopt($ch, CURLOPT_URL, 'http://old.junior35.ru/get/products.php');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot (http://mysite.ru)');
$data = curl_exec($ch);
curl_close($ch);

$products = $modx->fromJSON($data);

foreach ($products as $product) {
    $pagetitle = trim($product['NAME']);
    $arr_title = explode('/', $product['NAME']);
    $longtitle = trim($arr_title[0]);
    $menutitle = trim($arr_title[0]);
    $article = '';
    if (count($arr_title) > 1) {
        $article = preg_replace('/[^a-zA-Z0-9-]/', '', $arr_title[1]);
    }

    $ms_product = array();
    if (!empty($article)) {
        $ms_product = $modx->getObject('msProductData', array('article' => $article));
    }

    if (!empty($ms_product)) {
        if (empty($ms_product->get('old_id')) || $ms_product->get('old_id') != $product['ID']) {
            $ms_product->set('old_id', $product['ID']);
            $ms_product->save();
        }
        continue;
    }

    $vendor_url = $product['PROPERTY_PROVIDER_URL_VALUE'];
    $arr_vendor_url = explode('/', $vendor_url);
    $last_item_vendor_url = count($arr_vendor_url) - 1;
    $arr_skuid = explode('.', $arr_vendor_url[$last_item_vendor_url]);
    $skuid = '';
    if (count($arr_skuid) > 1) {
        $skuid = $arr_skuid[0];
    }

    $res = array();
    $res['class_key'] = 'msProduct';
    $res['template'] = 3;
    $res['parent'] = 2;
    $res['published'] = 0;
    $res['show_in_tree'] = 1;
    $res['unit'] = 'шт.';
    $res['pagetitle'] = $pagetitle;
    $res['longtitle'] = $longtitle;
    $res['menutitle'] = $menutitle;
    $res['article'] = $article;
    $res['old_id'] = $product['ID'];
    $res['price'] = $product['PROPERTY_PRICE_VALUE'];
    $res['vendor_url'] = $vendor_url;
    $res['fabric'] = $product['PROPERTY_MATERIAL_VALUE'];
    $res['made_in'] = $product['PROPERTY_MANUFACTURER_VALUE'];
    $res['vendor_skuid'] = $skuid;

    $response = $modx->runProcessor('resource/create', $res);
}