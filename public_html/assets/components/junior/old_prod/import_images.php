<?php
define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

$old_site = 'http://old.junior35.ru';

$ch = curl_init();
// GET запрос указывается в строке URL
curl_setopt($ch, CURLOPT_URL, 'http://old.junior35.ru/get/products.php');
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_USERAGENT, 'PHP Bot (http://mysite.ru)');
$data = curl_exec($ch);
curl_close($ch);

$products = $modx->fromJSON($data);

// ID => old_id
// PREVIEW_PICTURE
// DETAIL_PICTURE
// PROPERTY_PHOTO_VALUE
// PROPERTY_MORE_PHOTO_VALUE

foreach ($products as $product) {
    $ms_product = $modx->getObject('msProductData', array('old_id' => $product['ID']));
    if (empty($ms_product)) {continue;}

    $ms_image = $ms_product->get('image');
    if (!empty($ms_image)) {continue;}

    $images = array();
    if (!empty($product['PREVIEW_PICTURE'])) {
        $images[] = array(
            'path' => $product['PREVIEW_PICTURE'],
            'src' => file_get_contents($old_site . $product['PREVIEW_PICTURE'])
        );
    }
    if (!empty($product['DETAIL_PICTURE'])) {
        $images[] = array(
            'path' => $product['DETAIL_PICTURE'],
            'src' => file_get_contents($old_site . $product['DETAIL_PICTURE'])
        );
    }
    if (!empty($product['PROPERTY_PHOTO_VALUE'])) {
        $images[] = array(
            'path' => $product['PROPERTY_PHOTO_VALUE'],
            'src' => file_get_contents($old_site . $product['PROPERTY_PHOTO_VALUE'])
        );
    }
    if (!empty($product['PROPERTY_MORE_PHOTO_VALUE'])) {
        $images[] = array(
            'path' => $product['PROPERTY_MORE_PHOTO_VALUE'],
            'src' => file_get_contents($old_site . $product['PROPERTY_MORE_PHOTO_VALUE'])
        );
    }

    if (empty($images)) {continue;}

    // Создаем папку для изображений
    $path = MODX_ASSETS_PATH . 'images/old_site/' . $ms_product->get('id');
    mkdir($path, 0777, true);

    foreach ($images as $image) {
        // Получаем имя файла
        $img_file_path = explode('/', $image['path']);
        $img_name = $img_file_path[(count($img_file_path) - 1)];

        $img = $path . '/' . $img_name;
        file_put_contents($img, $image['src']);

        // Добавляем файл в галлерею
        $resp = $modx->runProcessor('gallery/upload',
            array('id' => $ms_product->get('id'), 'file' => $img),
            array('processors_path' => MODX_CORE_PATH.'components/minishop2/processors/mgr/')
        );
        if ($resp->isError()) {
            $modx->log(modX::LOG_LEVEL_ERROR, "Error on upload \"$img\": \n". print_r($resp->getAllErrors(), 1));
        }
        else {
            //$modx->log(modX::LOG_LEVEL_INFO, "Successful upload  \"$img\": \n". print_r($resp->getObject(), 1));
        }
    }

}

// Удаляем оригиналы изображений
RDir(MODX_ASSETS_PATH . 'images/old_site/');
function RDir( $path ) {
    // если путь существует и это папка
    if ( file_exists( $path ) AND is_dir( $path ) ) {
        // открываем папку
        $dir = opendir($path);
        while ( false !== ( $element = readdir( $dir ) ) ) {
            // удаляем только содержимое папки
            if ( $element != '.' AND $element != '..' )  {
                $tmp = $path . '/' . $element;
                chmod( $tmp, 0777 );
                // если элемент является папкой, то
                // удаляем его используя нашу функцию RDir
                if ( is_dir( $tmp ) ) {
                    RDir( $tmp );
                    // если элемент является файлом, то удаляем файл
                } else {
                    unlink( $tmp );
                }
            }
        }
        // закрываем папку
        closedir($dir);
        // удаляем саму папку
        if ( file_exists( $path ) ) {
            rmdir( $path );
        }
    }
}