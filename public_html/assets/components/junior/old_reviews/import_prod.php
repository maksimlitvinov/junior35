<?php
/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/extorder/vendor/autoload.php';

/* Тип парсера */
$inputFileType = 'Html';
/* Путь к файлу */
$inputFileName = './revievs_prod.xls';
/* Указываем сколько строк пропустить от начала */
$offset = 1;
/* Формат даты */
$date_format = 'Y-m-d G:i:s';
/* Определяем соответствие колонок */
$columns = [
    'A' => null, // Название
    'B' => ['name' => 'published', 'type' => 'bool'], // Активность
    'C' => null, // Сортировка
    'D' => ['name' => 'createdon', 'type' => 'date'], // Дата изменения
    'E' => ['name' => 'text', 'type' => 'string'], // Отзыв
    'F' => ['name' => 'product_id', 'type' => 'getint'], // Товар
    'G' => ['name' => 'user_id', 'type' => 'getint'], // Кем создан
    'H' => null, // Пользователь
    'I' => null // ID
];

/**  Create a new Reader of the type defined in $inputFileType  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
/**  Advise the Reader that we only want to load cell data  **/
$reader->setReadDataOnly(true);
/**  Load $inputFileName to a Spreadsheet Object  **/
$spreadsheet = $reader->load($inputFileName);
$worksheet = $spreadsheet->getActiveSheet();

foreach ($worksheet->getRowIterator() as $row) {
    // Пропускаем указанное кол-во строк. Строки начинаются с 1.
    if ($row->getRowIndex() <= $offset) {continue;}


    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(FALSE);

    $fields = [];
    foreach ($cellIterator as $cell) {
        $current_column = $cell->getColumn(); // Получаем колонку(A,B,C...)
        if (!isset($columns[$current_column]) || empty($columns[$current_column])) {continue;}

        $column = $columns[$current_column];
        $value = $cell->getValue();

        switch ($column['type']) {
            case 'string':
                $fields[$column['name']] = (string)$value;
                break;
            case 'bool':
                $fields[$column['name']] = $value == 'Да' ? 1 : 0;
                break;
            case 'date':
                $fields[$column['name']] = date($date_format, strtotime($value));
                break;
            case 'getint':
                $value_array = explode('[', $value);
                $value_tmp = explode(']', $value_array[1]);
                $fields[$column['name']] = (int)$value_tmp[0];
                break;
            default:
                $fields[$column['name']] = $value;
        }

    }

    $ms_product = $modx->getObject('msProductData', ['old_id' => $fields['product_id']]);
    if (empty($ms_product)) {continue;}
    $fields['product_id'] = $ms_product->get('id');

    $ticketThread = $modx->getObject('TicketThread', ['resource' => $fields['product_id']]);
    $user = $modx->getObject('modUserProfile', ['old_id' => $fields['user_id']]);
    if (empty($user)) {continue;}
    $fields['user_id'] = $user->get('internalKey');
    if (empty($ticketThread)) {
        $ticketThread = $modx->newObject('TicketThread');
        $ticketThread->set('resource', $fields['product_id']);
        $ticketThread->set('name', 'resource-' . $fields['product_id']);
        $ticketThread->set('subscribers', '[' . $fields['user_id'] . ']');
        $ticketThread->set('createdon', $fields['createdon']);
        $ticketThread->set('createdby', $fields['user_id']);
        $ticketThread->save();
    }

    $ticketComment = $modx->newObject('TicketComment');
    $ticketComment->set('text', $fields['text']);
    $ticketComment->set('raw', $fields['text']);
    $ticketComment->set('name', $user->get('fullname'));
    $ticketComment->set('email', $user->get('email'));
    $ticketComment->set('createdon', $fields['createdon']);
    $ticketComment->set('createdby', $fields['user_id']);
    $ticketComment->set('editedon', $fields['createdon']);
    $ticketComment->set('editedby', $fields['user_id']);
    $ticketComment->set('published', $fields['published']);

    $ticketComment->addOne($ticketThread);
    $ticketComment->save();
}