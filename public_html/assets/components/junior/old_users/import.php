<?php
/**
 * Для работы требуется php7.2
 * На beget.com в консоле команда будет такой: php7.2 ./import.php
 *
 * Скрипт расчитан на добавление и обновление пользователей и если пользователь
 * новый или email изменился, то будет отправлено сообщение на почту спросьюой восстановить пароль.
 *
 * Используется PhpOffice, но так как Bitrix генерирует файл с расширением .xls,
 * а по факту внутри HTML, то происходит парсинг HTML разметки.
 *
 * ВАЖНО: Данные для поля username должны находиться в первой колонке (A).
 * Именно по этой колонке проверяется наличие пользователя в базе.
 */

/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

exit('Скрипт отключен. Перед включением хорошо подумай - надо ли он тебе?');

/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'components/extorder/vendor/autoload.php';

/* Тип парсера */
$inputFileType = 'Html';
/* Путь к файлу */
$inputFileName = './junior_users.xls';
/* Указываем сколько строк пропустить от начала */
$offset = 1;
/* Определяем соответствие колонок */
$columns = [
    'A' => ['field' => 'username', 'type' => 'string', 'entity' => 'user'],
    'B' => ['field' => 'active', 'type' => 'bool', 'entity' => 'user'],
    'C' => null,
    'D' => ['field' => 'fullname', 'type' => 'part', 'num' => 2, 'entity' => 'profile'],
    'E' => ['field' => 'fullname', 'type' => 'part', 'num' => 1, 'entity' => 'profile'],
    'F' => ['field' => 'fullname', 'type' => 'part', 'num' => 3, 'entity' => 'profile'],
    'G' => ['field' => 'email', 'type' => 'string', 'entity' => 'profile'],
    'H' => null,
    'I' => ['field' => 'old_id', 'type' => 'int', 'entity' => 'profile'],
    'J' => ['field' => 'createdon', 'type' => 'date', 'entity' => 'user'],
    'K' => null,
    'L' => null,
    'M' => null,
    'N' => null,
    'O' => null,
    'P' => null,
    'Q' => ['field' => 'mobilephone', 'type' => 'string', 'entity' => 'profile'],
    'R' => null,
    'S' => ['field' => 'city', 'type' => 'string', 'entity' => 'profile'],
    'T' => ['field' => 'address', 'type' => 'string', 'entity' => 'profile'],
    'U' => null,
    'V' => null,
    'W' => null,
    'X' => null,
    'Y' => null,
    'Z' => null,
    'AA' => null,
    'AB' => null,
    'AC' => null,
    'AD' => null,
];

/**  Create a new Reader of the type defined in $inputFileType  **/
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
/**  Advise the Reader that we only want to load cell data  **/
$reader->setReadDataOnly(true);
/**  Load $inputFileName to a Spreadsheet Object  **/
$spreadsheet = $reader->load($inputFileName);
$worksheet = $spreadsheet->getActiveSheet();

foreach ($worksheet->getRowIterator() as $row) {
    // Пропускаем указанное кол-во строк. Строки начинаются с 1.
    if ($row->getRowIndex() <= $offset) {continue;}

    // Усыпаем на 2 сек. так как у beget ограничения на отправку = 30 писем в минуту.
    //sleep(2);

    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(FALSE);
    $needSendEmail = true;
    $fio = [];

    /** @var modUser $user */
    $user = $modx->newObject('modUser');
    /** @var modUserProfile $profile */
    $profile = $modx->newObject('modUserProfile');

    // Устанавливаем данные поьзователя
    foreach ($cellIterator as $cell) {
        $current_column = $cell->getColumn(); // Получаем колонку(A,B,C...)
        $current_row = $cell->getRow(); // Получаем строку (1,2,3...)

        if (!isset($columns[$current_column]) || empty($columns[$current_column])) {continue;}

        $column = $columns[$current_column];
        $value = $cell->getValue();

        /*
         * Если пользователь существует, то будем его обновлять.
         * Работает если логин идет первойколонкой.
         */
        if ($column['field'] == 'username') {
            $tmp_user = $modx->getObject('modUser', [$column['field'] => $value]);
            if (!empty($tmp_user)) {
                $user = $tmp_user;
                $profile = $modx->getObject('modUserProfile', ['internalKey' => $tmp_user->get('id')]);
            }
        }

        // Определяем нужно ли отправлять письмо
        if ($column['field'] == 'email') {
            $old_email = $profile->get('email');
            if (!empty($old_email) && $old_email == $value) {
                $needSendEmail = false;
            }
        }

        switch ($column['type']) {
            case 'string':
                ${$column['entity']}->set($column['field'], (string)$value);
                break;
            case 'bool':
                ${$column['entity']}->set($column['field'], $value == 'Да' ? 1 : 0);
                break;
            case 'part':
                $fio[$column['num']] = $value;
                ksort($fio);
                ${$column['entity']}->set($column['field'], implode(' ', $fio));
                break;
            case 'int':
                ${$column['entity']}->set($column['field'], (int)$value);
                break;
            case 'date':
                ${$column['entity']}->set($column['field'], strtotime($value));
                break;
            default:
                ${$column['entity']}->set($column['field'], $cell->getValue());
        }

    }

    if (empty($user->get('username'))) {continue;}
    $user->addOne($profile);

    // Добавляем пользователя в группу
    $groupList = ['Buyers'];
    $groups = [];
    foreach ($groupList as $item) {
        if ($user->isMember($item)) {continue;}
        /** @var modUserGroup $group */
        $group = $modx->getObject('modUserGroup', array('name' => $item));
        $user->set('primary_group', $group->get('id')); // Устанавливаем или меняем основную группу
        /** @var modUserGroupMember $groupMember */
        $groupMember = $modx->newObject('modUserGroupMember');
        $groupMember->set('user_group', $group->get('id'));
        $groupMember->set('role', 1); // 1 - это членство с ролью Member
        $groups[] = $groupMember;
    }
    $user->addMany($groups);

    // Сохраняем пользователя
    $profile->save();
    $user->save();

    // Не будем отправлять письмо, если E-mail не менялся или он не валидный
    if (!$needSendEmail) {continue;}
    if (!filter_var($profile->get('email'), FILTER_VALIDATE_EMAIL)) {continue;}

    /** @var pdoTools $pdoTools */
    $pdoTools = $modx->getService('pdoTools');
    $body = $pdoTools->getChunk('@FILE chunks/letters/importUser.tpl', array_merge($user->toArray(), $profile->toArray()));
    $emailsender = $modx->getOption('emailsender', [], 'info@webportal35.ru');
    $site_name = $modx->getOption('site_name', []);

    /** @var modPHPMailer $mail */
    $mail = $modx->getService('mail', 'mail.modPHPMailer');
    $mail->set(modMail::MAIL_BODY, $body);
    $mail->set(modMail::MAIL_FROM, $emailsender);
    $mail->set(modMail::MAIL_FROM_NAME, $site_name);
    $mail->set(modMail::MAIL_SUBJECT, 'Требуется восстановление пароля на сайте Джуниор');
    $mail->address('to', $profile->get('email'));
    $mail->address('reply-to', $emailsender);
    $mail->setHTML(true);
    if (!$mail->send()) {
        $this->xpdo->log(xPDO::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: '.$mail->mailer->ErrorInfo);
        $mail->reset();
    }
    else {
        $mail->reset();
    }
}

