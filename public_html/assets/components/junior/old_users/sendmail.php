<?php
/*
 * Скрипт выбирает пользоватеей с неустановленным паролем
 * И отправляет им письма.
 */

/*ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);*/

define('MODX_API_MODE', true);
require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/index.php';
/** @var modX $modx */
$modx->getService('error', 'error.modError');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);
$modx->setLogTarget('FILE');

/* Отправляем 404-ю если вызвана не через cli версию  */
if (php_sapi_name() != 'cli') {$modx->sendRedirect($modx->makeUrl($modx->getOption('error_page',[])), array('responseCode' => 'HTTP/1.1 404 Not Found'));}

$q = $modx->newQuery('modUser');
$q->where(array('password:=' => ''));
$users = $modx->getIterator('modUser', $q);

if (empty($users)) {return false;}
//var_dump(count($users));exit;
foreach ($users as $user) {
    // Усыпаем на 2 сек. так как у beget ограничения на отправку = 30 писем в минуту.
    sleep(2);

    $profile = $user->getOne('Profile');
    if (!filter_var($profile->get('email'), FILTER_VALIDATE_EMAIL)) {continue;}

    /** @var pdoTools $pdoTools */
    $pdoTools = $modx->getService('pdoTools');
    $body = $pdoTools->getChunk('@FILE chunks/letters/importUser.tpl', array_merge($user->toArray(), $profile->toArray()));
    $emailsender = $modx->getOption('emailsender', [], 'info@webportal35.ru');
    $site_name = $modx->getOption('site_name', []);

    /** @var modPHPMailer $mail */
    $mail = $modx->getService('mail', 'mail.modPHPMailer');
    $mail->set(modMail::MAIL_BODY, $body);
    $mail->set(modMail::MAIL_FROM, $emailsender);
    $mail->set(modMail::MAIL_FROM_NAME, $site_name);
    $mail->set(modMail::MAIL_SUBJECT, 'Требуется восстановление пароля на сайте Джуниор');
    $mail->address('to', $profile->get('email'));
    $mail->address('reply-to', $emailsender);
    $mail->setHTML(true);
    if (!$mail->send()) {
        $this->xpdo->log(xPDO::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: '.$mail->mailer->ErrorInfo);
        $mail->reset();
    }
    else {
        $mail->reset();
    }
}