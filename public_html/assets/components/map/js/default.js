var Map = {
    sel: {
        addresses: '.map_addresses',
        address: '.map_address',
        addressField: 'input[name="address"]',
        addressChoose: 'input[name="address-choose"]'
    },
    setup: function () {
        this.myMap = {};
        this.zoom = 16;
        this.center = '';
        this.pin = '/assets/layout/app/img/yandex-map-pin.png';
    },
    init: function () {
        this.setup();
        this.load();
    },
    load: function () {
        ymaps.ready(Map.initMap);

        $(Map.sel.addressChoose).on('change', function () {
            var coords = $(this).data('coords');
            coords = coords.split(',').map(function (item) {
                return parseFloat(item);
            });
            Map.setCenter(coords);
        });
    },

    initMap: function () {
        Map.collection = new ymaps.GeoObjectCollection();
        Map.myMap = new ymaps.Map("ya-map-1", {
            center: [59.220496, 39.891523],
            zoom: Map.zoom
        });
        $(Map.sel.addressField).each(function () {
            var address = $(this).val();
            var $checkbox = $(this).closest(Map.sel.address).find(Map.sel.addressChoose);
            ymaps.geocode(address, {provider: 'yandex#map', results: 3}).then(function (res) {
                var coords = res.geoObjects.get(0).geometry.getCoordinates();
                $checkbox.attr('data-coords', coords);
                Map.collection.add(new ymaps.Placemark(coords,{
                    balloonContent: '<p>' + address + '</p>'
                },Map.getPlacemark()));
                if ($checkbox.prop('checked')) {
                    Map.center = coords;
                    Map.setCenter(coords);
                }
            });
        });
        Map.myMap.geoObjects.add(Map.collection);
    },
    setCenter: function (coords) {
        Map.myMap.panTo(coords);
    },
    getPlacemark: function () {
        return {
            iconLayout: 'default#image',
            iconImageHref: Map.pin,
            iconImageSize: [41, 52],
            iconImageOffset: [-20, -50]
        }
    }
};

$(document).ready(function () {
    Map.init();
});