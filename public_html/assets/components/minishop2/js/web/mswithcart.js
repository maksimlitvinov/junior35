(function (window, document, $, miniShop2Config) {
    var miniShop2 = miniShop2 || {};
    miniShop2Config.callbacksObjectTemplate = function () {
        return {
            // return false to prevent send data
            before: [],
            response: {
                success: [],
                error: []
            },
            ajax: {
                done: [],
                fail: [],
                always: []
            }
        }
    };
    miniShop2.Callbacks = miniShop2Config.Callbacks = {
        Cart: {
            add: miniShop2Config.callbacksObjectTemplate(),
            remove: miniShop2Config.callbacksObjectTemplate(),
            change: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            status: miniShop2Config.callbacksObjectTemplate()
        },
        Order: {
            add: miniShop2Config.callbacksObjectTemplate(),
            getcost: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            submit: miniShop2Config.callbacksObjectTemplate(),
            getrequired: miniShop2Config.callbacksObjectTemplate(),
            addproduct: miniShop2Config.callbacksObjectTemplate(),
            updateproduct: miniShop2Config.callbacksObjectTemplate(),
            addrekvez: miniShop2Config.callbacksObjectTemplate()
        },
        Product: {
            getcost: miniShop2Config.callbacksObjectTemplate()
        }
    };
    miniShop2.Callbacks.add = function (path, name, func) {
        if (typeof func != 'function') {
            return false;
        }
        path = path.split('.');
        var obj = miniShop2.Callbacks;
        for (var i = 0; i < path.length; i++) {
            if (obj[path[i]] == undefined) {
                return false;
            }
            obj = obj[path[i]];
        }
        if (typeof obj != 'object') {
            obj = [obj];
        }
        if (name != undefined) {
            obj[name] = func;
        }
        else {
            obj.push(func);
        }
        return true;
    };
    miniShop2.Callbacks.remove = function (path, name) {
        path = path.split('.');
        var obj = miniShop2.Callbacks;
        for (var i = 0; i < path.length; i++) {
            if (obj[path[i]] == undefined) {
                return false;
            }
            obj = obj[path[i]];
        }
        if (obj[name] != undefined) {
            delete obj[name];
            return true;
        }
        return false;
    };
    miniShop2.ajaxProgress = false;
    miniShop2.setup = function () {
        // selectors & $objects
        this.actionName = 'ms2_action';
        this.action = ':submit[name=' + this.actionName + ']';
        this.form = '.ms2_form';
        this.$doc = $(document);

        this.sendData = {
            $form: null,
            action: null,
            formData: null
        };

        this.timeout = 300;
    };
    miniShop2.initialize = function () {
        miniShop2.setup();
        // Indicator of active ajax request

        //noinspection JSUnresolvedFunction
        miniShop2.$doc
            .ajaxStart(function () {
                miniShop2.ajaxProgress = true;
            })
            .ajaxStop(function () {
                miniShop2.ajaxProgress = false;
            })
            .on('submit', miniShop2.form, function (e) {
                e.preventDefault();
                var $form = $(this);
                var $el_action = $form.find(miniShop2.action);
                if ($form.find(miniShop2.action).length > 1) {
                    var el_action_number = $form.find(miniShop2.action).length - 1;
                    $el_action = $($form.find(miniShop2.action)[el_action_number]);
                }
                console.log($el_action);
                var action = $el_action.val();

                if (action) {
                    var formData = $form.serializeArray();
                    formData.push({
                        name: miniShop2.actionName,
                        value: action
                    });
                    miniShop2.sendData = {
                        $form: $form,
                        action: action,
                        formData: formData
                    };
                    miniShop2.controller();
                }
            });
        miniShop2.Cart.initialize();
        miniShop2.Product.initialize();
        miniShop2.Message.initialize();
        miniShop2.Order.initialize();
        miniShop2.Gallery.initialize();
    };
    miniShop2.controller = function () {
        var self = this;
        switch (self.sendData.action) {
            case 'cart/add':
                miniShop2.Cart.add();
                break;
            case 'cart/remove':
                miniShop2.Cart.remove();
                break;
            case 'cart/change':
                miniShop2.Cart.change();
                break;
            case 'cart/clean':
                miniShop2.Cart.clean();
                break;
            case 'cart/status':
                miniShop2.Cart.getStatus();
                break;
            case 'order/submit':
                miniShop2.Order.submit();
                break;
            case 'order/clean':
                miniShop2.Order.clean();
                break;
            case 'order/addProduct':
                miniShop2.Order.addProduct();
                break;
            case 'order/updateProduct':
                miniShop2.Order.updateProduct();
                break;
            case 'order/addRekvez':
                miniShop2.Order.addRekvez();
                break;
            default:
                return;
        }
    };
    miniShop2.send = function (data, callbacks, userCallbacks) {
        var runCallback = function (callback, bind) {
            if (typeof callback == 'function') {
                return callback.apply(bind, Array.prototype.slice.call(arguments, 2));
            }
            else if (typeof callback == 'object') {
                for (var i in callback) {
                    if (callback.hasOwnProperty(i)) {
                        var response = callback[i].apply(bind, Array.prototype.slice.call(arguments, 2));
                        if (response === false) {
                            return false;
                        }
                    }
                }
            }
            return true;
        };
        // set context
        if ($.isArray(data)) {
            data.push({
                name: 'ctx',
                value: miniShop2Config.ctx
            });
        }
        else if ($.isPlainObject(data)) {
            data.ctx = miniShop2Config.ctx;
        }
        else if (typeof data == 'string') {
            data += '&ctx=' + miniShop2Config.ctx;
        }

        // set action url
        var formActionUrl = (miniShop2.sendData.$form)
            ? miniShop2.sendData.$form.attr('action')
            : false;
        var url = (formActionUrl)
            ? formActionUrl
            : (miniShop2Config.actionUrl)
                      ? miniShop2Config.actionUrl
                      : document.location.href;
        // set request method
        var formMethod = (miniShop2.sendData.$form)
            ? miniShop2.sendData.$form.attr('method')
            : false;
        var method = (formMethod)
            ? formMethod
            : 'post';

        // callback before
        if (runCallback(callbacks.before) === false || runCallback(userCallbacks.before) === false) {
            return;
        }
        // send
        var xhr = function (callbacks, userCallbacks) {
            return $[method](url, data, function (response, textStatus, jqXHR) {
                if (response.success) {
                    if (response.message) {
                        miniShop2.Message.success(response.message);
                    }
                    runCallback(callbacks.response.success, miniShop2, response);
                    runCallback(userCallbacks.response.success, miniShop2, response);
                }
                else {
                    miniShop2.Message.error(response.message);
                    runCallback(callbacks.response.error, miniShop2, response);
                    runCallback(userCallbacks.response.error, miniShop2, response);
                }
            }, 'json').done(function () {
                runCallback(callbacks.ajax.done, miniShop2, xhr);
                runCallback(userCallbacks.ajax.done, miniShop2, xhr);
            }).fail(function () {
                runCallback(callbacks.ajax.fail, miniShop2, xhr);
                runCallback(userCallbacks.ajax.fail, miniShop2, xhr);
            }).always(function () {
                runCallback(callbacks.ajax.always, miniShop2, xhr);
                runCallback(userCallbacks.ajax.always, miniShop2, xhr);
            });
        }(callbacks, userCallbacks);
    };

    miniShop2.Cart = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            remove: miniShop2Config.callbacksObjectTemplate(),
            change: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            status: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Cart.cart = '#msCart';
            miniShop2.Cart.miniCart = '#msMiniCart';
            miniShop2.Cart.miniCartNotEmptyClass = 'full';
            miniShop2.Cart.countInput = 'input[name=count]';
            miniShop2.Cart.totalWeight = '.ms2_total_weight';
            miniShop2.Cart.totalCount = '.ms2_total_count';
            miniShop2.Cart.totalCost = '.ms2_total_cost';
            miniShop2.Cart.typeTotalCost = '.ms2_type_total_cost';
            miniShop2.Cart.typeTotalCount = '.ms2_type_total_count';
            miniShop2.Cart.type = '#msOrder input[name="cart_type"]';
        },
        initialize: function () {
            miniShop2.Cart.setup();
            if (!$(miniShop2.Cart.cart).length) {
                return;
            }
            miniShop2.$doc.on('change', miniShop2.Cart.cart + ' ' + miniShop2.Cart.countInput, function () {
                if (!!$(this).val()) {
                    $(this).closest(miniShop2.form).submit();
                }
            });
        },
        add: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.add.response.success = function (response) {
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.add, miniShop2.Callbacks.Cart.add);
        },
        remove: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.remove.response.success = function (response) {
                var statusType = miniShop2.Utils.getValueFromSerializedArray('type');
                var totalCost = miniShop2.Utils.formatPrice(response.data[statusType].total_cost);
                $(miniShop2.Cart.typeTotalCount).text(response.data[statusType].total_count);
                $(miniShop2.Cart.typeTotalCost).text(totalCost);
                this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.remove, miniShop2.Callbacks.Cart.remove);
        },
        change: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.change.response.success = function (response) {
                if (typeof(response.data.key) == 'undefined') {
                    this.Cart.remove_position(miniShop2.Utils.getValueFromSerializedArray('key'));
                }
                else {
                    $('#' + miniShop2.Utils.getValueFromSerializedArray('key')).find('');
                }
                if ($(miniShop2.Cart.cart).length > 0) {
                    var $row = $('#' + response.data.key);
                    var statusType = $row.find(miniShop2.Product.type).val();
                    var totalCost = miniShop2.Utils.formatPrice(response.data[statusType].total_cost);
                    $(miniShop2.Cart.typeTotalCount).text(response.data[statusType].total_count);
                    $(miniShop2.Cart.typeTotalCost).text(totalCost);
                    if (response.data[statusType].allow_order) {
                        miniShop2.Order.show();
                    }
                    else {
                        miniShop2.Order.hide();
                    }
                    miniShop2.Product.getCost(response.data.key);
                }
                this.Cart.status(response.data);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.change, miniShop2.Callbacks.Cart.change);
        },
        status: function (status) {
            if (status['total_count'] < 1) {
                location.reload();
            }
            else {
                //var $cart = $(miniShop2.Cart.cart);
                var $miniCart = $(miniShop2.Cart.miniCart);
                if (status['total_count'] > 0 && !$miniCart.hasClass(miniShop2.Cart.miniCartNotEmptyClass)) {
                    $miniCart.addClass(miniShop2.Cart.miniCartNotEmptyClass);
                }
                $(miniShop2.Cart.totalWeight).text(miniShop2.Utils.formatWeight(status['total_weight']));
                $(miniShop2.Cart.totalCount).text(status['total_count']);
                $(miniShop2.Cart.totalCost).text(miniShop2.Utils.formatPrice(status['total_cost']));
                if ($(miniShop2.Order.orderCost, miniShop2.Order.order).length) {
                    miniShop2.Order.getcost();
                }
            }
        },
        clean: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.clean.response.success = function (response) {
                this.Cart.status(response.data);
            };

            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.clean, miniShop2.Callbacks.Cart.clean);
        },
        remove_position: function (key) {
            $('#' + key).remove();
        },
        getStatus: function () {
            var callbacks = miniShop2.Cart.callbacks;
            callbacks.status.response.success = function (response) {
                var type = $(miniShop2.Cart.type).val();
                if (response.data[type].allow_order) {
                    miniShop2.Order.show();
                }
                else {
                    miniShop2.Order.hide();
                }
            };
            miniShop2.sendData.action = 'cart/status';
            miniShop2.sendData.formData = [{
                name: miniShop2.actionName,
                value: 'cart/status'
            }];
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Cart.callbacks.status, miniShop2.Callbacks.Cart.status);
        }
    };

    miniShop2.Product = {
        callbacks: {
            getcost: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Product.form = '.ms2_form';
            miniShop2.Product.order = '.product__order';
            miniShop2.Product.type = 'input[name="type"]';
            miniShop2.Product.max_count = 'input[name="max_count"]';
            miniShop2.Product.count = 'input[name="count"]';
            miniShop2.Product.countOrder = 'input[name="count__order"]';
            miniShop2.Product.plus = '.ms2_form .plus';
            miniShop2.Product.minus = '.ms2_form .minus';
            miniShop2.Product.plusOrder = '.ms2_form .plus__order';
            miniShop2.Product.minusOrder = '.ms2_form .minus__order';
            miniShop2.Product.product = '#msProduct';
            miniShop2.Product.option_size = 'input[name="options[size]"]';
            miniShop2.Product.inform = '.ms2_form .inform';
            miniShop2.Product.btn = '*[name="ms2_action"]';
            miniShop2.Product.total_cost = '.ms2_prod_total_cost';
        },
        initialize: function () {
            miniShop2.Product.setup();

            // Количество в корзину
            miniShop2.$doc
                .on('click', miniShop2.Product.plus, function (e) {
                e.preventDefault();
                var $form = $(this).closest(miniShop2.Product.form);
                var $field_count = $form.find(miniShop2.Product.count);
                var $field_max_count = $form.find(miniShop2.Product.max_count);
                var count = parseInt($field_count.val());
                var max_count = parseInt($field_max_count.val());
                if (count >= max_count) {
                    miniShop2.Message.error('Указано максимальное количество.');
                    return false;
                }
                $field_count.val(count + 1);
                if ($(miniShop2.Cart.cart).length > 0) {
                    $form.find(miniShop2.Product.btn).trigger('click');
                }
            })
                .on('click', miniShop2.Product.minus, function (e) {
                e.preventDefault();
                var $form = $(this).closest(miniShop2.Product.form);
                var $field_count = $form.find(miniShop2.Product.count);
                var count = parseInt($field_count.val());
                if (count > 1) {
                    $field_count.val(count - 1);
                    if ($(miniShop2.Cart.cart).length > 0) {
                        $form.find(miniShop2.Product.btn).trigger('click');
                    }
                }
            })
                .on('change', miniShop2.Product.count, function () {
                var $form = $(this).closest(miniShop2.Product.form);
                var count = parseInt($(this).val());
                var $field_max_count = $form.find(miniShop2.Product.max_count);
                var max_count = parseInt($field_max_count.val());
                if (count >= max_count) {$(this).val(max_count);}
                else if (count <= 0) {$(this).val(1);}
            });

            // Количество в заказ
            miniShop2.$doc
                .on('click', miniShop2.Product.plusOrder, function(e) {
                    e.preventDefault();
                    var $form = $(this).closest(miniShop2.Product.order);
                    var $field_max_count = $form.find(miniShop2.Product.max_count);
                    var $field_count = $form.find(miniShop2.Product.countOrder);
                    var count = parseInt($field_count.val());
                    var max_count = parseInt($field_max_count.val());
                    if (count >= max_count) {
                        miniShop2.Message.error('Указано максимальное количество.');
                        return false;
                    }
                    $field_count.val(count + 1);
                    if (miniShop2.Order.isProfileOrder()) {
                        miniShop2.Order.updateProduct($form);
                    }
                })
                .on('click', miniShop2.Product.minusOrder, function (e) {
                    e.preventDefault();
                    var $form = $(this).closest(miniShop2.Product.order);
                    var $field_count = $form.find(miniShop2.Product.countOrder);
                    var count = parseInt($field_count.val());
                    if (count > 1) {
                        $field_count.val(count - 1);
                        if (miniShop2.Order.isProfileOrder()) {
                            miniShop2.Order.updateProduct($form);
                        }
                    }
                })
                .on('change', miniShop2.Product.countOrder, function () {
                    var $form = $(this).closest(miniShop2.Product.order);
                    var $field_max_count = $form.find(miniShop2.Product.max_count);
                    var max_count = parseInt($field_max_count.val());
                    var count = parseInt($(this).val());
                    if (count > max_count) {
                        $(this).val(max_count);
                    }
                    else if (count <= 0) {
                        $(this).val(1);
                    }
                    if (miniShop2.Order.isProfileOrder()) {
                        miniShop2.Order.updateProduct($form);
                    }
                });

            if (miniShop2.Product.is_product_page()) {
                // Сообщить при поступлении
                miniShop2.$doc.on('click', miniShop2.Product.inform, function (e) {
                    e.preventDefault();
                    // TODO Выводить окно для подписки
                });
                // Выбор размера
                miniShop2.$doc.on('change', miniShop2.Product.form + ' ' + miniShop2.Product.option_size, function () {
                    $(miniShop2.Product.count).val(1);
                    var $field_type = $(this).closest(miniShop2.Product.form).find(miniShop2.Product.type);
                    var $field_max_count = $(this).closest(miniShop2.Product.form).find(miniShop2.Product.max_count);
                    var type = $(this).data('type');
                    $field_type.val(type);
                    $field_max_count.val($(this).data('count'));
                    $(miniShop2.Product.order).each(function() {
                        var bloc_type = $(this).data('type');
                        if (block_type == type) {
                            if($(this).hasClass('mfp-hide')) {
                                $(this).removeClass('mfp-hide');
                            }
                        }
                        else {
                            if(!$(this).hasClass('mfp-hide')) {
                                $(this).addClass('mfp-hide');
                            }
                        }
                    });
                });
            }
        },
        is_product_page: function () {
            return $(miniShop2.Product.product).length > 0;
        },
        getCost: function (key) {
            var callbacks = miniShop2.Product.callbacks;
            callbacks.getcost.response.success = function (response) {
                var product = response.data;
                var cost = miniShop2.Utils.formatPrice(product.cost);
                $('#' + product.key).find(miniShop2.Product.total_cost).text(cost);
            };
            miniShop2.sendData.action = 'product/getcost';
            miniShop2.sendData.formData = [
                {name: miniShop2.actionName, value: 'product/getcost'},
                {name: 'key', value: key},
                {name: 'type', value: $(miniShop2.Cart.type).val()}
            ];
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Product.callbacks.getcost, miniShop2.Callbacks.Product.getcost);
        }
    };

    miniShop2.Gallery = {
        setup: function () {
            miniShop2.Gallery.gallery = $('#msGallery');
            miniShop2.Gallery.files = miniShop2.Gallery.gallery.find('.fotorama');
        },
        initialize: function () {
            miniShop2.Gallery.setup();
            if (miniShop2.Gallery.files.length) {
                $('<link/>', {
                    rel: 'stylesheet',
                    type: 'text/css',
                    href: miniShop2Config.cssUrl + 'lib/fotorama.min.css',
                }).appendTo('head');
                $('<script/>', {
                    type: 'text/javascript',
                    src: miniShop2Config.jsUrl + 'lib/fotorama.min.js',
                }).appendTo('head');
            }

            // fix size gallery
            miniShop2.$doc.on('fotorama:ready', miniShop2.Gallery.files, function (e, Fotorama, extra) {
                if ((src = Fotorama.activeFrame.src)) {
                    measure = $.Fotorama.measures[src];
                    if (measure === undefined) {
                        for (i in $.Fotorama.measures) {
                            measure = $.Fotorama.measures[i];
                            break;
                        }
                        Fotorama.resize(measure);
                    }
                }
            });

        }
    };

    miniShop2.Order = {
        callbacks: {
            add: miniShop2Config.callbacksObjectTemplate(),
            getcost: miniShop2Config.callbacksObjectTemplate(),
            clean: miniShop2Config.callbacksObjectTemplate(),
            submit: miniShop2Config.callbacksObjectTemplate(),
            getrequired: miniShop2Config.callbacksObjectTemplate(),
            addproduct: miniShop2Config.callbacksObjectTemplate(),
            updateproduct: miniShop2Config.callbacksObjectTemplate(),
            addrekvez: miniShop2Config.callbacksObjectTemplate()
        },
        setup: function () {
            miniShop2.Order.order = '#msOrder';
            miniShop2.Order.deliveries = '#deliveries';
            miniShop2.Order.payments = '#payments';
            miniShop2.Order.deliveryInput = 'input[name="delivery"]';
            miniShop2.Order.inputParent = '.input-parent';
            miniShop2.Order.paymentInput = 'input[name="payment"]';
            miniShop2.Order.paymentInputUniquePrefix = 'input#payment_';
            miniShop2.Order.deliveryInputUniquePrefix = 'input#delivery_';
            miniShop2.Order.orderCost = '#ms2_order_cost';
            miniShop2.Order.not_allowed = '.ms2_oder_not_allowed';
            miniShop2.Order.allowed = '.ms2_oder_allowed';
            miniShop2.Order.btnChangeUserFields = '.change_user_fields';
            miniShop2.Order.userFields = '.user_fields';
            miniShop2.Order.productBtn = '.to-order';
            miniShop2.Order.id = '*[name="order"]';
            miniShop2.Order.productCount = '.order-product-count';
            miniShop2.Order.row = '.order__row';
            miniShop2.Order.productCost = '.order__product-cost';
            miniShop2.Order.productsCost = '.order-product-cost';
            miniShop2.Order.cityField = 'input[name="city"]';
            miniShop2.Order.deliveryField = 'select[name="delivery"]';
            miniShop2.Order.rekvezBtn = '.to-order-rekvez';
        },
        initialize: function () {
            miniShop2.Order.setup();
            if ($(miniShop2.Order.order).length) {
                miniShop2.$doc
                    .on('click', miniShop2.Order.order + ' [name="' + miniShop2.actionName + '"][value="order/clean"]', function (e) {
                        miniShop2.Order.clean();
                        e.preventDefault();
                    })
                    .on('change', miniShop2.Order.order + ' input,' + miniShop2.Order.order + ' textarea', function () {
                        var $this = $(this);
                        var key = $this.attr('name');
                        var value = $this.val();
                        miniShop2.Order.add(key, value);
                    })
                    .on('change', miniShop2.Order.deliveryField, function () {
                        if ($(this).val() == 1) {
                            $('.choise-point').removeClass('mfp-hide');
                            var city = $(miniShop2.Order.cityField).val();
                            if (city.length > 0) {
                                Cdek.defaultCity = city;
                            }
                            Cdek.cdekInit();
                        }
                        else {
                            $('.choise-point').addClass('mfp-hide');
                        }
                    })
                    .on('click', miniShop2.Order.btnChangeUserFields, function (e) {
                        e.preventDefault();
                        if ($(this).hasClass('active')) {return false;}
                        $(miniShop2.Order.userFields).find('input[type="hidden"]').each(function () {
                            $(this).attr('type', 'text');
                            $(this).prev().hide();
                        });
                        $(this).addClass('active');
                    });
                if ($(miniShop2.Order.deliveryField).val() == 1) {
                    var city = $(miniShop2.Order.cityField).val();
                    if (city.length > 0) {
                        Cdek.defaultCity = city;
                    }
                    Cdek.cdekInit();
                }
                var $deliveryInputChecked = $(miniShop2.Order.deliveryInput + ':checked', miniShop2.Order.order);
                $deliveryInputChecked.trigger('change');

                miniShop2.Cart.getStatus()
            }
            miniShop2.$doc
                .on('click', miniShop2.Order.productBtn, function (e) {
                e.preventDefault();
                var action = $(this).val();
                var actionName = miniShop2.actionName;
                var $block = $(this).closest(miniShop2.Product.order);
                var $form = $(this).closest(miniShop2.Product.form);
                miniShop2.sendData.action = $(this).val();
                miniShop2.sendData.formData = [
                    {'name' : actionName, 'value' : action},
                    {'name' : 'id', 'value' : $form.find('input[name="id"]').val()},
                    {'name' : 'type', 'value' : $form.find(miniShop2.Product.type).val()},
                    {'name' : 'count', 'value' : $block.find(miniShop2.Product.countOrder).val()},
                    {'name' : $block.find(miniShop2.Order.id).attr('name'), 'value' : $block.find(miniShop2.Order.id).val()},
                    {'name' : $form.find(miniShop2.Product.option_size).attr('name'), value : $form.find(miniShop2.Product.option_size + ':checked').val()}
                ];

                if (miniShop2.Order.isProfileOrder()) {
                    miniShop2.Order.updateProduct($form);
                }
                else {
                    miniShop2.Order.addProduct();
                }
            })
                .on('click', miniShop2.Order.rekvezBtn, function (e) {
                    e.preventDefault();
                    var $form = $(this).closest(miniShop2.Product.form);
                    var action = $form.find('input[name="' + miniShop2.actionName + '"]').val();
                    var rekvez = $form.find('textarea[name="rekvez"]').val();

                    miniShop2.sendData.action = action;
                    miniShop2.sendData.formData = [
                        {'name' : miniShop2.actionName, 'value' : action},
                        {'name' : $form.find(miniShop2.Order.id).attr('name'), 'value' : $form.find(miniShop2.Order.id).val()},
                        {'name' : 'rekvez', 'value' : rekvez},
                    ];

                    miniShop2.controller();
                });
        },
        updatePayments: function (payments) {
            var $paymentInputs = $(miniShop2.Order.paymentInput, miniShop2.Order.order);
            $paymentInputs.attr('disabled', true).prop('disabled', true).closest(miniShop2.Order.inputParent).hide();
            if (payments.length > 0) {
                for (var i in payments) {
                    if (payments.hasOwnProperty(i)) {
                        $paymentInputs.filter(miniShop2.Order.paymentInputUniquePrefix + payments[i]).attr('disabled', false).prop('disabled', false).closest(miniShop2.Order.inputParent).show();
                    }
                }
            }
            if ($paymentInputs.filter(':visible:checked').length == 0) {
                $paymentInputs.filter(':visible:first').trigger('click');
            }
        },
        add: function (key, value) {
            var callbacks = miniShop2.Order.callbacks;
            var old_value = value;
            callbacks.add.response.success = function (response) {
                (function (key, value, old_value) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    switch (key) {
                        case 'delivery':
                            $field = $(miniShop2.Order.deliveryInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            }
                            else {
                                miniShop2.Order.getrequired(value);
                                miniShop2.Order.updatePayments($field.data('payments'));
                                miniShop2.Order.getcost();
                            }
                            break;
                        case 'payment':
                            $field = $(miniShop2.Order.paymentInputUniquePrefix + response.data[key]);
                            if (response.data[key] != old_value) {
                                $field.trigger('click');
                            }
                            else {
                                miniShop2.Order.getcost();
                            }
                            break;
                        //default:
                    }
                    $field.val(response.data[key]).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                })(key, value, old_value);
            };
            callbacks.add.response.error = function () {
                (function (key) {
                    var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                    if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                        $field.closest(miniShop2.Order.inputParent).addClass('error');
                    }
                    else {
                        $field.addClass('error');
                    }
                })(key);
            };

            var data = {
                key: key,
                value: value
            };
            data[miniShop2.actionName] = 'order/add';
            miniShop2.send(data, miniShop2.Order.callbacks.add, miniShop2.Callbacks.Order.add);
        },
        getcost: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getcost.response.success = function (response) {
                $(miniShop2.Order.orderCost, miniShop2.Order.order).text(miniShop2.Utils.formatPrice(response.data['cost']));
            };
            var data = {};
            data[miniShop2.actionName] = 'order/getcost';
            miniShop2.send(data, miniShop2.Order.callbacks.getcost, miniShop2.Callbacks.Order.getcost);
        },
        clean: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.clean.response.success = function () {
                location.reload();
            };

            var data = {};
            data[miniShop2.actionName] = 'order/clean';
            miniShop2.send(data, miniShop2.Order.callbacks.clean, miniShop2.Callbacks.Order.clean);
        },
        submit: function () {
            miniShop2.Message.close();

            // Checking for active ajax request
            if (miniShop2.ajaxProgress) {
                //noinspection JSUnresolvedFunction
                miniShop2.$doc.ajaxComplete(function () {
                    miniShop2.ajaxProgress = false;
                    miniShop2.$doc.unbind('ajaxComplete');
                    miniShop2.Order.submit();
                });
                return false;
            }

            var callbacks = miniShop2.Order.callbacks;
            callbacks.submit.before = function () {
                $(':button, a', miniShop2.Order.order).attr('disabled', true).prop('disabled', true);
            };
            callbacks.submit.response.success = function (response) {
                if (response.data['redirect']) {
                    document.location.href = response.data['redirect'];
                }
                else if (response.data['msorder']) {
                    document.location.href = document.location.origin + document.location.pathname
                        + (document.location.search ? document.location.search + '&' : '?')
                        + 'msorder=' + response.data['msorder'];
                }
                else {
                    location.reload();
                }
            };
            callbacks.submit.response.error = function (response) {
                setTimeout((function () {
                    $(':button, a', miniShop2.Order.order).attr('disabled', false).prop('disabled', false);
                }.bind(this)),3 * miniShop2.timeout);
                $('[name]', miniShop2.Order.order).removeClass('error').closest(miniShop2.Order.inputParent).removeClass('error');
                for (var i in response.data) {
                    if (response.data.hasOwnProperty(i)) {
                        var key = response.data[i];
                        //var $field = $('[name="' + response.data[i] + '"]', miniShop2.Order.order);
                        //$field.addClass('error').closest(miniShop2.Order.inputParent).addClass('error');
                        var $field = $('[name="' + key + '"]', miniShop2.Order.order);
                        if ($field.attr('type') == 'checkbox' || $field.attr('type') == 'radio') {
                            $field.closest(miniShop2.Order.inputParent).addClass('error');
                        }
                        else {
                            $field.addClass('error');
                        }
                    }
                }
            };
            return miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.submit, miniShop2.Callbacks.Order.submit);
        },
        getrequired: function (value) {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.getrequired.response.success = function (response) {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
                var requires = response.data['requires'];
                for (var i = 0, length = requires.length; i < length; i++) {
                    $('[name=' + requires[i] + ']', miniShop2.Order.order).addClass('required').closest(miniShop2.Order.inputParent).addClass('required');
                }
            };
            callbacks.getrequired.response.error = function () {
                $('[name]', miniShop2.Order.order).removeClass('required').closest(miniShop2.Order.inputParent).removeClass('required');
            };

            var data = {
                id: value
            };
            data[miniShop2.actionName] = 'order/getrequired';
            miniShop2.send(data, miniShop2.Order.callbacks.getrequired, miniShop2.Callbacks.Order.getrequired);
        },
        addProduct: function () {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.addproduct.before = function () {
                $(miniShop2.Order.productBtn).attr('disabled', true).prop('disabled', true);
            };
            callbacks.addproduct.response.success = function (response) {
                var $form = $(miniShop2.Product.form);
                $form.find(miniShop2.Product.max_count).each(function () {
                    $(this).val(response.data.max_count);
                });
                $form.find(miniShop2.Product.option_size + ':checked').closest('tr').find('.product__size-count').text(response.data.max_count);
            };
            callbacks.addproduct.response.error = function (response) {
                console.log('Error', response);
            };
            callbacks.addproduct.ajax.always = function () {
                $(miniShop2.Order.productBtn).attr('disabled', false).prop('disabled', false);
            };
            miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.addproduct, miniShop2.Callbacks.Order.addproduct);
        },
        updateProduct: function (form) {
            var $form = $(form);
            var action = $form.find('input[name="' + miniShop2.actionName + '"]').val();

            miniShop2.sendData.action = action;
            miniShop2.sendData.formData = [
                {'name' : miniShop2.actionName, 'value' : action},
                {'name' : 'product', 'value' : $form.find('input[name="product"]').val()},
                {'name' : 'count', 'value' : $form.find(miniShop2.Product.countOrder).val()}
            ];

            var callbacks = miniShop2.Order.callbacks;
            callbacks.updateproduct.response.success = function (response) {
                $('input[name="product"]').each(function () {
                    if ($(this).val() == response.data.product.id) {
                        var $item = $(this).closest(miniShop2.Order.row);

                        $item.find(miniShop2.Product.max_count).val(response.data.max_count);
                        $item.find(miniShop2.Order.productCost).text(response.data.product.cost);
                        $(miniShop2.Order.productCount).text(response.data.order.count);
                        $(miniShop2.Order.productsCost).text(response.data.order.cost);
                    }
                });
                //console.log('Success', response);
            };
            callbacks.updateproduct.response.error = function (response) {
                $('input[name="product"]').each(function () {
                    if ($(this).val() == response.data.product.id) {
                        $(this).closest(miniShop2.Product.form).find(miniShop2.Product.countOrder).val(response.data.product.count);
                    }
                });
            };

            miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.updateproduct, miniShop2.Callbacks.Order.updateproduct);
        },
        addRekvez: function (form) {
            var callbacks = miniShop2.Order.callbacks;
            callbacks.addrekvez.response.success = function (response) {};
            callbacks.addrekvez.response.error = function (response) {console.error(response);};

            miniShop2.send(miniShop2.sendData.formData, miniShop2.Order.callbacks.addrekvez, miniShop2.Callbacks.Order.addrekvez);
        },
        isProfileOrder: function () {
            return $(miniShop2.Product.form + miniShop2.Product.order).length > 0;
        },
        show: function () {
            $(miniShop2.Order.not_allowed).hide();
            $(miniShop2.Order.allowed).show();
        },
        hide: function () {
            $(miniShop2.Order.allowed).hide();
            $(miniShop2.Order.not_allowed).show();
        }
    };

    miniShop2.Message = {
        initialize: function () {
            miniShop2.Message.close = function () {
            };
            miniShop2.Message.show = function (message) {
                if (message != '') {
                    alert(message);
                }
            };

            if (typeof($.fn.jGrowl) != 'function') {
                $.getScript(miniShop2Config.jsUrl + 'lib/jquery.jgrowl.min.js', function () {
                    miniShop2.Message.initialize();
                });
            }
            else {
                $.jGrowl.defaults.closerTemplate = '<div>[ ' + miniShop2Config.close_all_message + ' ]</div>';
                miniShop2.Message.close = function () {
                    $.jGrowl('close');
                };
                miniShop2.Message.show = function (message, options) {
                    if (message != '') {
                        $.jGrowl(message, options);
                    }
                }
            }
        },
        success: function (message) {
            miniShop2.Message.show(message, {
                theme: 'ms2-message-success',
                sticky: false
            });
        },
        error: function (message) {
            miniShop2.Message.show(message, {
                theme: 'ms2-message-error',
                sticky: false
            });
        },
        info: function (message) {
            miniShop2.Message.show(message, {
                theme: 'ms2-message-info',
                sticky: false
            });
        }
    };

    miniShop2.Utils = {
        empty: function (val) {
            return (typeof(val) == 'undefined' || val == 0 || val === null || val === false || (typeof(val) == 'string' && val.replace(/\s+/g, '') == '') || (typeof(val) == 'object' && val.length == 0));
        },
        formatPrice: function (price) {
            var pf = miniShop2Config.price_format;
            price = this.number_format(price, pf[0], pf[1], pf[2]);

            if (miniShop2Config.price_format_no_zeros && pf[0] > 0) {
                price = price.replace(/(0+)$/, '');
                price = price.replace(/[^0-9]$/, '');
            }

            return price;
        },
        formatWeight: function (weight) {
            var wf = miniShop2Config.weight_format;
            weight = this.number_format(weight, wf[0], wf[1], wf[2]);

            if (miniShop2Config.weight_format_no_zeros && wf[0] > 0) {
                weight = weight.replace(/(0+)$/, '');
                weight = weight.replace(/[^0-9]$/, '');
            }

            return weight;
        },
        // Format a number with grouped thousands,
        number_format: function (number, decimals, dec_point, thousands_sep) {
            // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
            // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // bugfix by: Michael White (http://crestidg.com)
            var i, j, kw, kd, km;

            // input sanitation & defaults
            if (isNaN(decimals = Math.abs(decimals))) {
                decimals = 2;
            }
            if (dec_point == undefined) {
                dec_point = ',';
            }
            if (thousands_sep == undefined) {
                thousands_sep = '.';
            }

            i = parseInt(number = (+number || 0).toFixed(decimals)) + '';

            if ((j = i.length) > 3) {
                j = j % 3;
            }
            else {
                j = 0;
            }

            km = j
                ? i.substr(0, j) + thousands_sep
                : '';
            kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
            kd = (decimals
                ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, '0').slice(2)
                : '');

            return km + kw + kd;
        },
        getValueFromSerializedArray: function (name, arr) {
            if (!$.isArray(arr)) {
                arr = miniShop2.sendData.formData;
            }
            for (var i = 0, length = arr.length; i < length; i++) {
                if (arr[i].name == name) {
                    return arr[i].value;
                }
            }
            return null;
        },
    };

    $(document).ready(function ($) {
        miniShop2.initialize();
        var html = $('html');
        html.removeClass('no-js');
        if (!html.hasClass('js')) {
            html.addClass('js');
        }
    });

    window.miniShop2 = miniShop2;
})(window, document, jQuery, miniShop2Config);
