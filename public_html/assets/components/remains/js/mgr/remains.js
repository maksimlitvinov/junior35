var remains = function (config) {
    config = config || {};
    remains.superclass.constructor.call(this, config);
};
Ext.extend(remains, Ext.Component, {
    page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, tools: {}
});
Ext.reg('remains', remains);

remains = new remains();