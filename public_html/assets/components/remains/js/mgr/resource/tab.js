
remains.grid.sizeItems = function (config) {
    config = config || {};
    this.sm = new Ext.grid.CheckboxSelectionModel();

    Ext.applyIf(config, {
        id: 'remains-grid-sizes',
        url: remains.config.connector_url,
        baseParams: {
            action: 'mgr/remains/getlist',
            product_id: remains.config.resource
        },
        fields: ['size', 'count', 'actions'],
        autoHeight: true,
        paging: true,
        remoteSort: true,
        sm: this.sm,
        columns: [
            {header: 'Размер', sortable: false, dataIndex: 'size', width: 50},
            {header: 'Количество', sortable: false, dataIndex: 'count', width: 50},
            {header: 'Действия', dataIndex: 'actions',width: 75,renderer: remains.tools.renderActions, id: 'actions'}
        ],
        tbar: [{
            text: '<i class="icon icon-plus"></i> Добавить',
            handler: this.createItem,
            scope: this
        }],
        viewConfig: {
            forceFit: true,
            enableRowBody: true,
            autoFill: true,
            showPreview: true,
            scrollOffset: 0,
            getRowClass: function (rec, ri, p) {
                return '';
            }
        },
        listeners: {
            rowDblClick: function (grid, rowIndex, e) {
                var row = grid.store.getAt(rowIndex);
                this.updateItem(grid, e, row);
            }
        }
    });

    remains.grid.sizeItems.superclass.constructor.call(this, config);
};
Ext.extend(remains.grid.sizeItems, MODx.grid.Grid, {
    window: {},

    getMenu: function(grid, rowIndex) {
        var row = grid.getStore().getAt(rowIndex);
        return remains.tools.getMenu(row.data.actions, this);
    },

    createItem: function (btn, e) {
        if (this.window.createRemains == undefined) {
            this.window.createRemains = MODx.load({
                xtype: 'remains-window-remain-create',
                listeners: {
                    'success': {fn:function() {this.refresh();}, scope:this},
                    'failure': {fn:function() { console.log('hhh'); },scope:this},
                }
            });
        }
        this.window.createRemains.fp.getForm().reset();
        this.window.createRemains.show(e.target);
    },
    
    updateItem: function (grid, e, row) {
        if (typeof(row) != 'undefined') {
            this.menu.record = row;
        }
        else {
            this.menu.record = this.getSelectionModel().getSelections()[0];
        }
        var id = this.menu.record.id;
        MODx.Ajax.request({
            url: remains.config.connector_url,
            params: {
                action: 'mgr/remains/get',
                id: id
            },
            listeners: {
                success: {fn: function(r) {
                    if (this.window.updateRemains != undefined) {
                        this.window.updateRemains.close();
                        this.window.updateRemains.destroy();
                    }
                    this.window.updateRemains = MODx.load({
                        xtype: 'remains-window-remain-update',
                        record: r,
                        listeners: {
                            success: {fn:function() { this.refresh(); },scope:this}
                        }
                    });
                    this.window.updateRemains.fp.getForm().reset();
                    this.window.updateRemains.fp.getForm().setValues(r.object);
                    this.window.updateRemains.show(e.target);
                }, scope: this}
            }
        });

    },

    removeItem: function (grid, e) {
        var ids = this._getSelectedIds();
        if (!ids) {return;}
        remains.tools.onAjax(this.getEl());

        MODx.msg.confirm({
            title: 'Удаление размера',
            text: 'Вы действительно хотите удалить этот размер?',
            url: remains.config.connector_url,
            params: {
                action: 'mgr/remains/remove',
                ids: ids.join(',')
            },
            listeners: {
                success: {fn:function(r,q) {this.refresh();}, scope:this}
            }
        });
    },

    onClick: function(e) {
        var elem = e.getTarget();
        if (elem.nodeName == 'BUTTON') {
            var row = this.getSelectionModel().getSelected();
            if (typeof(row) != 'undefined') {
                var type = elem.getAttribute('type');
                if (type == 'menu') {
                    var ri = this.getStore().find('id', row.id);
                    return this._showMenu(this, ri, e);
                }
                else {
                    this.menu.record = row;
                    return this[type](this, e);
                }
            }
        }
        return this.processEvent('click', e);
    },

    _getSelectedIds: function() {
        var ids = [];
        var selected = this.getSelectionModel().getSelections();
        for (var i in selected) {
            if (!selected.hasOwnProperty(i)) {continue;}
            ids.push(selected[i]['id']);
        }
        return ids;
    },

    refresh: function() {
        this.getStore().reload({
            callback: function(items,b,c){
                var in_stock = Ext.getCmp('in_stock');
                var avalable = Math.floor(in_stock.getValue() / items.length);
                var countInGrids = remains.tools.getCountFromRows(this);

                Ext.getCmp('available_sizes').setValue(countInGrids);
                remains.tools.setMsField('available_sizes', countInGrids);

                Ext.getCmp('count_in_row').setValue(items.length);
                remains.tools.setMsField('count_in_row', items.length);

                Ext.getCmp('available_rows').setValue(avalable);
                remains.tools.setMsField('available_rows', avalable);
            }
        });
    }
});
Ext.reg('remains-grid-size-items', remains.grid.sizeItems);

/*remains.panel.RtopPanel = function (config) {
    config = config || {};

    Ext.applyIf(config, {
        id: 'remains-rpanel-top',
        cls: 'container',
        bodyStyle: '',
        defaults: {collapsible: false, autoHeight: true},
        items: [{
            layout: 'form',
            items: []
        }]
    });

    remains.panel.RtopPanel.superclass.constructor.call(this, config);
};
Ext.extend(remains.panel.RtopPanel, MODx.FormPanel);
Ext.reg('remains-top-panel', remains.panel.RtopPanel);*/

remains.panel.ResourceInject = function (config) {
    config = config || {};

    if (!config.update) {
        config.update = true;
    }

    Ext.apply(config, {
        id: 'remains-panel-resource-inject',
        cls: 'remains-panel-resource-inject',
        bodyCssClass: 'main-wrapper',
        forceLayout: true,
        deferredRender: false,
        autoHeight: true,
        border: false,
        items: this.getMainItems(config)
    });

    remains.panel.ResourceInject.superclass.constructor.call(this, config);
};
Ext.extend(remains.panel.ResourceInject, MODx.Panel, {

    getMainItems: function (config) {
        var remainsData =  [{
            xtype: 'panel',
            id: 'remains-panel',
            layout: 'fit',
            items: [
                {
                    layout: 'column',
                    autoHeight: true,
                    items: [
                        {
                            columnWidth: 1,
                            layout: 'form',
                            style: 'margin-bottom:10px',
                            items: [
                                {
                                    xtype: 'numberfield',
                                    id: 'count_in_row',
                                    fieldLabel: 'Кол-во в ряду',
                                    name: 'count_in_row',
                                    hiddenName: 'count_in_row',
                                    anchor: '100%',
                                    disabled: true,
                                    listeners: {
                                        render: {
                                            fn: function (el) {
                                                //console.log('Load', el);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'numberfield',
                                    id: 'available_rows',
                                    fieldLabel: 'Доступно рядов',
                                    name: 'available_rows',
                                    hiddenName: 'available_rows',
                                    anchor: '100%',
                                    disabled: true,
                                    value: 0
                                },
                                {
                                    xtype: 'numberfield',
                                    id: 'available_sizes',
                                    fieldLabel: 'Количество в размерах',
                                    name: 'available_sizes',
                                    hiddenName: 'available_sizes',
                                    readOnly: true,
                                    anchor: '100%',
                                    disabled: true,
                                    value: 0
                                    /*xtype: 'xcheckbox',
                                    fieldLabel: 'Без рядов',
                                    id: 'without_rows',
                                    name: 'without_rows',
                                    hiddenName: 'without_rows',
                                    anchor: '100%',
                                    listeners: {
                                        'check': {
                                            fn: function (cb, checked) {
                                                var parent = cb.ownerCt;
                                                if (checked) {
                                                    parent.get('available_rows').setValue(0);
                                                }
                                                else {
                                                    this.refreshMsFields(parent.get('in_stock').getValue());
                                                }
                                                remains.tools.setMsField(cb.getName(), checked ? 1 : 0);
                                            },
                                            scope:this
                                        }
                                    }*/
                                },
                                {
                                    xtype: 'numberfield',
                                    fieldLabel: 'В наличии для рядов',
                                    id: 'in_stock',
                                    name: 'in_stock',
                                    hiddenName: 'in_stock',
                                    anchor: '100%',
                                    listeners: {
                                        'change': {
                                            fn: function (el, val, old) {
                                                remains.tools.setMsField(el.name, val);
                                                this.refreshMsFields(val);
                                            },
                                            scope: this
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'remains-grid-size-items', 
                    resource: remains.config.resource,
                    id: 'remains-grid-sizes'
                }
            ]
        }];
        return remainsData;
    },

    refreshMsFields: function (val) {
        var countItems = Ext.getCmp('remains-grid-sizes').getStore().getTotalCount();
        var available = Math.floor(val / countItems);
        Ext.getCmp('available_rows').setValue(available);
        remains.tools.setMsField('available_rows', available);
    }
});
Ext.reg('remains-panel-resource-inject', remains.panel.ResourceInject);

Ext.override(miniShop2.panel.Product, {

    remainsOriginals: {
        getFields: miniShop2.panel.Product.prototype.getFields,
    },

    getFields: function (config) {
        var fields = this.remainsOriginals.getFields.call(this, config);

        for (var i in fields) {
            if (!fields.hasOwnProperty(i)) {
                continue;
            }
            var item = fields[i];
            if (item.id == "modx-resource-tabs") {
                for (var i2 in item.items) {
                    if (!item.items.hasOwnProperty(i2)) {
                        continue;
                    }
                    var tab = item.items[i2];
                    if (tab.id == "minishop2-product-tab" && tab.items[0]) {
                        tab.items[0].items.push({
                            title: 'Размеры',
                            hideMode: 'offsets',
                            items: [{xtype: 'remains-panel-resource-inject'}]
                        });
                    }
                }
            }
        }

        return fields;
    },

    setOptionValues: function (optionValues) {

        var extraFields = miniShop2.config.extra_fields;
        var optionFields = miniShop2.config.option_fields;
        var allOptions = [];

        for (i in extraFields) {
            if (!extraFields.hasOwnProperty(i)) {
                continue;
            }
            var key = extraFields[i];
            allOptions.push({'key': key, 'id': 'modx-resource-' + key});
        }

        for (i in optionFields) {
            if (!optionFields.hasOwnProperty(i)) {
                continue;
            }
            item = optionFields[i];
            allOptions.push({'key': item.key, 'id': 'modx-resource-' + item.key});
        }

        var optionFormValues = {};

        for (i in allOptions) {
            if (!allOptions.hasOwnProperty(i)) {
                continue;
            }

            item = allOptions[i];
            if (!optionValues.hasOwnProperty(item.key)) {
                continue;
            }

            field = this.getForm().findField(item.id);
            if (!field) {
                continue;
            }

            optionFormValues[field.name] = optionValues[item.key];
        }

        this.getForm().setValues(optionFormValues);
    },

    setOptionsValues: function () {

        var extraFields = miniShop2.config.extra_fields;
        var optionFields = miniShop2.config.option_fields;
        var allOptions = [];

        for (i in extraFields) {
            if (!extraFields.hasOwnProperty(i)) {
                continue;
            }
            key = extraFields[i];
            allOptions.push({'key': key, 'id': 'modx-resource-' + key});
        }

        for (i in optionFields) {
            if (!optionFields.hasOwnProperty(i)) {
                continue;
            }
            item = optionFields[i];
            allOptions.push({'key': item.key, 'id': 'modx-resource-' + item.key});
        }

        MODx.Ajax.request({
            url: msoptionsprice.config.connector_url,
            params: {
                action: 'mgr/misc/option/getvalues',
                rid: this.config.record.id || 0,
                limit: 0,

            },
            listeners: {
                success: {
                    fn: function (response) {

                        var optionValues = response.results || {};
                        var optionFormValues = {};

                        for (i in allOptions) {
                            if (!allOptions.hasOwnProperty(i)) {
                                continue;
                            }

                            item = allOptions[i];
                            if (!optionValues.hasOwnProperty(item.key)) {
                                continue;
                            }

                            field = this.getForm().findField(item.id);
                            if (!field) {
                                continue;
                            }

                            optionFormValues[field.name] = optionValues[item.key];

                            /*if ((field.name.indexOf('[]') + 1)) {
                             optionFormValues[field.name] = optionValues[item.key];
                             }
                             else {
                             optionFormValues[field.name] = optionValues[item.key];
                             }*/
                        }
                        this.getForm().setValues(optionFormValues);
                    },
                    scope: this
                }
            }
        });

    },

});

remains.window.createRemains = function (config) {
    config = config || {};
    this.ident = config.ident || 'remainsnew'+Ext.id();

    Ext.applyIf(config, {
        title: 'Добавление размера',
        id: this.ident,
        autoHeight: true,
        width: 650,
        url: remains.config.connector_url,
        baseParams: {
            product_id: remains.config.resource,
            action: 'mgr/remains/create'
        },
        fields: [
            {xtype: 'textfield', fieldLabel: 'Размер', name: 'size', id: 'rem-'+this.ident+'-size', anchor: '100%'},
            {xtype: 'textfield', fieldLabel: 'Количество', name: 'count', id: 'rem-'+this.ident+'-count', anchor: '100%'}
        ],
        keys: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
    });

    remains.window.createRemains.superclass.constructor.call(this,config);
};
Ext.extend(remains.window.createRemains, MODx.Window);
Ext.reg('remains-window-remain-create', remains.window.createRemains);

remains.window.updateRemains = function (config) {
    config = config || {};
    this.ident = config.ident || 'remainsupdate'+Ext.id();

    Ext.applyIf(config, {
        title: 'Редактирование размера',
        id: this.ident,
        autoHeight: true,
        width: 650,
        url: remains.config.connector_url,
        baseParams: {
            product_id: remains.config.resource,
            action: 'mgr/remains/update'
        },
        fields: [
            {xtype: 'hidden',name: 'id',id: 'rem-'+this.ident+'-id'},
            {xtype: 'textfield', fieldLabel: 'Размер', name: 'size', id: 'rem-'+this.ident+'-size', anchor: '100%'},
            {xtype: 'textfield', fieldLabel: 'Количество', name: 'count', id: 'rem-'+this.ident+'-count', anchor: '100%'}
        ],
        key: [{key: Ext.EventObject.ENTER,shift: true,fn: function() {this.submit() },scope: this}]
    });

    remains.window.updateRemains.superclass.constructor.call(this, config);
};
Ext.extend(remains.window.updateRemains, MODx.Window);
Ext.reg('remains-window-remain-update', remains.window.updateRemains);

/*MODx.Ajax.request({
    url: this.config.url
    ,params: {
        source: this.config.baseParams.source
        ,from: from
        ,to: to
        ,action: this.config.sortAction || 'browser/directory/sort'
        ,point: dropEvent.point
    }
    ,listeners: {
        'success': {fn:function(r) {
                var el = dropEvent.dropNode.getUI().getTextEl();
                if (el) {Ext.get(el).frame();}
                this.fireEvent('afterSort',{event:dropEvent,result:r});
            },scope:this}
        ,'failure': {fn:function(r) {
                MODx.form.Handler.errorJSON(r);
                this.refresh();
                return false;
            },scope:this}
    }
});
xtype: 'xcheckbox'
    ,boxLabel: _('duplicate_children') + ' ('+this.config.childCount+')'
    ,hideLabel: true
    ,name: 'duplicate_children'
    ,id: 'modx-'+this.ident+'-duplicate-children'
    ,checked: true
    ,listeners: {
    'check': {fn: function(cb,checked) {
        if (checked) {
            this.fp.getForm().findField('modx-'+this.ident+'-name').disable();
        } else {
            this.fp.getForm().findField('modx-'+this.ident+'-name').enable();
        }
    },scope:this}
}*/