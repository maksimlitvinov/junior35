remains.tools.renderActions = function (value, props, row) {
    var res = [];
    for (var i in row.data.actions) {
        if (!row.data.actions.hasOwnProperty(i)) {continue;}
        var a = row.data.actions[i];
        if (a['button']) {
            var cls = typeof(a['class']) == 'object' && a['class']['button']
                ? a['class']['button']
                : '';
            cls += ' icon icon-' +  a['icon'];
            res.push(
                '<li>\
                    <button class="btn btn-default '+ cls +'" type="'+a['type']+'" title="'+_('remains_action_'+a['type'])+'"></button>\
				</li>'
            );
        }
    }
    return '<ul class="remains-row-actions">' + res.join('') + '</ul>';
};

remains.tools.getMenu = function (actions, grid) {
    var menu = [];
    for (var i in actions) {
        if (!actions.hasOwnProperty(i)) {continue;}
        var a = actions[i];
        if (!a['menu']) {
            if (a == '-') {menu.push('-');}
            continue;
        }
        else if (menu.length > 0 && /^remove/i.test(a['type'])) {
            menu.push('-');
        }

        var cls = typeof(a['class']) == 'object' && a['class']['menu']
            ? a['class']['menu']
            : '';

        cls += ' icon icon-' + a['icon'];

        menu.push({
            text: '<i class="' + cls + ' x-menu-item-icon"></i> ' + _('remains_action_'+a['type']),
            handler: grid[a['type']]
        });
    }

    return menu;
};

remains.tools.onAjax = function (el) {
    Ext.Ajax.el = el;
    Ext.Ajax.on('beforerequest', remains.tools.beforerequest);
    Ext.Ajax.on('requestcomplete', remains.tools.requestcomplete);
};

remains.tools.beforerequest = function () {Ext.Ajax.el.mask('Удаление','x-mask-loading');};

remains.tools.requestcomplete = function () {
    Ext.Ajax.el.unmask();
    Ext.Ajax.el = null;
    Ext.Ajax.un('beforerequest', remains.tools.beforerequest);
    Ext.Ajax.un('requestcomplete', remains.tools.requestcomplete);
};

remains.tools.setMsField = function (name, val) {
    MODx.Ajax.request({
        url: remains.config.connector_url,
        params: {
            action: 'mgr/minishop/update',
            id: remains.config.resource,
            val: val,
            name: name
        },
        listeners: {
            success: {
                fn: function(r) {
                    // TODO Сделать оповещение о сохранении
                }
            }
        }
    });
};

remains.tools.getCountFromRows = function (grid) {
    var count = 0;
    if (grid.data.items.length > 0) {
        for (var item of grid.data.items) {
            count += parseInt(item.data.count);
        }
    }
    return count;
};