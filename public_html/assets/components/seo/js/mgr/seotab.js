Ext.override(MODx.panel.Resource, {
    getParentFields: MODx.panel.Resource.prototype.getFields,

    getFields: function (config) {
        var parentFields = this.getParentFields.call(this, config);

        for (var i in parentFields) {
            var item = parentFields[i];
            if (item.id == 'modx-resource-tabs') {
                item.items.push({
                    id: 'modx-resource-seo'
                    ,autoHeight: true
                    ,title: 'SEO'
                    //,title: _('resource_groups')
                    ,layout: 'form'
                    ,anchor: '100%'
                    ,items: [{
                        html: '<p>На данной вкладке указываются SEO данные</p>',
                        //html: '<p>'+_('resource_access_message')+'</p>',
                        xtype: 'modx-description'
                    },{
                        layout: 'column',
                        width: '100%',
                        anchor: '100%',
                        cls: 'main-wrapper',
                        defaults: {
                            labelSeparator: '',
                            labelAlign: 'top',
                            border: false,
                            msgTarget: 'under'
                        },
                        items: [{
                            columnWidth: .5,
                            layout: 'form',
                            items: [{
                                xtype: 'textfield',
                                width: '100%',
                                value: config.record.seo_title || '',
                                description: '<b>[[*seo_title]]</b><br />'+_('resource_seo_title_help'),
                                fieldLabel: _('resource_seo_title') || 'Заголовок',
                                name: 'seo_title',
                                id: 'modx-resource-seo_title',
                                anchor: '100%',
                                maxLength: 255
                            }]
                        },{
                            columnWidth: .5,
                            layout: 'form',
                            items: [{
                                xtype: 'textfield',
                                width: '100%',
                                value: config.record.seo_key || '',
                                description: '<b>[[*seo_key]]</b><br />'+_('resource_seo_key_help'),
                                fieldLabel: _('resource_seo_key') || 'Ключевые слова',
                                name: 'seo_key',
                                id: 'modx-resource-seo_key',
                                anchor: '100%',
                                maxLength: 255
                            }]
                        },{
                            columnWidth: 1,
                            layout: 'form',
                            style: 'margin-left: 0',
                            items: [{
                                xtype: 'textarea',
                                width: '100%',
                                value: config.record.seo_desc || '',
                                description: '<b>[[*seo_desc]]</b><br />'+_('resource_seo_desc_help'),
                                fieldLabel: _('resource_seo_desc') || 'Описание',
                                name: 'seo_desc',
                                id: 'modx-resource-seo_desc',
                                grow: true,
                                anchor: '100%',
                                maxLength: 255
                            }]
                        }]
                    }]
                });
            }
        }
        return parentFields;
    }
});