Tooltip = {
	sel: {
		btn: '.tooltip__btn',
		block: '.tooltip',
		trigger: '.tooltip .trigger'
	},
	classes: {
		active: 'active'
	},
	init: function () {
		this.load();
	},
	load: function () {
		$(document)
		.on('click', 'body,html', function () {
			Tooltip.closeAll();
		})
		.on('click', this.sel.btn, function (e) {
			e.preventDefault();
			e.stopPropagation();
			var $block = $($(this).attr('href'));
			if ($block.hasClass(Tooltip.classes.active)) {
				Tooltip.close($block);
			}
			else {
				Tooltip.show($block,this);
			}
		})
		.on('click', this.sel.trigger, function (e) {
			e.preventDefault();
			var el = $(this).attr('href');
			$(el).trigger('click');
		});
	},
	show: function (el,btn) {
		var btnPosition = $(btn).offset();
		var btnWidth = $(btn).width() + parseInt($(btn).css('padding-left')) + parseInt($(btn).css('padding-right'));
		var elWidth = $(el).width();
		var elHeight = $(el).height();

		var elLeftPosition = (btnPosition.left + btnWidth / 2) - elWidth / 2;
		var elTopPosition = btnPosition.top - elHeight;
		console.log(btnPosition.top,elHeight,elTopPosition);
		$(el).css({ left:elLeftPosition+'px',top:elTopPosition+'px' }).addClass(Tooltip.classes.active);
	},
	close: function (el) {
		$(el).removeClass(Tooltip.classes.active);
	},
	closeAll: function () {
		// console.log($(Tooltip.sel.btn + '.' + Tooltip.classes.active));
		$(Tooltip.sel.block + '.' + Tooltip.classes.active).each(function () {
			Tooltip.close(this);
		});
	}
};

$(function() {

	$("select").styler();

	//toggle menu
	$(".toggle-menu").on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('on');
		$(".main-menu").toggleClass('show');
	});

	//slider
	var swiper = new Swiper('.four-items-slider .swiper-container', {
		slidesPerView: 4,
		spaceBetween: 32,
		loop: true,
		navigation: {
			nextEl: '.four-items-slider .my-swiper-button-next',
			prevEl: '.four-items-slider .my-swiper-button-prev',
		},
		breakpoints: {
			0: {
				slidesPerView: 1,
			},
			576: {
				slidesPerView: 2,
			},
			768: {
				slidesPerView: 3,
			},
			992: {
				slidesPerView: 4,
			},
		}
	});

	var counter = $('.swiper-counter');
	var now = $(".swiper-counter .now");
	var total = $(".swiper-counter .total");

	var swiper = new Swiper('.one-item-slider .swiper-container', {
		slidesPerView: 1,
		loop: false,
		navigation: {
			nextEl: '.one-item-slider .my-swiper-button-next',
			prevEl: '.one-item-slider .my-swiper-button-prev',
		},
		on: {
			init: function () {

				now.html(1);
				total.html(this.slides.length);
			},
			slideChange: function () {
				console.log(this);
				now.html(this.activeIndex + 1);
			}
		}
	});



	$(".second-submenu").each(function(index, el) {
		$(this).parent("li").addClass("has-second-submenu");
	});

	$(".has-second-submenu").each(function(index, el) {
		$(this).append('<div class="arrow"><svg width="10" height="16" viewBox="0 0 10 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.56021 8.00004C9.56021 8.2868 9.45072 8.57351 9.2322 8.79213L2.3525 15.6718C1.91487 16.1094 1.20532 16.1094 0.767859 15.6718C0.330402 15.2343 0.330402 14.5249 0.767859 14.0872L6.8554 8.00004L0.768072 1.91282C0.330614 1.47518 0.330614 0.765843 0.768072 0.328421C1.20553 -0.109426 1.91508 -0.109426 2.35271 0.328421L9.23242 7.20795C9.45097 7.42668 9.56021 7.7134 9.56021 8.00004Z" /></svg></div>');
	});

	$(".has-second-submenu .arrow").on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('show');
		$(this).parent(".has-second-submenu").find(".second-submenu").slideToggle()
	});

	$(".main-menu li").each(function(index, el) {
		if ($(this).find(".submenu").length > 0) {
			$(this).addClass("has-submenu");
			$(this).append("<div class='mobile-arrow'></div>");
		}
	});


	// submenu hover
	if ( $(window).width() > 768 ) {
		//desktop
		$(".main-menu li.has-submenu").hover(function() {
			/* Stuff to do when the mouse enters the element */
			$(".main-menu .submenu").removeClass('show');
			$(this).find(".submenu").addClass('show');
			$(".main-menu li").removeClass('show-submenu');
			$(this).addClass("show-submenu");
		}, function() {

			/* Stuff to do when the mouse leaves the element */
		});

		$(".main-menu .submenu").hover(function() {
			/* Stuff to do when the mouse enters the element */
		}, function() {
			$(this).removeClass('show');
			// var menuitem = '#' + $(this).data("menu");
			$(".main-menu li").removeClass('show-submenu');
		});
	}else{
		//mobile
		$(".main-menu .mobile-arrow").on('click', function(event) {
			event.preventDefault();
			$(this).prev(".submenu").toggleClass("show");
			$(this).toggleClass('open');

		});

		// When the user scrolls the page, execute myFunction
		window.onscroll = function() {myFunction()};

		// Get the header
		var header = $("header");

		// Get the offset position of the navbar
		var sticky = header.offset().top - 57;

		// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
		function myFunction() {
			if (window.pageYOffset > sticky) {
				header.addClass("sticky")
			} else {
				header.removeClass("sticky");
			}
		}

		//search icon
		$(".mobile-icons .search-icon").on('click', function(event) {
			event.preventDefault();
			$(".mobile-search-block").toggleClass('show');
		});

	}

	// popups
	$('.popup-btn').magnificPopup({
		type: 'inline',
		preloader: false
	});

	$('.img-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: false
		}
	});

	//price sort
	$(".price-sort-btn").on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('to-hight to-low');
		if ($(this).find("span").html() == 'Цена возрастает') {
			$(this).find("span").html('Цена убывает');
		}else{
			$(this).find("span").html('Цена возрастает');
		}
	});

	//filter show
	$(".menu-filter-wrapper .show-btn").on('click', function(event) {
		event.preventDefault();
		$(".menu-filter-wrapper").toggleClass('show');
	});

	$(".filter-button").on('click', function(event) {
		event.preventDefault();
		$(".menu-filter-wrapper").addClass('show');
	});

	$(".menu-filter-wrapper .close-btn").on('click', function(event) {
		event.preventDefault();
		$(".menu-filter-wrapper").removeClass('show');
	});

	// product-gallery
	var galleryThumbs = new Swiper('.product-gallery .gallery-thumbs', {
		spaceBetween: 16,
		slidesPerView: 4,
		direction: 'vertical',
		freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		breakpoints: {
			1200: {
				direction: 'vertical',
			},
			0: {
				direction: 'horizontal',
			},
		}
	});
	var galleryTop = new Swiper('.product-gallery .gallery-items', {
		spaceBetween: 10,
		direction: 'vertical',
		thumbs: {
			swiper: galleryThumbs
		},
		breakpoints: {
			1200: {
				direction: 'vertical',
			},
			0: {
				direction: 'horizontal',
			},
		}
	});


	// $(".count-block .minus").on('click', function(event) {
	// 	event.preventDefault();
	// 	var input = $(this).parent().find("input");
	// 	var val = Number(input.val());
	// 	if (val > 1) {
	// 		input.val(Number(val - 1));
	// 	}
	// });

	// $(".count-block .plus").on('click', function(event) {

	// 	event.preventDefault();
	// 	var input = $(this).parent().find("input");
	// 	var val = Number(input.val());
	// 	var max = Number($(this).closest('.form-to-cart').find('input:radio:checked').data('count'));
	// 	if (val < max) {
	// 		input.val(Number(val + 1));
	// 	}
	// 	else {
	// 		miniShop2.Message.error('Выбрано максимальное количество.');
	// 	}
	// });

	// $(".count-block input").change(function(event) {

	// 	var val = Number($(this).val());
	// 	var max = Number($(this).closest('.form-to-cart').find('input:radio:checked').data('count'));
	// 	console.log(max);
	// 	if (val < 1) {
	// 		$(this).val(1);
	// 	}else if (val > max) {
	// 		$(this).val(max);
	// 	}

	// });

	$(".password-wrapper .show-password-btn").on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('hide');
		var input = $(this).siblings('input');
		if (input.prop("type") === "password") {
			input.prop('type', 'text');
		}else{
			input.prop('type', 'password');

		}
	});


	$(".map-wrapper .addresses-block input[type=radio]").on('change', function(event) {
		$(".map-wrapper .addresses-block .additional-text").addClass('hidden');
		$(this).parent(".custom-radio").next(".additional-text").removeClass('hidden');
	});

	//открытие закрытие спойлеров
	$(".spoiler-block .spoiler-title").click(function(event) {
		$(this).toggleClass('open');
		$(this).next('.spoiler-content').slideToggle();
		$(this).nextAll('.spoiler-note').toggle();
	});

	//tooltip
	Tooltip.init();

	// tabs
	$('ul.tabs__caption').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
	});

	if($('.own-scrollbar').length > 0){
		$('.own-scrollbar').each(element, new SimpleBar(element, {
			autoHide : false,
		}));
	}
	// $('.own-scrollbar').each(element, new SimpleBar(element, {
	// 	autoHide : false,
	// }));

	// Array.prototype.forEach.call(
	// 	document.querySelectorAll('.own-scrollbar'),
	// 	el => new SimpleBar()
	// );

});
